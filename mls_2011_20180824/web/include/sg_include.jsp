<%-- <%@ page import="kr.or.copyright.mls.support.constant.*" %>
<%@ page import="signgate.crypto.util.*" %>

<%!
	// 인증서 환경 설정
	private static String ServerCertPath = SignContants.ServerCertPath; // 서버 인증서 저장 디렉토리
	private static String CRLCacheDirectory = SignContants.CRLCacheDirectory; // 인증서 폐지목록 저장 디렉토리
	
	private static String ServerSignCertFile = SignContants.ServerSignCertFile; // 서버 전자서명용 인증서 파일명
	private static String ServerSignKeyFile = SignContants.ServerSignKeyFile; // 서버 전자서명용 개인키 파일명
	private static String ServerKmCertFile = SignContants.ServerKmCertFile; // 서버 암호화용 인증서 파일명
	private static String ServerKmKeyFile = SignContants.ServerKmKeyFile; // 서버 암호화용 개인키 파일명

	// 허용할 인증서 정책 OID 리스트 (범용 인증서 OID 리스트)
	public static final String[] AllowedPolicyOIDs = SignContants.AllowedPolicyOIDs;

	byte[] ServerSignCert = null;
	byte[] ServerSignKey = null;
	byte[] ServerKmCert = null;
	byte[] ServerKmKey = null;
	String ServerSignCertPem = "";
	String ServerKmCertPem = "";

/* 운영 환경(UNIX)에서 사용 서버인증서 정보 초기화 */
/***********************************************************/
	public void jspInit()
	{
		try
		{
			ServerSignCert = FileUtil.readBytesFromFileName(ServerSignCertFile);
			ServerSignKey = FileUtil.readBytesFromFileName(ServerSignKeyFile);
			ServerKmCert = FileUtil.readBytesFromFileName(ServerKmCertFile);
			ServerKmKey = FileUtil.readBytesFromFileName(ServerKmKeyFile);

			ServerSignCertPem = CertUtil.derToPem( ServerSignCert );
			ServerKmCertPem = CertUtil.derToPem( ServerKmCert );
		}
		catch(Exception e)
		{
			System.out.println( "Can't read server's certificate!!!!!!!!!!!!!!!!!!!!!" );
		}
	}
/***********************************************************/
%>
<%
	response.setHeader("Cache-Control","no-cache,no-store");
	response.setHeader("Pragma","no-cache,no-store");
	
	// 재사용공격을 막기 위해서 사용자 인증 데이터에 매번 변하는 값(nonce)을 포함시킨다.
	session.removeAttribute( SignContants.CHALLENGE ); // 기존의 값이 있을 경우 지워서 초기화 한다.
	String strChallenge = Base64Util.encode( RandomUtil.genRand() ); // 페이지가 열릴 때 마다 사용할 nonce를 새로 생성한다.
	session.setAttribute( SignContants.CHALLENGE, strChallenge ); // 사용자 인증 데이터 검사 페이지에서 사용할 수 있도록 세션 데이터로 저장한다.
	
	// 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값을 임의로 생성한다.
	String strCertId = Base64Util.encode( RandomUtil.genRand() );
%>
<script type="text/javascript" language='javascript' src='/js/sg_basic.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_error.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_util.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_cert.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_sign.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_encrypt.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_pkcs7.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_hash.js'></script>
<script type="text/javascript" language='javascript' src='/js/sg_base64.js'></script>

<script type="text/javascript" language="javascript">
<!--
/*
CRLCacheDirectory : <%=CRLCacheDirectory%>
ServerCertPath : <%=ServerCertPath%>
ServerSignCertFile : <%=ServerSignCertFile%>
ServerSignKeyFile : <%=ServerSignKeyFile%>
ServerKmCertFile : <%=ServerKmCertFile%>
ServerKmKeyFile : <%=ServerKmKeyFile%>

Java Properties:
-------------------------------------------------------
file.encoding=<%= System.getProperty("file.encoding") %>
user.language=<%= System.getProperty("user.language") %>
user.region=<%= System.getProperty("user.region") %>

*/
/**
 * @type   : prototype_function
 * @access : public
 * @desc   : 자바스크립트의 내장 객체인 String 객체에 simpleReplace 메소드를 추가한다. simpleReplace 메소드는
 *           스트링 내에 있는 특정 스트링을 다른 스트링으로 모두 변환한다. String 객체의 replace 메소드와 동일한
 *           기능을 하지만 간단한 스트링의 치환시에 보다 유용하게 사용할 수 있다.
 * <pre>
 *     var str = "abcde"
 *     str = str.simpleReplace("cd", "xx");
 * <\/pre>
 * 위의 예에서 str는 "abxxe"가 된다.
 * @sig    : oldStr, newStr
 * @param  : oldStr required 바뀌어야 될 기존의 스트링
 * @param  : newStr required 바뀌어질 새로운 스트링
 * @return : replaced String.
 * @author : 임재현
 */
String.prototype.simpleReplace = function(oldStr, newStr) {
	var rStr = oldStr;

	rStr = rStr.replace(/\\/g, "\\\\");
	rStr = rStr.replace(/\^/g, "\\^");
	rStr = rStr.replace(/\$/g, "\\$");
	rStr = rStr.replace(/\*/g, "\\*");
	rStr = rStr.replace(/\+/g, "\\+");
	rStr = rStr.replace(/\?/g, "\\?");
	rStr = rStr.replace(/\./g, "\\.");
	rStr = rStr.replace(/\(/g, "\\(");
	rStr = rStr.replace(/\)/g, "\\)");
	rStr = rStr.replace(/\|/g, "\\|");
	rStr = rStr.replace(/\,/g, "\\,");
	rStr = rStr.replace(/\{/g, "\\{");
	rStr = rStr.replace(/\}/g, "\\}");
	rStr = rStr.replace(/\[/g, "\\[");
	rStr = rStr.replace(/\]/g, "\\]");
	rStr = rStr.replace(/\-/g, "\\-");

  	var re = new RegExp(rStr, "g");
    return this.replace(re, newStr);
}

// 사용자 서명용  인증서 선택하여 인증서 정보 얻기
function fn_selectUserSignCert(strCertID){
	//	사용자 전자서명용 인증서 얻기
	//var strSignCert = getUserSignCert( strCertID );
	// 위에서 사용자가 선택한 전자서명용 인증서를 얻는다. 
	// 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var strSignCert = getUserSignCert( strCertID );
	
	if ( strSignCert == "" )
	{
		var strErr = getErrorString();
		if ( strErr == "" )	//	사용자가 인증서 선택을 취소한 경우
		{
			clearCertificateInfo( strCertID ); // 사용자가 선택했던 인증서 정보를 메모리에서 지운다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
			alert( "인증서 선택을 취소하였습니다");
			return;
		}
		else
		{
			alert( getErrorFunctionName() + ": " + getErrorString() );
			return;
		}
	}

	return strSignCert;

}

// 선택된 인증서 정보와 스트링 데이터에 대한 전자서명 생성하기
function fn_genDigitalSign(strCertID, strOriginalMessage) {
	//	스트링 데이터에 대한 전자서명 생성하기
	var strSignValue = generateDigitalSignature( strCertID, strOriginalMessage );
	
	if ( strSignValue == "" )
	{
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return;
	}

	return strSignValue;
}

// 데이터 암호화 할때 사용될 비밀키와 선택된 인증서로 암호화 한다. 
function fn_encryptSessionKey(strCertID, strKmCert){	
	// 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 선택된 인증서(공개키)로 암호화하여 반환한다.
	// 이때 생성한 비밀키를 추적하기 위하여 strCertID 값이 필요하다.
	var strEncryptedSessionKey = encryptSessionKey( strCertID, strKmCert );
	if ( strEncryptedSessionKey == "" )
	{
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return;
	}
	
	return strEncryptedSessionKey;
}

// 입력되는 스트링 데이터를 암호화 한다.
function fn_encryptStrData(strCertID, strData){
	// 필요한 만큼 암호화 함수를 호출하여 사용할 수 있다.
	var strEncryptedData = encryptDataString( strCertID, strData );
	if ( strEncryptedData == "" )
	{
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return;
	}
	
	return strEncryptedData;
}

// 인증서 소유자를 확인하기 위한 정보를 생성한다.
function fn_createRandomNumber(strCertID){
	var strRandomNumber = getRandomNumber( strCertID );
	if ( strRandomNumber == "" ){
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return;
	} else {
		return strRandomNumber;
	}
}


// 인증서 소유자를 확인하기 위한 정보를 암호화 한다.
function fn_encryptUserRandomNumber(strCertID, strUserRandomNumber){
	var strEncryptedUserRandomNumber = encryptDataString( strCertID, strUserRandomNumber );
	if ( strEncryptedUserRandomNumber == "" )
	{
		clearCertificateInfo( strCertID ); // 사용자가 선택했던 인증서 정보를 메모리에서 지운다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return;
	} else {
		return strEncryptedUserRandomNumber;
	}
}

// 입력된 주민번호가 선택된 인증서의 소유자인지 확인
function fn_checkUserSSNCertOwner(strSignCert, strSSN, strRandomNumber){
	// 인증서, 주민등록번호, 비밀키로 인증서 소유여부 확인
	var bReturn = checkCertOwner( strSignCert, strSSN, strRandomNumber );
	
	if ( !bReturn ){
		alert( getErrorFunctionName() + ": " + getErrorString() );
		return false;
	}else{
		//alert( "인증서 소유자 확인에 성공하였습니다." );
		return true;
	}

}

// 주민등록번호 유효성 확인
function fn_ssnNumChk(no){
    var num;
    
    no = no.simpleReplace("-", "");	// 주민등록번호에 "-"가 포함된 경우 제외
    if(no == ""){
    	return false;
    }
    
    
    /*
    num=(parseInt(no.charAt(0))*2) + (parseInt(no.charAt(1))*3) + (parseInt(no.charAt(2))*4);
    num+=(parseInt(no.charAt(3))*5)+(parseInt(no.charAt(4))*6)+(parseInt(no.charAt(5))*7);
    num+=(parseInt(no.charAt(6))*8)+(parseInt(no.charAt(7))*9)+(parseInt(no.charAt(8))*2);
    num+=(parseInt(no.charAt(9))*3)+(parseInt(no.charAt(10))*4)+(parseInt(no.charAt(11))*5);
    num=11-(num%11);
    
    check = (no.charAt(12)==num) ? true : false;
    
    return check;
	*/

        var a1=no.substring(0,1);   //주민번호 계산법
        var a2=no.substring(1,2);          
        var a3=no.substring(2,3);
        var a4=no.substring(3,4);
        var a5=no.substring(4,5);
        var a6=no.substring(5,6);
        var checkdigit=a1*2+a2*3+a3*4+a4*5+a5*6+a6*7;
        var b1=no.substring(6,7);
        var b2=no.substring(7,8);
        var b3=no.substring(8,9);
        var b4=no.substring(9,10);
        var b5=no.substring(10,11);
        var b6=no.substring(11,12);
        var b7=no.substring(12,13);
        var checkdigit=checkdigit+b1*8+b2*9+b3*2+b4*3+b5*4+b6*5; 
        checkdigit = checkdigit%11;
        checkdigit = 11 - checkdigit;
        checkdigit = checkdigit%10;   

     if (checkdigit != b7){
     	
     	return false;
     	
    }else{
    	return true;	
    } 	
}

// 외국인번호 유효성 확인
function fn_fgnNumChk(no) {
        var sum=0;
        var odd=0;
        buf = new Array(13);
        
        no = no.simpleReplace("-", "");	// 외국인번호에 "-"가 포함된 경우 제외
        if(no == ""){
	    	return false;
	    }    

        for(i=0; i<13; i++) { buf[i]=parseInt(no.charAt(i)); }
        odd = buf[7]*10 + buf[8];
        if(odd%2 != 0) { return false; }
        if( (buf[11]!=6) && (buf[11]!=7) && (buf[11]!=8) && (buf[11]!=9) ) {
                return false;
        }

        multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
        for(i=0, sum=0; i<12; i++) { sum += (buf[i] *= multipliers[i]); }
        sum = 11 - (sum%11);
        if(sum >= 10) { sum -= 10; }
        sum += 2;
        if(sum >= 10) { sum -= 10; }
        if(sum != buf[12]) { return false }
        return true;
}

// 사업자등록번호 유효성 확인
function fn_bizNumChk(no){
    var sum = 0; 
    var getlist = new Array(10);
    var chkvalue =new Array("1","3","7","1","3","7","1","3","5"); 
    
    no = no.simpleReplace("-", "");	// 사업자등록번호에 "-"가 포함된 경우 제외
    if(no == ""){
    	return false;
    }    
   
    try {
        for(var i=0; i<10; i++) {
            getlist[i] = no.substring(i, i+1);
        } 
        
        for(var i=0; i<9; i++) {
            sum += getlist[i]*chkvalue[i];
        } 
        sum = sum + parseInt((getlist[8]*5)/10);
        sidliy = sum % 10; 
        sidchk = 0; 
        
        if(sidliy != 0) { 
            sidchk = 10 - sidliy; 
        } else {
            sidchk = 0; 
        } 

        if(sidchk != getlist[9]) { 
            return false; 
        } 
        return true;
    } catch(e) {
        return false;
    }
}


// KeyCode 값으로 숫자여부 확인
function fn_numKeyChk(){
    //숫자만 입력가능하게 확인
    event.returnValue=false;
    if(event.keyCode >= 48 && event.keyCode <= 57) event.returnValue=true;    //숫자허용
    if(event.keyCode == 45 && event.keyCode == 46) event.returnValue=true;    //숫자허용
}


/******************************************************************************
 * 공인인증서를 이용하여 전자서명 하는 경우 사용하는 함수
 * 함수를 이용하기 위하여 아래소스를 <form> TAG 안에 추가하여야 한다. 
 	 
	<!-- 로그인 사용자 인증 데이터를 매번 다르게 하기 위하여 WAS 서버에서 랜덤하게 생성한 nonce 값 -->
	<input type="hidden" name="Challenge" value="<%//=strChallenge%>" />	<-- JSP 코드 활성화
	<!-- 대칭키를 RSA 알고리즘으로 암호화하기 위한 WAS 서버의 암호화용 인증서  -->
	<input type="hidden" name="ServerKmCert" value="<%//=ServerKmCertPem%>" />	<-- JSP 코드 활성화
	<!-- 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값 -->
	<input type="hidden" name="certId" />
	<!-- 사용자가 선택한 전자서명용 인증서 정보 -->
	<input type="hidden" name="userSignCert" />
	<!-- 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 을 위한 원본 값  -->
	<TEXTAREA name="originalMessage" style="display:none;" ><\/TEXTAREA>
	<!-- 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 값을 생성 -->
	<input type="hidden" name="userSignValue" />
	<!-- 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 서버의 인증서(공개키)로 암호화한 값 -->
	<input type="hidden" name="encryptedSessionKey" />
	<!-- 사용자가 인증서 소유자임을 확인하기 위한 값 -->
	<input type="hidden" name="encryptedUserRandomNumber" />
	<!-- 사용자 주민등록번호를 암호화한 값 -->
	<input type="hidden" name="encryptedUserSSN" />
		
	<!-- 회원유형 (MEMBER_TYPE - BIZ_TYPE_CD)  -->
	<input type="hidden" name="memberType" value="${User.bizTypeCd}" />
	<!-- 주민등록번호 -->
	<input type="hidden" name="ssnNo" value="${User.idNo}" />
	<!-- 사업자번호  -->
	<input type="hidden" name="corpNo" value="${User.corpNo}" />
		

******************************************************************************/ 
// 공인인증서 를 이용한 전자서명
function fn_signCert(frm){
	var strCertID = "<%=strCertId%>"; // 사용자가 선택한 인증서를 KICA SecuKit JavaScript API에서 추적하는데 사용할 값
	frm.certId.value = strCertID;
	
	var strChallenge = frm.Challenge.value;
	var strServerKmCertPem = frm.ServerKmCert.value;
	var strUserSSN = "";
	var strMemberType = frm.memberType.value;
	
	// 사용자 유형(MEMBER_TYPE)에 따라 주민등록번호 / 사업자등록번호로 구분
	// 	1	:	개인		(주민등록번호)
	//	2	:	법인사업자	(사업자등록번호)	
	//	3	:	개인사업자	(사업자등록번호)
	//	4	:	임의단체(학회등)	(단체장의 주민등록번호)
	//	5	:	외국인	(외국인번호)
	switch (strMemberType){
		case "1": 	// 개인		(주민등록번호)
		case "4":	// 임의단체(학회등)		(주민등록번호)
			// 사용자 입력 값 (주민등록번호)
			strUserSSN = frm.ssnNo.value;
			
			// 주민등록번호 유효성 확인
			if(!fn_ssnNumChk(strUserSSN)){
				alert("사용자의 주민등록번호가 잘못 입력되어있습니다.\n 개인정보를 수정하시기 바랍니다.");
				return false;
			}
			break;
		case "2":	// 법인사업자	(사업자등록번호)	
		case "3": 	// 개인사업자	(사업자등록번호)
			// 사용자 입력 값 (사업자등록번호)
			strUserSSN = frm.corpNo.value;
			
			// 사업자등록번호 유효성 확인
			if(!fn_bizNumChk(strUserSSN)){
				alert("사용자의 사업자등록번호가 잘못 입력되어있습니다.\n 개인정보를 수정하시기 바랍니다.");
				return false;
			}
			break;
		case "5":	// 외국인	(외국인번호)
			// 사용자 입력 값 (외국인번호)
			strUserSSN = frm.ssnNo.value;
			
			// 외국인번호 유효성 확인
			if(!fn_fgnNumChk(strUserSSN)){
				alert("사용자의 외국인번호가 잘못 입력되어있습니다.\n 개인정보를 수정하시기 바랍니다.");
				return false;
			}
			break;
		default:
			break;
	}
	
		
	// 서버와 암호화 통신을 하기 위하여 사용할 비밀키를 추적하는데 사용할 값 확인
	if ( strServerKmCertPem == "" )
	{
		alert( "암호화에 사용할 인증서가 없습니다.\n서버 인증서 경로를 확인하십시오." );
		return false;
	}
	
	// 데이터를 암호화 할 때 사용할 비밀키를 생성하고, 서버의 인증서(공개키)로 암호화하여 반환한다.
	// 이때 생성한 비밀키를 추적하기 위하여 strCertID 값이 필요하다.
	var strEncryptedSessionKey = fn_encryptSessionKey( strCertID, strServerKmCertPem );
	if ( strEncryptedSessionKey == "" )
	{
		alert( "서버 인증서로 암호화 과정중 오류가 발생하였습니다.\n시스템 담당자에게 문의하시기 바랍니다." );
		return false;
	}
	frm.encryptedSessionKey.value = strEncryptedSessionKey;
	
	// 주민등록번호/사업자등록번호/외국인번호를 암호화 한다.
	var strEncryptedUserSSN = fn_encryptStrData(strCertID, strUserSSN);
	frm.encryptedUserSSN.value = strEncryptedUserSSN;
	
	// 사용자가 전자서명을 할 때 사용할 인증서를 선택하고 비밀번호를 입력할 수 있는 창을 띄운다.
	// 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var bReturn = reselectCertificate( strCertID );
	if ( !bReturn )
	{
		var strErr = getErrorString(); // 오류 메시지를 얻는다.
		if ( getErrorString() == "" ) // 사용자가 인증서 선택을 취소할 경우 오류메시지는 ""이다.
		{
			alert( "암호화에 사용할 인증서가 없습니다.\n인증서를 선택하시기 바랍니다." );
			return false;
		}
		else // 그 밖의 오류 처리
		{
	  		alert( getErrorFunctionName() + ": " + getErrorString() );
			return false;
		}
	}
		
	// 위에서 사용자가 선택한 인증서(전자서명용 개인키)로 전자서명 값을 생성한다.
	// 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	
	// 원본 메시지를 BASE64로 Encode 하여 전송하며, 저장시는 Decode 한다.
	var strOriginalMessage = frm.originalMessage.value + strUserSSN + strChallenge;	// 원본 메시지 + 서명자정보(주민등록번호/사업자등록번호/외국인번호) + 추적값
	frm.originalMessage.value = base64Encode(strOriginalMessage);
	var strUserSignValue = fn_genDigitalSign( strCertID, strOriginalMessage );
	if ( strUserSignValue == "" )
	{
		clearCertificateInfo( strCertID ); // 전자서명 생성에 실패할 경우에 사용자가 선택했던 인증서 정보를 메모리에서 지운다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
		alert( "선택한 인증서로 전자서명 값 생성에 실패하였습니다.\n인증서를 다시 선택하시기 바랍니다." );
		return false;
	}
	frm.userSignValue.value = strUserSignValue;
		
	// 위에서 사용자가 선택한 전자서명용 인증서를 얻는다. // 이 때 사용자가 선택한 인증서를 추적하기 위해서 strCertID 값이 필요하다.
	var strSignCert = fn_selectUserSignCert(strCertID);	// 사용자 인증서
	if ( strSignCert == "" )
	{
		alert( "암호화에 사용할 인증서가 없습니다.\n인증서를 선택하시기 바랍니다." );
		return false;
	}
	frm.userSignCert.value = strSignCert;
	
	var strRandomNumber = fn_createRandomNumber(strCertID);	// 인증서 소유자 확인을 위한 정보 생성
	
	// 선택된 인증서와 주민등록번호를 비교하여 소유자인지 확인
	if(fn_checkUserSSNCertOwner(strSignCert, strUserSSN, strRandomNumber)){
	
		// 인증서 소유자 확인을 위한 정보를 암호화 한다.
		var strEncryptedUserRandomNumber = fn_encryptUserRandomNumber( strCertID, strRandomNumber );
		if ( strEncryptedUserRandomNumber == "" )
		{
			alert( "인증서 소유자 확인을 위한 정보 암호화에 실패하였습니다.\n인증서를 선택하시기 바랍니다." );
			return false;
		}
		frm.encryptedUserRandomNumber.value = strEncryptedUserRandomNumber;
		
	} else {
		return false;
	}
	
	return true;
}

<!-- 공인인증 처리결과 메시지  -->
<%
	String singCertMessage = (String) request.getSession(true).getAttribute(SignContants.SIGN_VALIDATE);
	if(singCertMessage != null && !singCertMessage.equals("") && !singCertMessage.equals("null") && !singCertMessage.equals(null)){
		out.println("alert('"+singCertMessage+"');");
		
		request.getSession(true).removeAttribute(SignContants.SIGN_VALIDATE);		
	}
%>
//-->
</script> --%>