<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.net.*" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>

<% 
  User user = SessionUtil.getSession(request);

  String sessUserIdnt = user.getUserIdnt();
  String sessUserName = user.getUserName();
  String sessMgnbYsno = user.getMgnbYsno();
  String sessMail     = user.getMail();
  String sessMoblPhon = user.getMoblPhon();
  String sessUserDivs = user.getUserDivs();
  
  String loginYn = "N";
  if (sessUserIdnt != null) 
	  loginYn = "Y";
  
  
  // cookie test
/*
  if(request.getCookies()!=null) {
  	Cookie[] cookies = request.getCookies();
  	
  	for(int i=0; i<cookies.length; i++) {
  	
  		Cookie thisCookie = cookies[i];
		out.println("Cookie Name:::"+thisCookie.getName());
  		if(thisCookie.getName().equals("UID")){
  			out.println("Cookie Value:::"+thisCookie.getValue());
  		}
  	}
  }
*/
  
%>
<!--SCRIPT language=JavaScript src="/js/zoomInOut.js" type=text/JavaScript></SCRIPT-->
<script language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript">
<!--

/* 화면 확대 축소 시작 IE 전용 */
 var nowZoom = 100; // 현재비율
 var maxZoom = 200; // 최대비율(500으로하면 5배 커진다)
 var minZoom = 80; // 최소비율

 //화면 키운다.
 function zoomIn() {
 	if (nowZoom < maxZoom) {
   		nowZoom += 10; //25%씩 커진다.
  	} else {
   		return;
  	}
  	document.body.style.zoom = nowZoom + "%";
 }

 //화면 줄인다.
 function zoomOut() {
		if (nowZoom > minZoom) {
 			nowZoom -= 10; //25%씩 작아진다.
		} else {
 			return;
		}
		document.body.style.zoom = nowZoom + "%";
}

//초기화
function zoom() {
	document.body.style.zoom = 100 + "%";
}

// 로그인 절차 수행
function fn_logout(){
	var frm = document.logoutForm;

	frm.action = "/userLogin/userLogin.do";
	frm.submit();
}

function fn_selectUserInfo() {
	var frm = document.userInfoModi;

	frm.action = "/user/user.do";
	frm.submit();
}

// 로그인유무	return Y/N
function GetLoginYN() {
		
		return '<%=loginYn%>' ;
		
}

//-->
</script>
<div id="header">
	<!--home기타메뉴 및 화면확대축소 start-->
	<div id="siteNavi">
		<div id="global">
			<ul class="navi">
				<li tabindex="2"><a href="/main/main.do"><img src="/images/home.gif" alt="홈" /></a></li>
				<!--li tabindex="3"><a href="#"><img src="/images/sitemap.gif" alt="사이트맵" /></a></li-->
				<li tabindex="3"><a href="mailto:clms@copyright.or.kr"><img src="/images/contactus.gif" alt="contactus" /></a></li>
			</ul>
			<!--화면 확대 축소 start-->
			<ul class="sizeControl">
				<li tabindex="4"><a href="javascript:zoomIn();" title="확대"><img src="/images/zoomin.gif" alt="확대" /></a></li>
				<li tabindex="5"><a href="javascript:zoom();" title="보통"><img src="/images/default.gif" alt="기본" /></a></li>
				<li tabindex="6"><a href="javascript:zoomOut();" title="축소"><img src="/images/zoomout.gif" alt="축소" /></a></li>
			</ul>
			<!--화면 확대 축소 end-->
		</div>
	</div>
	<!--home기타메뉴 및 화면확대축소 end-->
	<!--로고 및 탑메인메뉴 start-->
	<div id="topNavi">
		<div class="logo">
			<h1 tabindex="1"><a href="/main/main.do">저작물내권리찾기</a></h1>
		</div>
		<div class="navi">
			<div class="mainMenu">
				<script type="text/javascript" language="javascript">
  				GetFlash('/images/swf/topNavi.swf','502','44');
  			</script>
				<!--자바스크립트 불가한 웹브라우져 환경 start-->
				<noscript>
				<ul>
					<li><a href="javascript:menu('1','0','0');">내권리찾기신청</a></li>
					<li><a href="javascript:menu('2','0','0');">보상신청</a></li>
					<li><a href="javascript:menu('3','0','0');">신청결과조회</a></li>
					<li><a href="/board/board.do?menuSeqn=1&page_no=1">FAQ</a></li>
					<li><a href="/board/board.do?menuSeqn=2&page_no=1">Qna</a></li>
					<li><a href="javascript:menu('6','0','0');">내권리찾기소개</a></li>
				</ul>
				</noscript>
				<!--자바스크립트 불가한 웹브라우져 환경 start-->
				<!--스타일 제거시 start-->
				<ul class="hide">
					<li><a href="javascript:menu('1','0','0');">내권리찾기신청</a></li>
					<li><a href="javascript:menu('2','0','0');">보상신청</a></li>
					<li><a href="javascript:menu('3','0','0');">신청결과조회</a></li>
					<li><a href="/board/board.do?menuSeqn=1&page_no=1">FAQ</a></li>
					<li><a href="/board/board.do?menuSeqn=2&page_no=1">Qna</a></li>
					<li><a href="javascript:menu('6','0','0');">내권리찾기소개</a></li>
				</ul>
				<!--스타일 제거시 end-->
			</div>
			<!--로그인박스 start-->
<%    if (sessUserIdnt != null) {  %>
			<!--로그아웃박스 start-->
			<div class="logoutBox">
				<ul>
					<li tabindex="14"><a href="javascript:fn_logout();"><img src="/images/logout_b.gif" alt="로그아웃" /></a></li>
					<li tabindex="15" class="rBg"><a href="javascript:fn_selectUserInfo();"><img src="/images/memInfoModi_b.gif" alt="회원정보수정" /></a></li>
				</ul>
			</div>
			<!--로그아웃박스 end-->
<%    } else {  %>
			<div class="loginBox">
				<ul>
					<li tabindex="14"><a href="/user/user.do?method=goSgInstall"><img src="/images/login_b.gif" alt="로그인" /></a></li>
					<li tabindex="15"><a href="/user/user.do?method=goPage"><img src="/images/join_b.gif" alt="회원가입" /></a></li>
					<li tabindex="16" class="rBg"><a href="/user/user.do?method=goIdntPswdSrch"><img src="/images/idpwSrch_b.gif" alt="아이디/비밀번호찾기" /></a></li>
				</ul>
			</div>
<%    }    %>
			<!--로그인박스 end-->
		</div>
	</div>
  <form name="logoutForm" method="post">
  	<input type="hidden" name="method" value="showForm">
  	<input type="hidden" name="logout" value="Y">
  </form>
  <form name="userInfoModi" method="post">
  	<input type="hidden" name="method" value="selectUserInfo">
  	<input type="hidden" name="userIdnt" value="<%=sessUserIdnt%>">
  </form>

	<!--로고 및 탑메인메뉴 end-->
</div>
