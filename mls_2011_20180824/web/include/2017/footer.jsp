<%@ page contentType="text/html;charset=euc-kr"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil"%>
<%@ page import="kr.or.copyright.common.userLogin.model.User"%>
<!-- FOOTER str-->
<div id="foot_bor">
	<div class="wrap_menu_tail">
		<div class="inner">
			<div class="menu_tail">
				<ul>
					<li><a href="/main/main.do?method=goUserMgnt" class="first">이용약관</a></li>
					<li><a href="http://www.copyright.or.kr/customer-center/user-guide/privacy-policy/index.do" target="_blank">개인정보처리방침</a></li>
					<li><a href="/main/main.do?method=goHomeCopyright">저작권정책 </a></li>
					<!-- <li><a href="/mlsInfo/guideInfo.jsp">관련사이트안내</a></li> -->
					<li><a href="/board/board.do?menuSeqn=1&amp;page_no=1">자주묻는질문</a></li>
				</ul>
			</div>
			<script type="text/javascript">
					function move(){
						var frm = document.linkForm;
						var url = frm.siteLink.value;
						//alert(url);
 						  if (url == "SITE001" ) {
						    	// 대표 누리집
								$('#footerSiteLinkFrom').attr("action","https://www.copyright.or.kr/ssoLoginMain.do");
								$('#footerSiteLinkFrom').submit();
						    } else if (url == "SITE005" ) {
						    	// 디지털 저작권 거래소
								$('#footerSiteLinkFrom').attr("action","https://www.kdce.or.kr/user/main.do");
								$('#footerSiteLinkFrom').submit();
						    } else if (url == "SITE006" ) {
						    	// 권리자 찾기
								$('#footerSiteLinkFrom').attr("action","https://www.findcopyright.or.kr/ssoMain.do");
								$('#footerSiteLinkFrom').submit();
						    } else if (url == "SITE007" ) {
						    	// OLIS 오픈소스SW 라이선스
								$('#footerSiteLinkFrom').attr("action","https://www.olis.or.kr/membership/ssoOlisLogin.do");
								$('#footerSiteLinkFrom').submit();
						    } else if (url == "SITE008" ) {
						    	// 공유마당
								$('#footerSiteLinkFrom').attr("action","https://gongu.copyright.or.kr");
								$('#footerSiteLinkFrom').submit();
						    } else if (url == "SITE009" ) {
						    	// 저작권 인증
								$('#footerSiteLinkFrom').attr("action","https://cras.copyright.or.kr/front/right/comm/ssoLoginMain.do");
								$('#footerSiteLinkFrom').submit();
						    } else {
								window.open(url,"");
						    }
					}
						function move1(){
						var frm = document.linkForm;
						var url = frm.siteLink1.value;
						if(url == ''){
							alert("관련사이트를 선택해 주세요");
							return;
						}else{
							window.open(url,'');
						}
					}
			</script>
			<form name="linkForm" action="#">
				<input type="submit" style="display:none;" />

				<div class="foot_rt">
					<select title="관련사이트" name="siteLink">
							<option value="SITE001">대표홈페이지</option>
							<option value="SITE005">디지털 저작권거래소</option>
							<option value="SITE006">권리자 찾기</option>
							<option value="http://www.uci.or.kr">UCI(국가디지털콘텐츠식별체계)</option>
							<option value="http://gongu.copyright.or.kr">공유마당</option>
							<option value="SITE007">OLIS(오픈소스SW 라이선스)</option>
							<option value="https://edu-copyright.or.kr">원격교육연수원</option>
							<option value="https://edulife.copyright.or.kr">원격평생교육원</option>
							<option value="https://www.cros.or.kr">저작권 등록</option>
							<option value="https://www.swes.or.kr">온라인 임치</option>
							<option value="https://edu-copyright.or.kr">원격교육연수원</option>
							<option value="SITE009">저작권 인증</option>
					</select>
					<a onclick="move();"><img src="/images/2017/new/btn_move.png" alt="이동" /></a>
					<select title="유관기관" name="siteLink1">
						<option value="">유관기관</option>
						<option value="http://www.komca.or.kr/">한국음악저작권협회</option>
						<option value="http://www.koscap.or.kr/">함께하는음악저작인협회</option>
						<option value="http://www.fkmp.kr/">한국음악실연자연합회</option>
						<option value="http://www.riak.or.kr/">한국음반산업협회</option>
						<option value="http://www.copyrightkorea.or.kr/">한국문예학술저작권협회</option>
						<option value="http://www.copycle.or.kr/">한국복제전송저작권협회</option>
						<option value="http://www.ktrwa.or.kr/">한국방송작가협회</option>
						<option value="http://www.mdak.or.kr/">한국영화배급협회</option>
						<option value="http://www.scenario.or.kr/">한국시나리오작가협회</option>
						<option value="http://www.kfpa.net/">한국영화제작가협회</option>
						<option value="http://www.kbpa.co.kr/">한국방송실연자연합회</option>
						<option value="http://www.kpf.or.kr/">한국언론진흥재단</option>
					</select>
					<a onclick="move1();"><img src="/images/2017/new/btn_move.png" alt="이동" /></a>
					
				</div>
			</form>
		</div>
	</div>
	<form id="footerSiteLinkFrom" method="post" action="#" target="_blank">
	<c:if test="${not empty access_token }">
					<input type="hidden" name="accessToken" value="${access_token}"/>
					<input type="hidden" name="refreshToken" value="${refresh_token}"/>
				</c:if>
</form>
	
	<div id="foot">
		<div class="foot_cen">
			<p class="foot_lf">
				<a href="#none"><img src="/images/2017/new/logo_tail.png" alt="logo" /></a>
			</p>
			<div class="txt">
				대표전화 : 055-792-0129<br />
				52852 경상남도 진주시 충의로 19(LH공사 1,3,5층) 한국저작권 위원회<br />
				Copyright ⓒ 2012 FINDCOPYRIGHT. ALL RIGHTS RESERVED.
			</div>
			
		</div>
		<img src="/images/main/main_40.gif" alt="그림" class="icon" />
	</div>
</div>
<!-- //FOOTER end -->
<!-- FOOTER end -->