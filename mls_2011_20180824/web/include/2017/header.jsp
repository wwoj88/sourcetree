<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page session="true" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ page import="SafeSignOn.*" %>

<!-- 추가된 링크 -->
<link rel="stylesheet" type="text/css" href="/css/2017/new.css"/>
<link type="text/css" rel="stylesheet" href="/css/2012/style.css"/>
  

<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">var Doit = jQuery.noConflict();</script>
<script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" language="javascript" src="/js/2017/jquery.bxslider.js"></script>
<script type="text/javascript" language="javascript" src="/js/2017/script.js"></script>


<% 
String httpVersion = request.getProtocol(); 

if ( httpVersion.equals( "HTTP/1.0" ) ) 
{ 
response.setDateHeader("Expires", 0 ); 
response.setHeader( "Pragma", "no-cache" ); 
} 
else if ( httpVersion.equals( "HTTP/1.1" ) ) 
{ 
response.setHeader("Cache-Control", "no-cache" ); 
} 
%>
<%



String sessUserIdnt = "";
String sessUserName = "";
String sessSsoYn = "";
String sessMail     = "";
String sessMoblPhon = "";
String sessUserDivs = "";
String sessClmsUserId= "";
String sessClmsPwd = "";
String sessSsoUserIdnt= "";
String access_token ="";
String refresh_token="";

User user = SessionUtil.getSession(request);

sessUserIdnt = user.getUserIdnt();
sessUserName = user.getUserName();
sessSsoYn    = user.getSso_loginyn();
sessMail     = user.getMail();
sessMoblPhon = user.getMoblPhon();
sessUserDivs = user.getUserDivs();
access_token = user.getAccessToken();
refresh_token =user.getRefreshToken();
sessClmsUserId = user.getClmsUserIdnt();
sessClmsPwd = user.getClmsPswd();


String loginYn = "N";
if (sessUserIdnt != null) 
    loginYn = "Y";
%>
<script type="text/javascript">
//<![CDATA[


function fn_enterChk(event, index){

  if (event.keyCode == 13) {
    mainSrch(index);
  }
  
}
function goInnerSearch(index) {
  var frm = document.srchForm;
  //console.log(index)
  
  if(index!=0)
  {
    document.getElementsByName("srchWord")[0].blur();

    //frm.worksTitle.value = document.getElementsByName("srchWord")[0].value = " " ;
    //frm.query.value = document.getElementsByName("srchWord")[0].value ;
    //frm.page.value = "1";
    //frm.target.value="total";
    //frm.resrch.value="no";
     switch (index) {
      case "1"    : 
        frm.genreCd.value = "음악" ;
        frm.collection.value = "cf_music,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "2"   : 
        frm.genreCd.value = "어문" ;
        frm.collection.value = "cf_literature,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "3"  : 
        frm.genreCd.value = "방송대본" ;
        frm.collection.value = "cf_scenario,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "4"  : 
        frm.genreCd.value = "영화" ;
        frm.collection.value = "cf_movie,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "5"  : 
        frm.genreCd.value = "방송" ;
        frm.collection.value = "cf_broadcast,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";         
          break;
      case "6"  : 
        frm.genreCd.value = "뉴스" ;
        frm.collection.value = "cf_news,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;    
      case "7"  : 
        frm.genreCd.value = "미술" ;
        frm.collection.value = "cf_art,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "8"  : 
        frm.genreCd.value = "이미지" ;
        frm.collection.value = "cf_image,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "9"  : 
        frm.genreCd.value = "사진" ;
        frm.collection.value = "cf_photo,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "99"  : 
        frm.genreCd.value = "기타" ;
        frm.collection.value = "cf_etc,reg_copyright,dg_music1,dg_literature,dg_movie,dg_news";
          break;
      case "999"  : 
       // console.log("default call");
        frm.genreCd.value = "total" ;
          break;
      case "9999"  : 
         // console.log("default call");
          frm.genreCd.value = "total" ;
          frm.menuFlag.value = "N";
            break;
      default    : 
        frm.genreCd.value = "" ;
            break;
    } 
    
    //frm.submit();
  }
  
  /* if(document.getElementsByName("srchWord")[0].value == ""){
    alert("검색어를 입력하세요.");
    document.getElementsByName("srchWord")[0].focus();
    return;
  } */
  
  document.getElementsByName("srchWord")[0].blur();

  frm.worksTitle.value = document.getElementsByName("srchWord")[0].value ;
  //frm.query.value = document.getElementsByName("srchWord")[0].value ;
  frm.query.value = '${query}';
  frm.action = "/search/search.do";
  frm.submit(); 

}

function mainSrch(index) {
  var frm = document.srchForm;
  frm.genreCd.value = "total" ;
  //frm.menuFlag.value = "N";
  document.getElementsByName("srchWord")[0].blur();

  frm.worksTitle.value = document.getElementsByName("srchWord")[0].value ;
  frm.query.value = document.getElementsByName("srchWord")[0].value ;
  frm.action = "/search/search.do";
  frm.submit(); 

}

/* 화면 확대 축소 시작 IE 전용 */
var nowZoom = 100; // 현재비율
var maxZoom = 200; // 최대비율(500으로하면 5배 커진다)
var minZoom = 80; // 최소비율

//화면 키운다.
function zoomIn() {
  if (nowZoom < maxZoom) {
      nowZoom += 10; //25%씩 커진다.
  } else {
      return;
  }
  document.body.style.zoom = nowZoom + "%";
  quickSetPos(nowZoom);
  
}

//화면 줄인다.
function zoomOut() {
    if (nowZoom > minZoom) {
      nowZoom -= 10; //25%씩 작아진다.
    } else {
      return;
    }
    document.body.style.zoom = nowZoom + "%";
    quickSetPos(nowZoom);
}

//초기화
function zoom() {
  document.body.style.zoom = 100 + "%";
}

//로그인 절차 수행
function fn_logout(){
  var frm = document.logoutForm;

  frm.action = "/userLogin/userLogin.do";
  frm.submit();
}

function fn_selectUserInfo() {
  var frm = document.userInfoModi;

  frm.action = "/user/user.do";
  frm.submit();
}

// 로그인유무  return Y/N
function GetLoginYN() {
    
    return '<%=loginYn%>' ;
}

$(function(){
  getSrchList();
})

function getSrchList(){
  var url = "/main/main.do?method=getSrchList";
  //비동기 통신을 하여 json타입으로 호출한다. 
  
  /* $.ajax({ 
    type: "POST",
    url: url , 
    //data : datas, 
    dataType : 'json' , 
    success: function(data) {
      srchListSet(data);
      
    } 
  });  */
  
  var xhr = new XMLHttpRequest();
  
  xhr.onload = function(){
    if(xhr.status==200)
    {
    //  console.log(xhr.responseText);
      srchListSet(xhr.responseText)
    }
  }
  xhr.open('post',url,true);
  xhr.send(null);
}

function srchListSet(data){
  var jsonData=JSON.parse(data);
  var searchList = jsonData["searchWordList"];
  var appendString ="<ul>";
   for(var i = 0 ; i < searchList.length ; i++)
  {
     appendString +="<li><strong class='num' style='color:#FF5700;'>"+searchList[i].ROWNUM+"<\/strong>&nbsp;&nbsp;"+searchList[i].SRCH_WORD+"<\/li>";
  }
  appendString +="<\/ul>";
  document.getElementById('quick_n').childNodes[3].innerHTML=appendString;
}

//]]>
</script>
<div id="skipNav">
  <a href="#contentBody">본문 바로가기</a>
  <a href="#gnb_n">주메뉴 바로가기</a>
</div>
<div id="header">
  <div class="header_btm">
    <div class="logo">
      <h1><a href="/main/main.do"><img src="/images/2017/new/main_logo_n.png" alt="한국저작권위원회 권리자찾기 정보시스템" /></a></h1>
    </div>
    <div class="hearinp">
        <input id="srchWord2" title="검색" name="srchWord" value="" type="text" onkeydown="javascript:fn_enterChk(event, 999);" /><a href="#" onclick="javascript:mainSrch(999);">
        <img src="/images/2017/new/search.png" alt="검색" /></a>
    </div>
    <div class="headuli">
          
<%    if (sessUserIdnt == null) {  %>
      <ul>
        <li class="none"><a href="/user/user.do?method=goSgInstall">로그인</a></li>
        <li><a href="/user/user.do?method=goPage">회원가입</a></li>
        <li class="none">
        <a href="#" onclick="javascript:zoomIn()"><img src="/images/2017/new/txt_plus.png" alt="글씨확대" /></a><a href="#" onclick="javascript:zoomOut()"><img src="/images/2017/new/txt_minus.png" alt="글씨축소" /></a>
        </li>
      </ul>
<%    } else if( sessSsoYn != null) {  %>
      <ul>
      <li class="none"><a href="#1" onclick="javascript:fn_logout();">로그아웃</a></li>
      <!-- <li class="none"><a href="/statBord/statBo06List.do?bordCd=6">마이페이지</a></li>-->
      <!-- <li class="none"><a href="/myStat/statRsltInqrList.do">마이페이지</a></li> -->
      <li class="none"><a href="/statBord/statBo01ListMy.do?bordCd=1">마이페이지</a></li>
      
      <li><a href="https://oneid.copyright.or.kr/login/login.do">회원정보수정</a></li>
      <a href="#" onclick="javascript:zoomIn()"><img src="/images/2017/new/txt_plus.png" alt="글씨확대" /></a><a href="#" onclick="javascript:zoomOut()"><img src="/images/2017/new/txt_minus.png" alt="글씨축소" /></a>
      </ul>
       
<%    }else{    %>
      <ul>
      <li class="none"><a href="#1" onclick="javascript:fn_logout();">로그아웃</a></li>
      <!-- <li class="none"><a href="/statBord/statBo06List.do?bordCd=6">마이페이지</a></li>-->
      <!-- <li class="none"><a href="/myStat/statRsltInqrList.do">마이페이지</a></li> -->
      <li class="none"><a href="/statBord/statBo01ListMy.do?bordCd=1"">마이페이지</a></li>
      
      <li><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보수정</a></li>
      <a href="#" onclick="javascript:zoomIn()"><img src="/images/2017/new/txt_plus.png" alt="글씨확대" /></a><a href="#" onclick="javascript:zoomOut()"><img src="/images/2017/new/txt_minus.png" alt="글씨축소" /></a>
      </ul> 
      
  <%    }   %>    
        
    
      <!-- //top end -->
    </div>
    <p class="clear">
    </p>
  </div>
  <p class="clear">
  </p>
       <form name="srchForm" method="post" action="/search/search.do">
              
          <input type="hidden" name="collection" value="ALL"/>
          <input type="hidden" name="gubunVal" value="F"/>
          <input type="hidden" name="worksTitle" value=""/> 
          <!-- <input type="hidden" name="target" value=""/> -->
          <input type="hidden" name="genreCd" title="장르" id="genreCd"/>
          <input type="hidden" name="query" value="${query}"/>
          <input type="hidden" name="menuFlag" value=""/>
          <!--  -->
          <input type="hidden" name="resultcount" value="10"/>
          <input type="hidden" name="page" value=""/>
          <input type="hidden" name="mode" value=""/>
          <input type="hidden" name="arr_range" value="all"/>
          <input type="hidden" name="resrch" value=""/>             
          <input type="hidden" id="urlCd" name="urlCd" value="01"/>
         </form>
          <form name="logoutForm" method="post" action="#">
            <input type="submit" style="display:none;"/>
            <input type="hidden" name="method" value="showForm"/>
            <input type="hidden" name="logout" value="Y"/>
          </form>
          
          <form name="userInfoModi" method="post" action="#">
            <input type="submit" style="display:none;"/>
            <input type="hidden" name="method" value="selectUserInfo"/>
            <input type="hidden" name="userIdnt" value="<%=sessUserIdnt%>"/>
          </form>
          
          <form name="ssoForm" method="post" action="#">
            <input type="submit" style="display:none;"/>
            <input type="hidden" name="ssotoken" value=""/>
            <input type="hidden" name="ssotoken2" value=""/>
          </form>
</div>
<!--/* 2017-10-30 -->
<div class="bg_2c65aa">
  <div class="wrap_gnb">
    <div id="gnb_n">
      <ul>
        <li>
          <a href="/mlsInfo/guideInfo.jsp" name="gnb_n_title">권리자 찾기 소개</a>
          <ul class="depth2">
            <li><a href="/mlsInfo/guideInfo.jsp">권리자 찾기 소개</a></li>
            <li><a href="/mlsInfo/contactUs.jsp">이용문의</a></li>
          </ul>
        </li>
        <li>
          <a href="javascript:goInnerSearch('999')" name="gnb_n_title">분야별 권리자 찾기</a>
          <ul id="searchMenu" class="depth2">
            <li><a href="#"onclick="javascript:goInnerSearch('999'); return false;">전체</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('1'); return false;">음악</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('2'); return false;">어문</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('3'); return false;">방송대본</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('4'); return false;">영화</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('5'); return false;">방송</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('6'); return false;">뉴스</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('7'); return false;">미술</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('8'); return false;">이미지</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('9'); return false;">사진</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('99'); return false;">기타</a></li>
            
          </ul>
        </li>
        <li>
          <a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1" name="gnb_n_title">미분배 보상금 저작물 찾기</a>
          <ul class="depth2">
            <li><a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1">미분배 보상금 대상 저작물 확인</a></li>
            <li><a href="/mlsInfo/inmtInfo02.jsp">소개 및 이용방법</a></li>
          </ul>
        </li>
        <li>
          
          <a href="/mlsInfo/liceSrchInfo07.jsp" name="gnb_n_title">법정허락</a>
          <ul class="depth2">
            <li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
            <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
            <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
            <li><a href="/stat/statSrch.do">법정허락 기승인 신청 서비스</a></li>
            <!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
            <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>

          </ul>
        </li>
        <li>
          <a href="/mlsInfo/useLicestatInfo02.jsp" name="gnb_n_title">관리저작물 등록 안내</a>
          <ul class="depth2">
            <li><a href="/mlsInfo/useLicestatInfo02.jsp">위탁관리저작물 정보 등록 안내</a></li>
            <li><a href="/mlsInfo/useLicestatInfo03.jsp">위탁관리저작물 정보 등록기관 현황</a></li>
            <!-- <li><a href="/mlsInfo/useLicestatInfo01.jsp">미분배 보상금 대상 저작물 등록 안내 </a></li> -->
          </ul>
        </li>
        <li>
          <a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1" name="gnb_n_title">알림마당</a>
          <ul class="depth2">
            <li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1">공지사항</a></li>
            <li><a href="/mlsInfo/linkList01.jsp">자료실</a></li>
            <li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">자주 묻는 질문</a></li>
            <li><a href="/eventMgnt/eventList.do">이벤트</a></li>
            <li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!--/* gnb_n -->
    <a href="#" id="btn_sitemap" class="btn_sitemap"><img src="/images/2017/new/icon_sitemap.png" alt="" class="icon"/>전체보기</a>
    <div class="list_sitemap">
      <div class="cell cell1">
        <h2 class="tit">권리자 찾기 소개</h2>
        <ul>
          <li><a href="/mlsInfo/guideInfo.jsp">권리자 찾기 소개</a></li>
          <li><a href="/mlsInfo/contactUs.jsp">이용문의</a></li>
          <!-- <li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li> -->
        </ul>
      </div>
      <div class="cell cell2">
        <h2 class="tit">분야별 권리자 찾기</h2>
        <ul id="searchs">
          <li><a href="#"onclick="javascript:goInnerSearch('999'); return false;">전체</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('1'); return false;">음악</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('2'); return false;">어문</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('3'); return false;">방송대본</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('4'); return false;">영화</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('5'); return false;">방송</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('6'); return false;">뉴스</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('7'); return false;">미술</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('8'); return false;">이미지</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('9'); return false;">사진</a></li>
            <li><a href="#"onclick="javascript:goInnerSearch('99'); return false;">기타</a></li>
        </ul>
      </div>
      <div class="cell cell3">
        <h2 class="tit">미분배 보상금 저작권 찾기</h2>
        <ul>
          <li><a href="/inmtPrps/inmtPrps.do?mNum=4&sNum=0&leftsub=0&srchDIVS=1">미분배 보상금 대상 저작물 확인</a></li>
          <li><a href="/mlsInfo/inmtInfo02.jsp">소개 및 이용방법</a></li>
        </ul>
      </div>
      <div class="cell cell4">
        <h2 class="tit">법정허락</h2>
          <ul class="depth2">
            <!-- <li><a href="javascript:goInnerSearch('9999')">상당한 노력 신청 서비스</a></li>
            <li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
            <!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
            <li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
            <li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
            <!-- <li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
            <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
            <li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
            <!-- <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li> -->
            <!-- <li><a href="/srchList.do">상당한 노력 신청 방법</a></li> -->
          </ul>
      </div>
      <div class="cell cell5">
        <h2 class="tit">관리저작물 등록안내</h2>
        <ul>
          <li><a href="/mlsInfo/useLicestatInfo02.jsp">위탁관리저작물 정보 등록 안내</a></li>
          <li><a href="/mlsInfo/useLicestatInfo03.jsp">위탁관리저작물 정보 등록기관 현황</a></li>
          <li><a href="/mlsInfo/useLicestatInfo01.jsp">미분배 보상금 대상 저작물 등록 안내 </a></li>
        </ul>
      </div>
      <div class="cell cell6">
        <h2 class="tit">알림마당</h2>
        <ul>
          <li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=4&amp;page_no=1">공지사항</a></li>
          <li><a href="/mlsInfo/linkList01.jsp">자료실</a></li>
          <li><a href="/board/board.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;menuSeqn=1&amp;page_no=1">자주 묻는 질문</a></li>
          <li><a href="/eventMgnt/eventList.do">이벤트</a></li>
          <li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
        </ul>
      </div>
      <a href="#" class="btn_close">닫기 <strong>X</strong></a>
    </div>
    <!--/* list_sitemap -->
  </div>
  <!--/* wrap_gnb -->
</div>


<!-- 퀵메뉴 시작 -->
<div id="quick_n">
  <h2 class="tit">최근 검색 순위</h2>
  <div class="inner">
  </div>
</div>
  <!-- 퀵메뉴 끝 -->
