<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
%>
<div class="left">
	<ul id="sub_lnb">
	<li id="lnb1"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1"><img src="/images/2011/content/sub_lnb0801_off.gif" title="신청현황" alt="신청현황" /></a>
		<ul>
		<li id="lnb11"><a href="/rsltInqr/rsltInqr.do?DIVS=10&amp;page_no=1">저작권찾기</a></li>
		<li id="lnb12"><a href="/rsltInqr/rsltInqr.do?DIVS=20&amp;page_no=1">보상금</a></li>
		<!--<li id="lnb13"><a href="/myStat/myStat.do?method=statRsltInqrList">법정허락</a></li>
		--></ul>
	</li>
	<li id="lnb2"><a href="/worksPriv/worksPriv.do?page_no=1"><img src="/images/2011/content/sub_lnb0802_off.gif" title="저작권콘텐츠" alt="저작권콘텐츠" /></a>
		<ul>
		<li id="lnb21"><a href="/worksPriv/worksPriv.do?page_no=1">나의 저작물</a></li>
		<li id="lnb22"><a href="/board/board.do?mNum=5&amp;sNum=0&amp;leftsub=2&amp;menuSeqn=9&amp;page_no=1&amp;rgstIdnt=<%=sessUserIdnt%>">나의 질문과 답</a></li>
		</ul>
	</li>
	<li id="lnb3"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>"><img src="/images/2011/content/sub_lnb0803_off.gif" title="회원정보" alt="회원정보" /></a>
		<ul>
		<li id="lnb31"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=U&amp;userIdnt=<%=sessUserIdnt%>">회원정보수정</a></li>
		<li id="lnb32"><a href="/user/user.do?method=selectUserInfo&amp;DIVS=D&amp;userIdnt=<%=sessUserIdnt%>">회원탈퇴</a></li>
		</ul>
	</li>
	</ul>
</div>