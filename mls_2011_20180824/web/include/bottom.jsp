<%@ page contentType="text/html;charset=euc-kr" %>
<%@ page import="java.net.*" %>

<div id="footer">
		<div class="logo">
			<h1><a href="http://www.copyright.or.kr" target="_blank">저작권로고</a></h1>
		</div>
		<div class="all">
			<div class="navi">
				<ul>
					<li><a href="/main/main.do?method=goUserMgnt">이용약관</a></li>
					<li><a href="/main/main.do?method=goPersMgnt">개인보호정책</a></li>
					<li><a href="/main/main.do?method=goHomeCopyright">저작권보호정책</a></li>
					<li><a href="/main/main.do?method=goSiteInfo1">내권리찾기 소개</a></li>
					<!--li><a href="#">사이트맵</a></li-->
					<li><a href="/board/board.do?menuSeqn=1&page_no=1">FAQ</a></li>
					<li><a href="/board/board.do?menuSeqn=2&page_no=1">Q&amp;A</a> </li>
				</ul>
			</div>
			<div class="copyright">Copyright (C) 2007 한국저작권위원회. All rights reserved.</div>
		</div>
</div>