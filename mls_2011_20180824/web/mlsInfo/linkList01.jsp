<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>서비스 안내자료 | 홍보자료 | 저작권 찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->

function fn_fileDownLoad(filePath, fileName, realFileName) {
	
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 
 function fn_View1(){

      window.open("https://www.youtube.com/watch?v=6k8AHSQgcUY","","top=100, left=100, status=yes,menubar=yes,toolbar=yes,fullscreen=yes,resizable=yes")     

   }
function fn_View2(){

      window.open("https://www.youtube.com/watch?v=eqRh5bWIEZ8","","top=100, left=100, status=yes,menubar=yes,toolbar=yes,fullscreen=yes,resizable=yes")     

   }
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader5.jsp" /> --%>
		<!-- 2017 주석처리 -->
		<!-- <script type="text/javascript">initNavigation(5);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
			<form name="form1" method="post" action="#">
								<input type="hidden" name="filePath"> 
								<input type="hidden" name="fileName"> 
								<input type="hidden" name="realFileName">
								<input type="hidden" name="action_div"> 
								<input type="submit" style="display: none;">
			</form>	
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">홍보자료</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/linkList01.jsp" class="on">서비스 안내자료</a></li>
				<li><a href="/main/main.do?method=bannerList">배너 자료</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				홍보자료
				&gt;
				<span class="bold">서비스 안내자료</span>
			</div>
			<div class="con_rt_hd_title">서비스 안내자료</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h2 class="sub_con_h2">서비스 안내자료</h2>
				<div class="mar_tp50 sub05_bg_fafafa">
					<div>
						<div class="float_lf">
							<p class="sub_blue_point_bg">저작권 찾기 CF</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_50.gif" alt="저작권 찾기 CF" /><br />
								<a href="#1" onclick="javascript:fn_View2()" class="pop_check mar_tp10">바로보기</a>
							</div>
						</div>
						<div class="float_rt">
							<p class="sub_blue_point_bg">안내동영상</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_51.gif" alt="안내동영상" /><br />
								<a href="#1" onclick="javascript:fn_View1()" class="pop_check mar_tp10">바로보기</a>
							</div>
						</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp40">
						<div class="float_lf">
							<p class="sub_blue_point_bg">저작권 찾기 서비스 안내 리플릿</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_52.gif" alt="저작권 찾기 서비스 안내 리플릿" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','저작권 찾기 서비스 안내 리플릿.pdf','01.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/CopyrightSearchGuideLeaflet.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<div class="float_rt">
							<p class="sub_blue_point_bg">저작권 찾기 서비스 안내 리플릿 - 이용자편</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_52_1.gif" alt="저작권 찾기 서비스 안내 리플릿" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','저작권 찾기 서비스 안내 리플릿 - 이용자편.pdf','02.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/CopyrightSearchGuideLeafletUser.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<p class="clear"></p>
<!--
 						<div class="float_rt">
							<p class="sub_blue_point_bg">법정허락제도 안내 리플릿</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_53.gif" alt="법정허락제도 안내 리플릿" /><br />
								<a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','법정허락제도 안내 리플릿.pdf','법정허락제도 안내 리플릿.pdf')" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<p class="clear"></p>
-->
					</div>
					<div class="mar_tp40">
						<div class="float_lf">
							<p class="sub_blue_point_bg">저작권 찾기 서비스 안내 리플릿 - 창작자편</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_52_2.gif" alt="저작권 찾기 서비스 안내 리플릿" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','저작권 찾기 서비스 안내 리플릿 - 창작자편.pdf','03.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/Copyright SearchGuideLeafletCreator.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
 						<div class="float_rt">
							<p class="sub_blue_point_bg">법정허락제도 안내 리플릿</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_53.gif" alt="법정허락제도 안내 리플릿" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','법정허락제도 안내 리플릿.pdf','04.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/LegalPermissionSystem.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp40">
						<div class="float_lf">
							<p class="sub_blue_point_bg">위탁관리저작물 등록안내서</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_54.gif" alt="위탁관리저작물 등록안내서" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','위탁관리저작물 등록안내서.pdf','05.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/ConsignedAdministrationProductionsGuide.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<div class="float_rt">
							<p class="sub_blue_point_bg">디지털저작권거래소 소개자료</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_55.gif" alt="디지털저작권거래소 소개자료" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','디지털저작권거래소 소개자료.pdf','06.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/DigitalCopyrightExchangeIntroductionData.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp40">
						<div class="float_lf">
							<p class="sub_blue_point_bg">디지털저작권거래소 소개 포스터</p>
							<div class="align_cen mar_tp10">
								<img src="/images/sub_img/sub_56.gif" alt="디지털저작권거래소 소개 포스터" /><br />
								<!-- <a href="#1" onclick="javascript:fn_fileDownLoad('/home/right4me/web/upload/form/','디지털저작권거래소 사업 소개 포스터_1030.pdf','07.pdf')" class="pop_check mar_tp10">다운로드</a> -->
								<a href="http://www.findcopyright.or.kr/upload/form/DigitalCopyrightExchangeIntroductionPoster.pdf" class="pop_check mar_tp10">다운로드</a>
							</div>
						</div>
						<p class="clear"></p>
					</div>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
	  	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	    	
</body>
</html>