<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 승인 신청 소개 및 이용방법 - 소개 | 저작권자 찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader2.jsp" /> --%>
<!-- 		<script type="text/javascript">initNavigation(3);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		    	    <!-- 래프 -->
					<div class="con_lf">
						<div class="con_lf_big_title">저작권자 찾기</div>
						<ul class="sub_lf_menu">
							<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li>
							<li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li>
							<li><a href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
							<li><a href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
							<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
							<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
							<li><a class="on" href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
							<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
							<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
						</ul>
					</div>
					<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="con_rt" id="contentBody">
			      	<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						저작권자 찾기
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">소개 및 이용방법</span>
					</div>
			      		<div class="con_rt_hd_title">소개 및 이용방법</div>
			      	
			      	<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h1 class="sub_con_h1 float_lf">법정허락 승인 신청</h1>
				<div class="float_rt"><a href="/mlsInfo/statInfo02.jsp" class="method_bg_25869b" style="color: #ffffff;">자세한 이용방법 바로가기</a></div>
				<p class="clear"></p>
				<h2 class="color_2c65aa mar_tp20">다른 사람의 저작물(실연·음반·방송 및 데이터베이스)를 이용하기 위해서</h2>
				<p class="mar_tp5">① 상당한 노력을 기울였어도 권리자를 알 수 없거나 권리자의 거소를 알 수 없는 경우</p>
				<p class="mar_tp5">② 특별한 목적으로 저작물을 이용하고자 하였으나 권리자와 협의가 성립되지 않아 이용할 수 없는 경우 위의 경우에 해당할 때 </p>
				<p class="mar_tp5">&nbsp;&nbsp;&nbsp;&nbsp;저작권법에 의하여 권리자를 대신하여 저작물 이용을 승인하는 제도입니다.</p>
				<h1 class="sub_con_h1 mar_tp40">법정허락 대상</h1>
				<div class="sub_con_h2 mar_tp30">저작재산권자 불명 저작물(저작권법 제50조)</div>
					<p class="mar_tp5">상당한 노력을 기울였어도 공표된 저작물(외국인의 저작물 제외)의 저작재산권자나 그의 거소를 알 수 없는 경우</p>
				<div class="sub_con_h2 mar_tp30">공표된 저작물의 방송(저작권법 제51조)</div>
					<p class="mar_tp5">공표된 저작물을 공익상 필요에 의하여 방송하고자 하는 방송사업자가 그 저작재산권자와 협의하였으나 협의가 성립되지 아니한 경우</p>
				<div class="sub_con_h2 mar_tp30">판매용 음반의 제작(저작권법 제52조)</div>
					<p class="mar_tp5">판매용 음반이 우리나라에서 처음으로 판매되어 3년이 경과한 경우 그 음반에 녹음된 저작물을 녹음하여 다른 판매용 음반을 제작하고자 하는 자가 그 저작재산권자와 협의하였으나 협의가 성립되지 아니한 경우</p>
				<div>
					<div class="float_lf">
						<div class="sub_con_h2 mar_tp30">실연·음반 및 방송 이용(저작권법 제89조)</div>
							<p class="mar_tp5">실연·음반 및 방송 이용에 관하여 저작권법 제50조 내지 제52조까지의 규정을 준용</p>
						<div class="sub_con_h2 mar_tp30">데이터베이스의 이용(저작권법 제97조)</div>
							<p class="mar_tp5">데이터베이스 이용에 관하여 저작권법 제50조 및 제51조까지의 규정을 준용</p>
					</div>
					<div class="float_rt mar_tp30"><img src="/images/sub_img/sub_20.gif" alt="그림" /></div>
					<p class="clear"></p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">법정허락 신청 수수료</h1>
				<p class="mar_tp20">1건당 1만원(보상금 공탁은 별도)</p>
				<h1 class="sub_con_h1 mar_tp40">법정허락 처리 절차</h1>
				<h2 class="sub_con_h2 mar_tp30">미분배 보상금 저작물 및 저작재산권자 불명 저작물 대상 신청</h2>
				<!-- <img class="mar_tp30" src="/images/sub_img/sub_21.gif" alt="미분배 보상금 저작물 및 저작재산권자 불명 저작물 대상 신청" /> -->
				<img class="mar_tp30" src="/images/sub/sub_tu6.jpg" alt="미분배 보상금 저작물 및 저작재산권자 불명 저작물 대상 신청" />
				<h2 class="sub_con_h2 mar_tp30">방송 및 판매용 음반의 제작을 위한 신청</h2>
				<!-- <img class="mar_tp30" src="/images/sub_img/sub_22.gif" alt="방송 및 판매용 음반의 제작을 위한 신청" /> -->
				<img class="mar_tp30" src="/images/sub/sub_tu7.jpg" alt="방송 및 판매용 음반의 제작을 위한 신청" />
				<div class="pad_lf20 mar_tp20">
					<p>※ 승인통지를 받은 이후에는 저작재산권자가 보상금을 수령할 수 없는 경우 보상금을 공탁하고 공탁사실 공고</p>
					<p class="color_ff6000 mar_tp5">※ 실연·음반·방송 이용은 저작권법 제50조내지 제52조까지의 규정을 준용, 데이터베이스 이용은 저작권법 제50조 및 </p>
					<p class="color_ff6000 mar_tp5">&nbsp;&nbsp;&nbsp;&nbsp; 제51조까지의 규정을 준용</p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">법정허락 처리 절차</h1>
				<p class="word_dian_bg mar_tp5">승인 신청서(승인 신청 명세서 첨부)</p>
				<p class="word_dian_bg mar_tp5">보상금 산정내역서</p>
				<div class="word_dian_bg mar_tp5">해당 저작물 등이 공표되었음을 밝힐 수 있는 서류
					<p class="color_ff6000 mar_tp5">※ ‘공표’는 저작물을 공연, 공중송신 또는 전시 그 밖의 방법으로 공중에게 공개하는 경우와 저작물을 발행하는 경우를 말함</p>
					<p class="color_ff6000 mar_tp5">&nbsp;&nbsp;&nbsp;&nbsp;(저작권법 제2조 제25호)</p>
				</div>
				<p class="word_dian_bg mar_tp5">저작재산권자, 저작인접권자 또는 데이터베이스 제작자나 그의 거소를 알 수 없음을 밝힐 수 있는 서류</p>
				<p class="word_dian_bg mar_tp5">협의에 관한 경과 서류(협의가 성립되지 아니하여 승인 신청을 하는 경우에 한정)</p>
				<div class="word_dian_bg mar_tp5">해당 음반이 우리나라에서 판매되어 3년이 경과하였음을 밝힐 수 있는 서류
					<p class="mar_tp5">(저작권법 제52조 및 제89조에 따라 신청하는 경우에 한정)</p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">관련 서식 다운로드</h1>
				<table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="*"/>
						<col width="13%"/>
						<col width="13%"/>
						<col width="13%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">서식</th>
							<th scope="col">다운로드</th>
							<th scope="col">작성예시</th>
							<th scope="col" class="last_bor0">다운로드</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="align_lf pad_lf20 pad_rt20">이용 승인신청서</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5329&amp;brdctsfileno=1762"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
							<td>작성예시</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5329&amp;brdctsfileno=1867"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
						</tr>
						<tr>
							<td class="align_lf pad_lf20 pad_rt20">이용 승인신청명세서</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5292&amp;brdctsfileno=1732"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
							<td>작성예시</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5292&amp;brdctsfileno=1864"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
						</tr>
						<tr>
							<td class="align_lf pad_lf20 pad_rt20">저작(권)자 미등록확인서</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5333&amp;brdctsfileno=1765"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="align_lf pad_lf20 pad_rt20">법정허락 신청저작물 이의신청서 및 저작권자찾기 정보시스템 저작권자 조회내용 이의신청서</td>
							<td><a href="http://www.copyright.or.kr/customer-center/download-service/customer-support-form/download.do?brdctsno=5334&amp;brdctsfileno=1766"><img src="/images/sub_img/sub_23.gif" alt="PDF" /></a></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<h1 class="sub_con_h1 mar_tp40">문의</h1>
				<div class="bg_f8f8f8 mar_tp30">
					<div class="word_dian_bg">내방 및 우편
						<p>주소: 경남 진주시 충의로(LH공사) 5층 심의조사팀</p>
					</div>
					<p class="word_dian_bg">문의전화: 055-792-0085, 팩스: 055-792-0079</p>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
					<!-- //section -->

				<!-- //주요컨텐츠 end -->
	    		<!-- //content -->
	    		<!-- 다운로드내부처리 처리용 -->
	    		<div style="height: 0px;">
					<iframe name="process" width="1" height="1" frameborder="0" scrolling="no" title="다운로드내부처리 아이프레임"></iframe>
				</div>
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	<!-- //CONTAINER --> 

</body>
</html>