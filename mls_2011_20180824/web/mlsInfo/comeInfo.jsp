<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기 서비스란? | 저작권찾기 소개 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--

//-->
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">저작권 찾기 소개</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/guideInfo.jsp">저작권 찾기 서비스란?</a></li>
				<li><a href="/mlsInfo/contactUs.jsp">이용문의</a></li>
				<li><a href="/mlsInfo/comeInfo.jsp" class="on">찾아오시는 길</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				저작권 찾기 소개
				&gt;
				<span class="bold">찾아오시는 길</span>
			</div>
			<div class="con_rt_hd_title">찾아오시는 길</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h1 class="sub_con_h1">한국저작권위원회</h1>
				<div class="mar_tp20"><img src="/images/sub_img/sub_map01.jpg" alt="지도" /></div>
				<h2 class="sub_con_h2 mar_tp20">주소</h2>
				<div class="pad_lf20 mar_tp15">
					<p>경상남도 진주시 충의로 19(충무공동) LH공사 1,3,5층</p>
					<p class="mar_tp5">대표전화 : 055)792-0000  /  팩스번호 : 055)792-0019</p>
				</div>
				<h2 class="sub_con_h2 mar_tp20">교통이용안내</h2>
				<div class="pad_lf20 mar_tp15">
					<p>버스 : 110번, 111번. 152번, 296번</p>
					<p class="mar_tp5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;남동발전/LH(한국토지주택공사) 1단지 하차</p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">한국저작권위원회</h1>
				<div class="mar_tp20"><img src="/images/sub_img/sub_map02.jpg" alt="지도" /></div>
				<h2 class="sub_con_h2 mar_tp20">주소</h2>
				<div class="pad_lf20 mar_tp15">
					<p>서울특별시 용산구 후암로 107(동자동) 게이트웨이빌딩 5, 16층</p>
					<p class="mar_tp5">대표전화 : 02)2669-0010</p>
				</div>
				<h2 class="sub_con_h2 mar_tp20">교통이용안내</h2>
				<div class="pad_lf20 mar_tp15">
					<p>지하철   1, 4호선 서울역 11번 출구/지하 아케이드와 연결</p>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	    	
	    		    	

</body>
</html>