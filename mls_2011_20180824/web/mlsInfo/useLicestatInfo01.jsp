<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>미분배 보상금 관리저작물 등록안내 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader4.jsp" /> --%>
		<!-- 2017 주석처리 -->
<!-- 		<script type="text/javascript">initNavigation(4);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title" style="padding:23px 0;">관리저작물<br />등록 안내</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/useLicestatInfo01.jsp" class="on">미분배 보상금 대상 저작물 등록</a></li>
				<li><a href="/mlsInfo/useLicestatInfo02.jsp">위탁관리저작물 등록</a></li>
				<li><a href="/mlsInfo/useLicestatInfo03.jsp">위탁관리저작물 정보 등록기관 현황</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				관리저작물 등록 안내
				&gt;
				<span class="bold">미분배 보상금 대상 저작물 등록</span>
			</div>
			<div class="con_rt_hd_title">미분배 보상금 대상 저작물 등록</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<!-- 
				<div class="bg_f8f8f8">
					<div>
						<div class="float_lf">
							<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="inp1" /><label for="inp1">전체</label>
							<input type="checkbox" id="inp2" /><label for="inp2">저작물명</label>
							<input type="checkbox" id="inp3" /><label for="inp3">저작권자</label>
						</div>
						<div class="float_rt">
							<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
							<select>
								<option selected="selected">정확도순</option>
								<option>정확도</option>
								<option>정확</option>
							</select>&nbsp;&nbsp;&nbsp;
							<select>
								<option selected="selected">내림차순</option>
								<option>내림차</option>
								<option>내림</option>
							</select>
						</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp10">
						<select title="김색 족근을 선택 해주세요">
							<option selected="selected">통합검색</option>
							<option>정확도</option>
							<option>정확</option>
						</select>&nbsp;&nbsp;&nbsp;
						<select title="김색 족근을 선택 해주세요">
							<option selected="selected">전체</option>
							<option>내림차</option>
							<option>내림</option>
						</select>
						<input type="text" title="김색 내용을 입럭 해주세요" style="padding:3px;width:57%;" /><a href="#none" class="search_button">검색</a>
						<input type="checkbox" id="inp4" /><label for="inp4">결과 내 검색</label>
					</div>
				</div>
				 -->
				<div class="sub01_con_bg4_tp mar_tp30"></div>
				<div class="sub01_con_bg4">
					<h3 style="font-size:22px;">제출근거 및 목적</h3>
					<p class="mar_tp20">보상금 분배 공고 3년이 경과한 미분배 보상금 대상 저작물의 저작권자 찾기 위한 상당한 노력을 문화체육관광부 장관이</p>
					<p class="mar_tp5">대신하여 미분배 보상금 대상 저작물의 합법적인 이용을 촉진하고 미분배 보상금의 분배 활성화를 도모하고자 함</p>
				</div>
				<h2 class="sub_con_h2 mar_tp30">대상단체</h2>
				<p class="mar_tp10">한국음악실연자연합회, 한국음반산업협회, 한국복제전송저작권협회</p>
				<h2 class="sub_con_h2 mar_tp30">등록절차</h2>
				<div class="mar_tp30">
					<span class="sub04_img_bg w145">회원가입<br />(로그인)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w145">저작물 등록<br />(매년)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w145">변경 등록 <br />(매월 10일)</span>
				</div>
				<p class="color_2c65aa mar_tp10">※  최초 등록: 2008. 12. 31. 이전 분배공고 대상 저작물</p>
				<p class="color_2c65aa mar_tp5">※  이후 등록: 3년 경과된 미분배 보상금 저작물, 매년 3월말까지 등록 </p>
				<p class="color_2c65aa mar_tp5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;예) 2009년 저작물: 2013. 3. 31까지 등록</p>
				<h2 class="sub_con_h2 mar_tp30">등록방법</h2>
				<div class="mar_tp30">
					<span class="sub04_img_bg" style="width: 266px;">관리자 시스템 회원가입<br />(www.findcopyright.or.kr/admin/main)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w188" style="width: 136px;">관리자 시스템 로그인</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w217" style="width: 251px;">미분배 보상금 대상 저작물 > 보고 메뉴<br />엑셀파일 양식 다운로드 및 업로드</span>
				</div>
				<h2 class="sub_con_h2 mar_tp30">등록항목</h2>
				<div class="sub01_con_bg4 mar_tp30" style="border-top:1px solid #dddddd;">
					<h3 class="mar_tp20">판매용 음반 방송·공연, 디지털음성송신보상금</h3>
					<p class="mar_tp5">- 작성기관코드, 기관내부관리 ID ,곡명 , 앨범명 , 가수명 , 연주자명 , 음반제작자명 , 분배공고년도 , 분배여부</p>
					<h3 class="mar_tp20">교과용도서보상금</h3>
					<p class="mar_tp5">- 작성기관코드 , 기관내부관리 ID , 적작물명 , 자작자명</p>
					<p class="mar_tp5">- 학교 , 도서구분 , 발행출판사 , 교과목명칭 , 권별구분 , 학기구분  , 출판년도 , 저작물종류 , 이용페이지 , 분배공고년도 , &nbsp;&nbsp;분배여부</p>
					<h3 class="mar_tp20">도서관보상금</h3>
					<p class="mar_tp5">- 작성기관코드 , 기관내부관리ID , 저작물명 , 저작자명 , 출판사 , 발행년도 , 분배공고년도 , 분배여부</p>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER -->
	  	
	  	<!-- FOOTER str-->
	  	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end --> 
	    	
</body>
</html>