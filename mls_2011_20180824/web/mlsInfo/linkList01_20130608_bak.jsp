<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보자료 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
//-->

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<div id="header">
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(4);</script>
		</div>
		<!-- HEADER end -->
		
			<form name="form1" method="post" action="#">
								<input type="hidden" name="filePath"> 
								<input type="hidden" name="fileName"> 
								<input type="hidden" name="realFileName">
								<input type="hidden" name="action_div"> 
								<input type="submit" style="display: none;">
			</form>	
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis4">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_4.gif" alt="저작권 정보자료" title="저작권 정보자료" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	      <!-- 래프 -->
			      <div class="left">
			        <ul id="sub_lnb">
			          <li id="lnb1" class="active"><a href="/mlsInfo/linkList01.jsp">홍보자료</a></li>
			          <li id="lnb2" ><a href="/mlsInfo/linkList02.jsp">저작권정보</a></li>
			        </ul>
			      </div>
			      <!-- //래프 -->
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
				      	<p class="path"><span>Home</span><span>저작권 정보자료</span> <em>홍보자료</em> </p>
				      	<h1><img src="/images/2012/title/content_h1_0401.gif" alt="홍보자료" title="홍보자료" /></h1>
				      					      	
				      	<div class="section pr_area">
				      		<h2 class="mt20">저작권정보 콘텐츠 서비스</h2>
				      		<div class="pr_mv">
				      			<h3><img src="/images/2012/content/pr_tl1.gif" alt="" title="" /></h3>
				      			<p class="black mt15"><strong>디지털저작권거래소</strong> 안내동영상을 다운로드 할 수 있습니다.</p>
				      			<div class="pr_play"><!-- 동영상 플레이 새창 띄우기 --></div>
				      			<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','디지털저작권거래소 안내 동영상_1024(최종본).mp4','디지털저작권거래소 안내 동영상_1024(최종본).mp4')"  class="btn_download">
				      			<img src="/images/2012/button/btn_download.gif" alt="다운로드" title="다운로드" /></a>
				      		</div>
				      		
				      		<div class="floatDiv mt30">
				      			<div class="fl ce">
				      				<h3 class="lft">저작권찾기 안내 리플렛</h3>
				      				<p class="pr_img"><img src="/images/2012/content/link_4.jpg" alt="저작권찾기 안내 리플렛" /></p>
				      			<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','저작권 찾기 서비스 안내 리플렛.pdf','저작권 찾기 서비스 안내 리플렛.pdf')" class="btn_download">
				      			<img src="/images/2012/button/btn_download.gif" alt="다운로드" title="다운로드" />
				      			</a>
				      			</div>
				      			<div class="fr ce">
				      				<h3 class="lft">법정허락제도 안내 리플렛</h3>
				      				<p class="pr_img"><img src="/images/2012/content/link_2.jpg" alt="법정허락제도 안내 리플렛" /></p>
				      			<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','디지털저작권거래소 소개자료.pdf','디지털저작권거래소 소개자료.pdf')" class="btn_download">	
								<img src="/images/2012/button/btn_download.gif" alt="다운로드" title="다운로드" /></a>
				      			</div>
				      			<div class="fl ce mt30">
				      				<h3 class="lft">법정허락제도 간소화 제도 소개 포스터</h3>
				      				<p class="pr_img"><img src="/images/2012/content/link_3.jpg" alt="법정허락제도 간소화 제도 소개 포스터" /></p>
				      				<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','법정허락 간소화 제도 소개 포스터.pdf','법정허락 간소화 제도 소개 포스터.pdf')" class="btn_download">
				      				<img src="/images/2012/button/btn_download.gif" alt="다운로드" title="다운로드" /></a>
				      			</div>
				      			<div class="fr ce mt30">
				      				<h3 class="lft">디지털저작권거래소 소개 포스터</h3>
				      				<p class="pr_img"><img src="/images/2012/content/link_1.jpg" alt="디지털저작권거래소 소개 포스터" /></p>
				      				<a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','디지털저작권거래소 사업 소개 포스터_1030.pdf','디지털저작권거래소 사업 소개 포스터_1030.pdf')" class="btn_download">
				      				<img src="/images/2012/button/btn_download.gif" alt="다운로드" title="다운로드" /></a>
				      			</div>
				      			
				      		
								      		
				      		</div>		
				      		
					</div>
						<!-- //section -->
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>