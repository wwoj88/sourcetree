<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력 신청 소개 및 이용방법 - 이용방법 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis3">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	  <!-- 래프 -->
					<jsp:include page="/include/2012/leftMenu03.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb1","lnb11"); 
	 				</script>
					<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      	<p class="path"><span>Home</span><span>저작권자찾기</span> <span>저작권자 검색 및 상당한 노력 신청</span> <em>소개 및 이용방법</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0301.gif" alt="소개 및 이용방법" title="소개 및 이용방법" /></h1>
			      	
			           <!-- Tab str -->
                              <ul class="tab_menuBg">
	                             <li class="first"><a href="/mlsInfo/liceSrchInfo01.jsp">소개</a></li>
	                             <li class="on"><strong><a href="/mlsInfo/liceSrchInfo02.jsp">이용방법</a></strong></li>
                             </ul>
                             <!-- //Tab -->
			      	
			      		<div class="section">
			      			
			      		<h2 class="mt20">저작권자 검색</h2>
						<p class="TBar mt5 mb5">&nbsp</p>
						<!-- //타이틀 -->
						<!-- box -->
						<div class="white_box">
							<div class="box5">
								<div class="box5_con floatDiv">
									<p class="fl mt10">
										<img src="/images/2012/content/box_img4.gif" alt="">
									</p>
									<div class="fl ml30 ">
										<ul class="list1 mt10">
											<li>위탁관리업자의 관리저작물(600여곳), 저작권 등록부 등의 저작권 정보를 한 곳에 모아
												저작권자를 손쉽게<br/> 찾을 수 있도록 <strong>저작권자 검색 서비스</strong>를 제공합니다. <br />
											<span class="p11 blue2"><strong>* 저작권정보 :</strong> 저작물
													정보(명칭 등) 및 권리관리정보(작사자ㆍ작곡자 등의 저작권자, 가수ㆍ연주자 및</span> <br />
											<span class="p11 blue2" style="margin-left: 83px">음반제작자
													등의 저작인접권자, 신탁관리단체 등)</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- //box -->

						<h2 class="mt20">상당한노력</h2>
						<ul class="tab_style2">
							<li class="bgNone active"><a href="/mlsInfo/liceSrchInfo02.jsp">상당한 노력 직접수행</a>
							</li>
							<li><a href="/mlsInfo/liceSrchInfo05.jsp">저작권자 검색 및 상당한 노력 신청</a>
							</li>
						</ul>
			      			
			      			<ul class="tab_style floatDiv">
								<li><a href="/mlsInfo/liceSrchInfo02.jsp">저작권등록부
										열람안내</a>
								</li>
								<li class="active"><a href="/mlsInfo/liceSrchInfo03.jsp">신탁단체확인요청안내</a>
								</li>
							</ul>
			      			
			      			<div class="white_box">
	                                          <div class="box5">
		                                          <div class="box5_con floatDiv">
			                                          <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
			                                          <div class="fl ml30 ">
				                                          <ul class="list1 mt10">
				                                          <li>찾고자 하는 저작물을 저작권신탁관리업자에게 조회요청을 하고 그 내역을 법정허락 상당한노력 게시판에 본인이 직접 게시를 합니다. ( 저작재산권자나 그의 거소를 조회하는 확정일자 있는 문서를 보냈으나 이를 알 수 없다는 회신을 받거나 문서를 발송한 날부터 1개월이 지났는데도 회신이 없으면 상당한 노력이 충족된것입니다.) </li>
				                                          <li>저작권법 시행령 제18조 제1항 제2호 : 해당 저작물을 취급하는 저작권법 제105조 제1항에 따른 저작권신탁관리업자(해당 저작물이 속하는 분야의 저작물을 취급하는  저작권신탁관리업자가 없는 경우에는 저작권법 제105조 제1항에 따른 저작권대리중개업자 또는 해당 저작물에 대한 이용을 허락받은 사실이 있는 이용자 중 2명 이상)에게 저작재산권자나 그의 거소를 조회하는 확정일자 있는 문서를 보냈으나 이를 알 수 없다는 회신을 받거나 문서를 발송한 날부터 1개월이 지났는데도 회신이 없을것</li>
				                                          </ul>
			                                          </div>
		                                          </div>
	                                          </div>
                                         </div>
					</div>
					<!-- //section -->
					
					<div class="section mt25">	
						
						<!-- 타이틀 -->
						<p class="style2 title">저작권신탁관리업자 연락처 안내</p>
						<p class="TBar mt5 mb5">&nbsp</p>
						<!-- //타이틀 -->	
							      			
	                                    <h2>보상금관리 신탁단체</h2>                                      
	                                    <!-- 한국음악실연자연합회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt10">한국음악실연자연합회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.fkmp.kr/"><img title="한국음악실연자연합회 바로가기" alt="한국음악실연자연합회 바로가기" src="/images/2012/link/btn_logo01.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국음악실연자연헙회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국음악실연자연합회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">보상금/저작권찾기 신청관리</td>
												<td class="ce">권기태 팀장</td>
												<td class="ce">02-2659-7048</td>
												<td class="ce">kjery@fkmp.kr</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->   
								</div>
							</div>
							<!-- //한국음악실연자연합회 -->						
							<!-- 한국음반산업협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국음반산업협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.kapp.or.kr/"><img title="한국음반산업협회 바로가기" alt="한국음반산업협회 바로가기" src="/images/2012/link/btn_logo02.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
			                                    <table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국음악제작협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국음반산업협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">보상금/저작권찾기 신청관리</td>
												<td class="ce">김제경</td>
												<td class="ce">02-3270-5933</td>
												<td class="ce">kjk@riak.or.kr</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국음반산업협회 --> 						
							<!-- 한국복제전송저작권협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국복제전송저작권협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.copycle.or.kr "><img title="한국복제전송저작권협회 바로가기" alt="한국복제전송저작권협회 바로가기" src="/images/2012/link/btn_logo03.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국복제전송저작권협회 담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국복제전송저작권협회</caption>
										<colgroup>
											<col width="15%">
											<col width="15%">
			                                <col width="20%">
											<col width="25">
											<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col" colspan="2">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce" rowspan="2">보상금 신청관리</td>
												<td class="ce">교과용 보상금</td>
												<td class="ce">김나영</td>
												<td class="ce">070-4265-2525</td>
												<td class="ce">nayoung@korra.kr</td>
											</tr>
											<tr>
												<td class="ce">도서관 보상금</td>
												<td class="ce">김광성 과장</td>
												<td class="ce">070-4265-2522</td>
												<td class="ce">kskim@korra.kr</td>
											</tr>
		                                    			<tr>
												<td align="center" class="ce" colspan="2">저작권찾기 신청관리</td>
												<td class="ce">심명관</td>
												<td class="ce">070-4265-2536</td>
												<td class="ce">smk@korra.kr</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국복사전송권협회 -->   
						</div>
						
						<div class="section mt20">						
							<h2>신탁단체</h2>  						
							<!-- 한국음악저작권협회 -->
		                                    <div class="floatDiv">
			                                    <h3 class="mt10">한국음악저작권협회</h3>                                     
		                                    	<a class="linkLogo" href="http://www.komca.or.kr"><img title="한국음악저작권협회 바로가기" alt="한국음악저작권협회 바로가기" src="/images/2012/link/btn_logo04.gif"></a>
			                                    <div class="fl w526">
				                                    <!-- 그리드스타일 -->
										<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국음악저작권협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
											<caption>한국음악저작권협회</caption>
											<colgroup>
											<col width="30%">
											<col width="20%">
											<col width="20%">
											<col width="30%">
											</colgroup>
											<thead>
												<tr>
													<th scope="col">담당업무</th>
													<th scope="col">담당자</th>
													<th scope="col">전화번호</th>
													<th scope="col">E-mail</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="ce">저작권찾기 신청관리</td>
													<td class="ce">전종훈</td>
													<td class="ce">02-2660-0585</td>
													<td class="ce">bigshow0411@komca.or.kr</td>
												</tr>
											</tbody>
										</table>
										<!-- //그리드스타일 -->
									</div>
								</div>
								<!-- //한국음악저작권협회 -->
								<!-- 함께하는음악저작인협회 -->
                                    <div class="floatDiv">
	                                    <h3 class="mt10">함께하는음악저작인협회</h3>                                     
                                    	<a href="http://www.koscap.or.kr" class="linkLogo"><img src="/images/2012/link/btn_logo12.png" alt="함께하는음악저작인협회 바로가기" title="함께하는 음악저작인협회 바로가기"/></a>
	                                    <div class="fl w526">
		                                    <!-- 그리드스타일 -->
								<table cellspacing="0" cellpadding="0" border="1" summary="함께하는음악저작인협회 담당업무, 담당자, 전화번호, E-mail 정보를 나타낸 표입니다." class="grid"><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
									<caption>함께하는음악저작인협회</caption>
									<colgroup>
									<col width="30%">
									<col width="20%">
									<col width="20%">
									<col width="30%">
									</colgroup>
									<thead>
										<tr>
											<th scope="col">담당업무</th>
											<th scope="col">담당자</th>
											<th scope="col">전화번호</th>
											<th scope="col">E-mail</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="ce">저작권찾기 신청관리</td>
											<td class="ce">이용수</td>
											<td class="ce">02-333-8766</td>
											<td class="ce">yongsoo.lee@koscap.or.kr</td>
										</tr>
									</tbody>
								</table>
								<!-- //그리드스타일 -->
							</div>
						</div>
						<!-- //한함께하는 음악저작인협회 -->    						
							<!-- 한국문예학술저작권협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국문예학술저작권협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.copyrightkorea.or.kr"><img title="한국문예학술저작권협회 바로가기" alt="한국문예학술저작권협회 바로가기" src="/images/2012/link/btn_logo05.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국문예학술저작권협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국문예학술저작권협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="20%">
										<col width="30%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">박남규</td>
												<td class="ce">02-508-0440(대표)</td>
												<td class="ce">png@ekosa.org</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국문예학술저작권협회 -->						
							<!-- 한국영화배급협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국영화배급협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.mdak.or.kr/"><img title="한국영화배급협회 바로가기" alt="한국영화배급협회 바로가기" src="/images/2012/link/btn_logo06.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국영화배급협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국영화배급협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">김의수 부장</td>
												<td class="ce">02-3452-1001</td>
												<td class="ce">webmaster@mdak.or.kr</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국영화배급협회 -->						
							<!-- 한국영화제작자협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국영화제작자협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.kfpa.net/"><img title="한국영화제작자협회 바로가기" alt="한국영화제작자협회 바로가기" src="/images/2012/link/btn_logo07.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국영화제작가협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국영화제작자협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">정재훈</td>
												<td class="ce">02-2267-9983(대표)</td>
												<td class="ce">kfpa2@kfpa.net</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국영화제작자협회 -->						
							<!-- 한국방송작가협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국방송작가협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.ktrwa.or.kr/"><img title="한국방송작가협회 바로가기" alt="한국방송작가협회 바로가기" src="/images/2012/link/btn_logo08.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국방송작가협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국방송작가협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">김지숙 부장</td>
												<td class="ce">02-782-1696</td>
												<td class="ce">copyright@ktwa.or.kr</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국방송작가협회 -->						
							<!-- 한국시나리오작가협회 -->
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국시나리오작가협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.scenario.or.kr/"><img title="한국시나리오작가협회 바로가기" alt="한국시나리오작가협회 바로가기" src="/images/2012/link/btn_logo09.gif"></a>
		                                    <div class="fl w526">
			                                    <!-- 그리드스타일 -->
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국시나리오작가협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
										<caption>한국시나리오작가협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="20%">
										<col width="30%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">정지영 부장</td>
												<td class="ce">02-2275-0566(대표)</td>
												<td class="ce">scenario11@hanmail.net</td>
											</tr>
										</tbody>
									</table>
									<!-- //그리드스타일 -->
								</div>
							</div>
							<!-- //한국시나리오작가협회 -->						
							<!-- 한국방송실연자협회 -->
							<!--
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국방송실연자협회</h3>                                     
	                                    	<a class="linkLogo" href="http://www.kbpa.kr"><img title="한국방송실연자협회 바로가기" alt="한국방송실연자협회 바로가기" src="/images/2012/link/btn_logo10.gif"></a>
		                                    <div class="fl w526">
		          -->
			                                    <!-- 그리드스타일 -->
			        <!--                            
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국방송실연자협회  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<!-- 			
										<caption>한국방송실연자협회</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">육세영 팀장</td>
												<td class="ce">02-784-7802(대표)</td>
												<td class="ce">kbpa7802@chol.com</td>
											</tr>
										</tbody>
									</table>
							-->	
									<!-- //그리드스타일 -->
							<!--
								</div>
							</div>
							-->
							<!-- //한국방송실연자협회 -->						
							<!-- 한국언론진흥재단 -->
							<!--
	                                    <div class="floatDiv">
		                                    <h3 class="mt20">한국언론진흥재단</h3>                                     
	                                    	<a class="linkLogo" href="http://www.kpf.or.kr"><img title="한국언론진흥재단 바로가기" alt="한국언론진흥재단 바로가기" src="/images/2012/link/btn_logo11.gif"></a>
		                                    <div class="fl w526">
		          -->                         
			                                    <!-- 그리드스타일 -->
			        <!--                            
									<table cellspacing="0" cellpadding="0" border="1" class="grid" summary="한국언론진흥재단  담당업무, 담당자, 전화번호, E-mail을 나타낸 표입니다."><!-- border=1  스타일 지웠을 경우를 데이터 정렬 -->
							<!--		
										<caption>한국언론진흥재단</caption>
										<colgroup>
										<col width="30%">
										<col width="20%">
										<col width="25%">
										<col width="25%">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">담당업무</th>
												<th scope="col">담당자</th>
												<th scope="col">전화번호</th>
												<th scope="col">E-mail</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="ce">저작권찾기 신청관리</td>
												<td class="ce">미래기술팀</td>
												<td class="ce">02-2001-7782(대표)</td>
												<td class="ce">dnc@kpf.or.kr</td>
											</tr>
										</tbody>
									</table>
							-->
									<!-- //그리드스타일 -->
							<!--
								</div>
							</div>
							-->
							<!-- //한국방송실연자협회 -->
						</div>
					
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>