<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>내권리찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>

</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(2);</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  		<div id="container" class="container_vis2">
		    	<div class="container_vis">
		      <h2><img title="내권리찾기" alt="내권리찾기" src="/images/2012/title/container_vis_h2_2.gif" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    	   			      
			      <!-- 래프 -->
				  <jsp:include page="/include/2012/leftMenu02.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb2");</script>
				  <!-- //래프 -->
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      		<p class="path"><span>Home</span><span>내권리찾기</span> <em>미분배 보상금 대상 저작물 확인</em> </p>
			     	 	<h1><img src="/images/2012/title/content_h1_0202.gif" alt="미분배보상금 신청" title="미분배보상금 신청" /></h1>
			      	
			    	  	<!-- section -->
			      		<div class="section">
      						<!-- Tab str -->
	                          <ul id="tab11" class="tab_menuBg">
	                              <li class="first on"><strong><a href="/mlsInfo/inmtInfo01.jsp">소개</a></strong></li>
	                              <li><a href="/mlsInfo/inmtInfo02.jsp">이용방법</a></li>
	                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">방송음악</a></li>
								  <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=2">교과용</a></li>
	                              <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=3">도서관</a></li>
	                      		</ul>
                   			 <!-- //Tab -->
                                          
                                         <div class="white_box">
	                                          <div class="box5">
		                                          <div class="box5_con floatDiv">
			                                          <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
			                                          <div class="fl ml30 ">
				                                          <ul class="list1 mt10">
				                                          <li>저작권법에서는 저작물의 이용 촉진 활성화를 위하여 일정 사유에 대해 일일이 저작권자의 허락과 협의 절차를 거치지 않고 이용자가 저작물을 이용할 수 있도록 하고 있습니다. </li>
				                                          <li>이러한 저작물 사용에 대한 대가를 보상금이라고 하며 저작권자에게 제공하는 것입니다.</li>
				                                          <li><strong>보상금의 종류는 방송보상금, 교과용보상금, 도서관보상금 3가지 종류이며,</strong> 방송보상금은 방송에 사용된 음악 작품의 실연자, 최초 음반 제작자에게 보상되며 교과용보상금은 교과용 도서에 공표된 저작물의 권리자, 도서관보상금은 도서관 내에서 자료출력, 전송된 저작물의 권리자 등에게 보상됩니다.</li>
				                                          </ul>
			                                          </div>
		                                          </div>
	                                          </div>
                                         </div>
                                        
                                        <h2 class="mt20">미분배 보상금 대상 저작물 신청 절차</h2>
                                      
                                        <div class="mt20">
                                        		<h3>신탁단체 회원 경우</h3>
                                          	<p class="mt20"><img longdesc="#process2" title="신탁단체 회원 경우 미분배보상금 신청 절차" alt="신탁단체 회원 경우 미분배보상금 신청 절차" src="/images/2012/content/process2.gif"></p>
                                          	
                                          	<ul id="process2" class="skip">
                                          	<li>저작권찾기 정보시스템 처리
                                          		<ul>
                                          		<li>저작권자저작권찾기사이트방문</li>
                                          		<li>저작물정보조회</li>
                                          		<li>검색결과물의보상금 정보조회</li>
                                          		<li>저작권자미분배보상금신청</li>
                                          		<li>신청저작물 정보입력과 해당협회에 관련서류 제출</li>
                                          		</ul>
                                          	</li>
                                          	<li>신탁관리 단체관리
                                          		<ul>
                                          		<li>신탁관리단체로 신청 및 접수</li>
                                          		<li>신탁관리단체로 신청 및 접수</li>
                                          		<li>미분배보상금분배내역 확인</li>
                                          		<li>신탁관리단체에서 미분배보상금신청결과처리</li>
                                          		</ul>
                                          	</li>
                                          	</ul>
                                          	<p class="HBar mt25 mb5">&nbsp;</p>
                                         </div>
                                         
                                         <div class="mt20">
                                        		<h3>신탁단체 비회원 경우</h3>
                                          	<p class="mt20"><img longdesc="#process3" title="신탁단체 비회원 경우 미분배보상금 신청 절차" alt="신탁단체 비회원 경우 미분배보상금 신청 절차" src="/images/2012/content/process3.gif"></p>
                                          	
                                          	<ul id="process3" class="skip">
                                          	<li>저작권찾기 정보시스템 처리
                                          		<ul>
                                          		<li>저작권자저작권찾기사이트방문</li>
                                          		<li>저작물정보조회</li>
                                          		<li>검색결과물의보상금 정보조회</li>
                                          		<li>저작권자미분배보상금신청</li>
                                          		<li>신청저작물 정보입력과 해당협회에 관련서류 제출</li>
                                          		</ul>
                                          	</li>
                                          	<li>신탁관리 단체관리
                                          		<ul>
                                          		<li>신탁관리단체로 신청 및 접수</li>
                                          		<li>신탁관리단체로 신청 및 접수</li>
                                          		<li>신탁단체 회원가입 또는 보상금지급회원가입</li>
                                          		<li>신탁관리단체에서 미분배보상금신청결과처리</li>
                                          		</ul>
                                          	</li>
                                          	</ul>
                                          	<p class="HBar mt25 mb5">&nbsp;</p>
                                         </div>
                                         
                                         <h2 class="mt20">현재 미분배 보상금금액</h2>
                                         <div class="mt20">
                                         		<p>현재까지 보상금 관리단체에서 <strong>분배가 안된 <span class="blue2">보상금액의 합산</span></strong>이 아래와 같습니다.</p>
                                         		
                                         		<div class="indemnity">
                                         			<div class="charge"><span>12</span><span>123</span><span>358</span><span>254</span><span class="pr0">254</span></div>
                                         		</div>
                                   		
                                         		<a href=""><img src="/images/2012/button/btn_charge1.gif" alt="방송보상금 분배공고_실연자" title="방송보상금 분배공고_실연자" /></a> <a href=""><img src="/images/2012/button/btn_charge2.gif" alt="방송보상금 분배규정_음반제작자" title="방송보상금 분배규정_음반제작자" /></a> <a href=""><img src="/images/2012/button/btn_charge3.gif" alt="교과용보상금 분배절차" title="교과용보상금 분배절차" /></a> <a href=""><img src="/images/2012/button/btn_charge4.gif" alt="도서관보상금 분배절차" title="도서관보상금 분배절차" /></a>
                                         </div>
                                        
					</div>
					<!-- //section -->
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>