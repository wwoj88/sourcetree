<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 정보 확인 소개 및 이용방법 - 이용방법 | 내권리찾기 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">내 권리 찾기</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/rghtInfo01.jsp" class="on">저작권 정보 확인</a>
					<ul class="sub_lf_menu2">
						<li><a href="/mlsInfo/rghtInfo01.jsp">소개 및 이용방법</a></li>
						<li><a href="/mlsInfo/rghtInfo02.jsp" class="on">서비스 이용</a></li>
					</ul>
				</li>
				<li><a href="/mlsInfo/inmtInfo02.jsp">미분배 보상금 대상 저작물 확인</a>
					<ul class="sub_lf_menu2 disnone">
						<li><a href="/mlsInfo/inmtInfo02.jsp">소개 및 이용방법</a></li>
						<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">서비스 이용</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				내 권리 찾기
				&gt;
				저작권 정보 확인
				&gt;
				<span class="bold">서비스 이용</span>
			</div>
			<div class="con_rt_hd_title">서비스 이용</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<ul class="sub_menu1 w114 mar_tp30">
					<li class="on"><a href="#none" class="on">음악</a></li>
					<li><a href="#none">어문</a></li>
					<li><a href="#none">뉴스</a></li>
					<li><a href="#none">방송대본</a></li>
					<li><a href="#none">이미지</a></li>
					<li><a href="#none">영화</a></li>
					<li><a href="#none" class="last_rt_bor">방송</a></li>
				</ul>
				<p class="clear"></p>
				<div class="sub01_con_bg4_tp mar_tp30"></div>
				<div class="sub01_con_bg4">
					<div class="font15"><span class="color_2c65aa">음악저작물 저작권정보 변경</span>신청에 관한 문의는 신청권리구분에 해당하는 단체로 연락바랍니다.</div>
					<h3 class="mar_tp10">한국음악저작권협회 (작사,작곡,편곡)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">전종훈</span>
						<span class="word_dian_bg">Tel : 02-2660-0585</span>
						<span class="word_dian_bg">E-mail : bigshow0411@komca.or.kr</span>
					</div>
					<h3 class="mar_tp10">함께하는음악저작인협회</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">이용수</span>
						<span class="word_dian_bg">Tel : 02-333-8766</span>
						<span class="word_dian_bg">Tel : 02-333-8766</span>
					</div>
					<h3 class="mar_tp10">한국음악실연자연합회 (가창,연주,지휘)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">권기태 팀장</span>
						<span class="word_dian_bg">Tel : 02-2659-7048</span>
						<span class="word_dian_bg">E-mail : kjery@fkmp.kr</span>
					</div>
					<h3 class="mar_tp10">한국음반산업협회 (음반제작사)</h3>
					<div class="tel_and_email mar_tp10">
						<span class="word_dian_bg">김제경</span>
						<span class="word_dian_bg">Tel : 02-3270-5933</span>
						<span class="word_dian_bg">E-mail : kjk@riak.or.kr</span>
					</div>
				</div>
				<table class="sub_tab2 mar_tp20" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="17%"/>
						<col width="18%"/>
						<col width="15%"/>
						<col width="20%"/>
						<col width="*"/>
					</colgroup>
					<tbody>
						<tr>
							<th scope="col"><label for="inp1">작사/작곡/편곡</label></th>
							<td><input type="text" id="inp1" style="width:95%;padding:3px;" /></td>
							<th scope="col"><label for="inp2">곡명</label></th>
							<td><input type="text" id="inp2" style="width:95%;padding:3px;" /></td>
							<td rowspan="3" class="bro175_posi">
								<a href="#none" class="music_search"><img src="/images/sub_img/sub_25.gif" alt="조회" /></a>
								<div class="bro175"></div>
							</td>
						</tr>
						<tr>
							<th scope="col"><label for="inp3">가창/연주/지휘</label></th>
							<td><input type="text" id="inp3" style="width:95%;padding:3px;" /></td>
							<th scope="col"><label for="inp4">앨범명</label></th>
							<td><input type="text" id="inp4" style="width:95%;padding:3px;" /></td>
						</tr>
						<tr>
							<th scope="col"><label for="inp5">음반제작사</label></th>
							<td><input type="text" id="inp5" style="width:95%;padding:3px;" /></td>
							<th scope="col"><label for="inp6-01">발매일자</label></th>
							<td>
								<input type="text" id="inp6-01" style="width:29%;padding:3px 0;" />
								<input type="image" src="/images/sub_img/sub_26.gif" alt="날짜" />
								<input type="text" id="inp6-02" style="width:29%;padding:3px 0;" />
								<input type="image" src="/images/sub_img/sub_26.gif" alt="날짜" />
							</td>
						</tr>
					</tbody>
				</table>
				<div class="bg_fafafa">* 검색은 원하는 항목에 찾고자 하는 검색어를 기입한 후 오른쪽 ‘조회’ 버튼을 클릭하세요.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;도움말 <img src="/images/sub_img/sub_27.gif" alt="그림" /></div>
				<div class="sub02_music disnone">
					<table class="sub_tab3 mar_tp40" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
						<caption>한국저작권위원회</caption>
						<colgroup>
							<col width="10%"/>
							<col width="22%"/>
							<col width="15%"/>
							<col width="12%"/>
							<col width="12%"/>
							<col width="12%"/>
							<col width="*"/>
						</colgroup>
						<thead>
							<tr>
								<th scope="col" rowspan="2">순번</th>
								<th scope="col" colspan="2">곡명</th>
								<th scope="col">작사</th>
								<th scope="col">작곡</th>
								<th scope="col">편곡</th>
								<th scope="col" rowspan="2">음반제작사</th>
							</tr>
							<tr>
								<th scope="col">앨범명</th>
								<th scope="col">발매일자</th>
								<th scope="col">가창</th>
								<th scope="col">연주</th>
								<th scope="col">지휘</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td rowspan="2">1</td>
								<td colspan="2" class="align_lf10">네가 불던 날</td>
								<td>이미나</td>
								<td>성시경</td>
								<td>김두현</td>
								<td rowspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td class="align_lf10">처음</td>
								<td>20110907</td>
								<td>성시경</td>
								<td>강성호,정재필,홍준...</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td rowspan="2">2</td>
								<td colspan="2" class="align_lf10">네가 불던 날</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td rowspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td class="align_lf10">Sung Si Kyun...</td>
								<td>20110915</td>
								<td>성시경</td>
								<td>강성호,홍준호,신석...</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td rowspan="2">3</td>
								<td colspan="2" class="align_lf10">네가 불던 날 (MR)</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td rowspan="2">블루아이씨티</td>
							</tr>
							<tr>
								<td class="align_lf10">MR반주 뮤직섬 201...</td>
								<td>2011</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td rowspan="2">4</td>
								<td colspan="2" class="align_lf10">네가 불던 날 (MR)</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td rowspan="2">디지탈레코드</td>
							</tr>
							<tr>
								<td class="align_lf10">가요 MR 반주 최신곡...</td>
								<td>2011</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td rowspan="2">5</td>
								<td colspan="2" class="align_lf10">네가 불던 날</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td rowspan="2">젤리피쉬엔터테인먼트...</td>
							</tr>
							<tr>
								<td class="align_lf10">처음</td>
								<td>2011</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td rowspan="2">6</td>
								<td colspan="2" class="align_lf10">네가 불던 날</td>
								<td>이미나</td>
								<td>성시경</td>
								<td>김두현</td>
								<td rowspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td class="align_lf10">처음</td>
								<td>20110915</td>
								<td>성시경</td>
								<td>홍준호,정재필,신현...</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					<div class="page mar_tp30">
						<a href="#n"><img src="/images/sub_img/page_up01.gif" alt="그림" /></a>
						<a href="#n"><img src="/images/sub_img/page_up02.gif" alt="그림" /></a>
						<a href="#n" class="page_num on">1</a>
						<a href="#n" class="page_num">2</a>
						<a href="#n" class="page_num">3</a>
						<a href="#n" class="page_num">4</a>
						<a href="#n" class="page_num">5</a>
						<a href="#n" class="page_num">6</a>
						<a href="#n" class="page_num">7</a>
						<a href="#n" class="page_num">8</a>
						<a href="#n" class="page_num">9</a>
						<a href="#n" class="page_num">10</a>
						<a href="#n"><img src="/images/sub_img/page_down01.gif" alt="그림" /></a>
						<a href="#n"><img src="/images/sub_img/page_down02.gif" alt="그림" /></a>
					</div>
				</div>
				<table class="sub_tab3 mar_tp40 music_prompt" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="10%"/>
						<col width="22%"/>
						<col width="15%"/>
						<col width="12%"/>
						<col width="12%"/>
						<col width="12%"/>
						<col width="*"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col" rowspan="2">순번</th>
							<th scope="col" colspan="2">곡명</th>
							<th scope="col">작사</th>
							<th scope="col">작곡</th>
							<th scope="col">편곡</th>
							<th scope="col" rowspan="2">음반제작사</th>
						</tr>
						<tr>
							<th scope="col">앨범명</th>
							<th scope="col">발매일자</th>
							<th scope="col">가창</th>
							<th scope="col">연주</th>
							<th scope="col">지휘</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="7">검색조건을 입력해 주세요.</td>
						</tr>
					</tbody>
				</table>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	    
	    <!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->	
	    		    	
	
</body>
</html>