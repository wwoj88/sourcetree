<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>법정허락 승인 신청 소개 및 이용방법 - 이용방법 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />  
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--

function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 

//-->
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
<STYLE type="text/css">
.pr_area .pr_mv{ background: url(/images/2012/content/pr_box1.gif) no-repeat 0 0; padding: 22px 0 0 26px; position: relative; height: 162px;}
.pr_area .pr_mv h3{ background: none; padding: 0;}
.pr_area .pr_mv .pr_down{ position: absolute; top: 24px; right: 0; width: 317px; height: 165px; background: url(/images/2012/content/pr_down.png) no-repeat 0 0;}
.pr_area .pr_mv .pr_down a{ display: block; margin: 49px 0 0 133px; width: 58px; height: 55px; text-indent: -10000000px;}
.pr_area .pr_mv .btn_download{ display: block; margin: 10px 345px 0 0; text-align: right}
</STYLE>
</head>

<body>
		
		<!-- HEADER str-->
		<jsp:include page="/include/2012/subHeader3.jsp" />
		<script type="text/javascript">initNavigation(3);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
		<form name="form1" method="post" action="#">
			<input type="hidden" name="filePath"> <input type="hidden"
				name="fileName"> <input type="hidden" name="realFileName">
			<input type="hidden" name="action_div"> <input type="submit"
				style="display: none;">
		</form>
	  	<!-- CONTAINER str-->    
		<div id="contents">   
		    		
		     <!-- 래프 -->
				<div class="con_lf">
					<div class="con_lf_big_title">저작권자 찾기</div>
					<ul class="sub_lf_menu">
						<li><a href="/mlsInfo/liceSrchInfo01.jsp">저작권자 검색 및 상당한 노력 신청</a>
							<ul class="sub_lf_menu2 disnone">
								<li><a href="/mlsInfo/liceSrchInfo01.jsp">소개 및 이용방법</a></li>
								<li><a href="/srchList.do">서비스 이용</a></li>
								<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
							</ul>
						</li>
						<li><a href="/mlsInfo/statInfo01.jsp" class="on">법정허락 승인 신청</a>
							<ul class="sub_lf_menu2">
								<li><a href="/mlsInfo/statInfo01.jsp" class="on">소개 및 이용방법</a></li>
								<li><a href="/stat/statSrch.do">서비스 이용</a></li>
								<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
							</ul>
						</li>
					</ul>
				</div>
			<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      <div class="con_rt" id="contentBody">
			      	<div class="con_rt_head">
						<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
						&gt;
						저작권자 찾기
						&gt;
						법정허락 승인 신청
						&gt;
						<span class="bold">소개 및 이용방법</span>
					</div>
			      		<div class="con_rt_hd_title">소개 및 이용방법</div>
			      	
			      	<div id="sub_contents_con">
                                          
                            <h2 class="mt20">법정허락 승인 신청 대상물 조회 방법</h2>
                              
                            <p class="p11 mt20"> <strong>1. 조회하려고 하는 저작물의 검색 키워드를 넣고 조회 버튼을 클릭합니다.</strong></p>
                          	
                          	<p class="p11 mt10">&lt;그림 1. 법정허락 승인 신청 대상물 정보 검색 화면&gt;</p>
                              	<p class="capture"><img src="/images/2012/sample/statInfo02-01.png" width="650" alt="법정허락 승인 신청대상물 검색화면 이미지 캡쳐본" /></p>
                             
                             
                            <!-- 2 --> 
                            <h2 class="mt25">선택저작물  승인 신청 방법</h2>
                              
                            <p class="p11 mt20"> <strong>1. 검색 내용을 확인하고 이용허락신청을 원하는 저작물의 체크박스를 선택하여 [선택저작물 승인 신청]버튼을 클릭합니다.</strong></p>
                          	
                          	<p class="p11 mt10">&lt;그림 1. 법정허락 승인 신청 대상물 정보 검색결과 화면&gt;</p>
                              	<p class="capture"><img src="/images/2012/sample/statInfo02-0201.png" width="650" alt="법정허락 승인 신청대상물 검색결과화면 이미지 캡쳐본" /></p>
                   
                   
                             <p class="p11 mt20"> <strong>2. 선택저작물을 확인하고 승인 신청서 작성 및 승인 신청 명세서 정보를 확인합니다.</strong></p>
                              	
                              	<p class="p11 mt10">&lt;그림 2-1. 승인 신청서 화면&gt;</p>
                              	<p class="capture"><img src="/images/2012/sample/statInfo02-020201.png" width="500" alt="승인 신청서 화면 이미지 캡쳐본" /></p>
                              	<div class="floatDiv">
                              		<div class="fl w48">
                              			<p class="p11 mt10">&lt;그림 2-2. 승인 신청 명세서 화면&gt;</p>
                              			<p class="capture"><img src="/images/2012/sample/statInfo02-020202.png" width="500" alt="승인 신청 명세서 화면 이미지 캡쳐본" /></p>
                              		</div>
                              		<!-- 
                              		<div class="fr w48">
                              			<span class="p11 blue">저작권찾기 아이콘을 클릭하여  세부정보 팝업 내용 확인 </span>
                              			<p class="capture"><img src="" alt="세부정보 팝업 이미지 캡쳐본" /></p>
                              			<p>본인 또는 타인이  저작권찾기 신청한 내역이 있을 경우만 아이콘 노출이 되며, 클릭시,  신청된 세부 정보를 확인할 수 있다.<br />좌측에서는 가창자 정보가 공란이나, 보기에서는 가창자로 홍길동이 추가가 되었다.</p>
                              		</div>
                              		 -->
                              	</div>
                              	<p class="p11 mt10"> <strong>3. 신청정보를 확인하고 공인인증서를 첨부하여 제출합니다.</strong></p>		
                           	
                           	<!-- 3 --> 
                            <h2 class="mt25">개인 승인 신청 방법</h2>
                              
                            <p class="p11 mt20"> <strong>1. [개인 승인 신청]버튼을 클릭하여 법정허락 승인 신청서 및 승인 신청 명세서를 작성합니다.</strong></p>
                              	<p class="p11 mt10">&lt;그림 1-1. 승인 신청서 화면&gt;</p>
                              	<p class="capture"><img src="/images/2012/sample/statInfo02-0301.png" width="500" alt="승인 신청서 화면 이미지 캡쳐본" /></p>
                              	<div class="floatDiv">
                              		<div class="fl w48">
                              			<p class="p11 mt10">&lt;그림 1-2. 승인 신청 명세서 화면&gt;</p>
                              			<p class="capture"><img src="/images/2012/sample/statInfo02-0302.png" width="500" alt="승인 신청 명세서 화면 이미지 캡쳐본" /></p>
                              		</div>
                              		<!-- 
                              		<div class="fr w48">
                              			<span class="p11 blue">저작권찾기 아이콘을 클릭하여  세부정보 팝업 내용 확인 </span>
                              			<p class="capture"><img src="" alt="세부정보 팝업 이미지 캡쳐본" /></p>
                              			<p>본인 또는 타인이  저작권찾기 신청한 내역이 있을 경우만 아이콘 노출이 되며, 클릭시,  신청된 세부 정보를 확인할 수 있다.<br />좌측에서는 가창자 정보가 공란이나, 보기에서는 가창자로 홍길동이 추가가 되었다.</p>
                              		</div>
                              		 -->
                              	</div>
                              	<p class="p11 mt10"> <strong>2. 신청정보를 확인하고 공인인증서를 첨부하여 제출합니다.</strong></p>
                            <!-- 
                            <h2 class="mt20">저작권자  저작권찾기 신청 방법</h2>
                              <ul class="list1 ml5 mt10">
                               <li>저작권자가 확인하려 하는 저작물을 검색하고, 상세 조회 결과까지 확인을 하였으나,  수정 또는 추가하고자 하는 내용이 있을 경우 아래와 같이 신청을 합니다.
                               	<p class="capture"><img src="" alt="음악신청화면 이미지 캡쳐본" /></p>
                               </li>
                               <li>. 저작권자 저작권찾기 신청은 저작물 검색을 하고, 체크박스 선택하여 신청 페이지로 넘어가거나, 저작권자 저작권찾기 신청을 먼저 하고 신청페이지에서 검색을 하여 수정할 저작물을 선택하는 두가지 방법으로 진행이 가능합니다.<br /></br />신청저작물 정보에서 수정 또는 추가하고자 하는 사항을 입력하고, 필요에 따라서 문서를 첨부하여 신청을 합니다.
                               	<p class="capture"><img src="" alt="저작권자 저작권찾기 신청 화면 이미지 캡쳐본" /></p>
                               </li>
                             </ul>
                             -->
                                        
					</div>
					<!-- //section -->
				  </div>
				<!-- //주요컨텐츠 end -->
					<p class="clear"></p>
		    	</div>
	    		<!-- //content -->
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->

</body>
</html>