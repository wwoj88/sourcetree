<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력 신청 소개 및 이용방법 - 이용방법 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis3">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	      <!-- 래프 -->
					<jsp:include page="/include/2012/leftMenu03.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb1","lnb11"); 
	 				</script>
					<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      	<p class="path"><span>Home</span><span>저작권자찾기</span> <span>저작권자 검색 및 상당한 노력 신청</span> <em>소개 및 이용방법</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0301.gif" alt="소개 및 이용방법" title="소개 및 이용방법" /></h1>
			      	
			           <!-- Tab str -->
                             <ul class="tab_menuBg">
	                             <li class="first"><a href="/mlsInfo/liceSrchInfo01.jsp">소개</a></li>
	                             <li class="on"><strong><a href="/mlsInfo/liceSrchInfo02.jsp">이용방법</a></strong></li>
                             </ul>
                             <!-- //Tab -->
			      	
			      		<div class="section">
			      			
			      			<h2 class="mt20">저작권자 검색</h2>
							<p class="TBar mt5 mb5">&nbsp</p>
							<!-- //타이틀 -->
							<!-- box -->
							<div class="white_box">
								<div class="box5">
									<div class="box5_con floatDiv">
										<p class="fl mt10">
											<img src="/images/2012/content/box_img4.gif" alt="">
										</p>
										<div class="fl ml30 ">
											<ul class="list1 mt10">
												<li>위탁관리업자의 관리저작물(600여곳), 저작권 등록부 등의 저작권 정보를 한 곳에 모아
													저작권자를 손쉽게 <br/>찾을 수 있도록 <strong>저작권자 검색 서비스</strong>를 제공합니다. <br />
												<span class="p11 blue2"><strong>* 저작권정보 :</strong> 저작물
														정보(명칭 등) 및 권리관리정보(작사자ㆍ작곡자 등의 저작권자, 가수ㆍ연주자 및</span> <br />
												<span class="p11 blue2" style="margin-left: 83px">음반제작자
														등의 저작인접권자, 신탁관리단체 등)</span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- //box -->
	
							<h2 class="mt20">상당한노력</h2>
			      			
			      			<ul class="tab_style2">
								<li class="bgNone"><a href="/mlsInfo/liceSrchInfo02.jsp">상당한 노력 직접수행</a>
								</li>
								<li class="active"><a href="/mlsInfo/liceSrchInfo05.jsp">저작권자 검색 및 상당한 노력 신청</a>
								</li>
							</ul>
			      			
			      		<!-- 타이틀 -->
						<p class="style2 title mt30">저작권자 검색 및 상당한 노력 신청서비스</p>
						<p class="TBar mt5 mb5">&nbsp;</p>
						<!-- //타이틀 -->	
			      			<div class="white_box mt5">
	                                          <div class="box5">
		                                          <div class="box5_con floatDiv">
			                                          <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
			                                          <div class="fl ml30 ">
				                                          <ul class="list1 mt10">
				                                          <li>위탁관리(신탁 및 대리중개업자, 600여 곳) 기관 및 등록부에서 관리하는 저작권 정보를 한 곳에 모아서 손쉽게 저작권자를 확인할 수 있는 저작권자 검색 서비스 제공  </li>
				                                          <li>저작권자 미 검색 시 법정허락의 사전절차인 저작권자를 찾기 위한 상당한 노력을 정부가 대신 해주는 서비스</li>
				                                          </ul>
			                                          </div>
		                                          </div>
	                                          </div>
                             </div>
                             
                             <h2 class="mt30">절차</h2>
                             <div class="floatDiv mt10">
                             	<img alt="통합검색>저작권자정보제공>상당한노력신청(미검색)>상당한노력공고>이의신청및심사>법정허락대상저작물게시" title="통합검색>저작권자정보제공>상당한노력신청(미검색)>상당한노력공고>이의신청및심사>법정허락대상저작물게시" src="/images/2012/content/process11.gif" />
                             </div>
                             <h2 class="mt40">통합검색 순서도</h2>
                             <div class="floatDiv mt10">
                             	<img alt="상당한노력 절차2" src="/images/2012/content/process8.gif" />
                             </div>
                             <h2 class="mt40">상당한 노력 신청 절차</h2>
                             <div class="floatDiv mt10">
                             	<img alt="상당한노력 절차3" src="/images/2012/content/process13.gif" />
                             </div>
                                   
					</div>
					<!-- //section -->
					
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>