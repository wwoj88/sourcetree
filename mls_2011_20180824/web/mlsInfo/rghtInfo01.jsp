<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=euc-kr" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 정보 확인 소개 및 이용방법 - 소개 | 내권리찾기 | 저작권찾기 </title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2010/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--

//-->
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">내 권리 찾기</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/rghtInfo01.jsp" class="on">내 권리 찾기 서비스 이용</a>
					<ul class="sub_lf_menu2">
						<li><a href="/mlsInfo/rghtInfo01.jsp" class="on">소개 및 이용방법</a></li>
						<li><a href="/rghtPrps/rghtSrch.do?DIVS=M">서비스 이용</a></li>
					</ul>
				</li>
				<li><a href="/mlsInfo/inmtInfo02.jsp">미분배 보상금 대상 저작물 확인</a>
					<ul class="sub_lf_menu2 disnone">
						<li><a href="/mlsInfo/inmtInfo02.jsp">소개 및 이용방법</a></li>
						<li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">서비스 이용</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				내 권리 찾기
				&gt;
				<span class="bold">소개 및 이용방법</span>
			</div>
			<div class="con_rt_hd_title">내 권리 찾기</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<h1 class="sub_con_h1 float_lf">저작권 정보 확인 서비스란?</h1>
				<div class="float_rt"><a href="/mlsInfo/rghtInfo03.jsp" class="method_bg_25869b">자세한 이용방법 바로가기</a></div>
				<p class="clear"></p>
				<p class="mar_tp5">권리자가 정당한 보상을 받을 수 있도록 저작권 정보를 확인할 수 있게 해주고,</p>
				<p class="mar_tp5">수정이 필요한 경우 변경 방법을 알려드립니다.</p>
				<h2 class="sub_con_h2 mar_tp30">이용순서</h2>
				<img src="/images/sub_img/sub_24.gif" alt="이용순서" class="mar_tp30" />
				<h2 class="sub_con_h2 mar_tp30">저작권 정보 제공단체</h2>
				<table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="15%"/>
						<col width="*"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">구분</th>
							<th scope="col" class="last_bor0">제공단체</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>음악</td>
							<td class="last_bor0 align_lf pad_lf20">한국음악저작권협회, 함께하는음악저작인협회, 한국음악실연자연합회, 한국음반산업협회 등</td>
						</tr>
						<tr>
							<td>어문</td>
							<td class="last_bor0 align_lf pad_lf20">한국복제전송저작권협회, 한국문예학술저작권협회 등</td>
						</tr>
						<tr>
							<td>뉴스</td>
							<td class="last_bor0 align_lf pad_lf20">한국언론진흥재단 등</td>
						</tr>
						<tr>
							<td>영화</td>
							<td class="last_bor0 align_lf pad_lf20">한국영화배급협회, 한국영상자료원, 한국영화제작가협회 등</td>
						</tr>
						<tr>
							<td>방송대본</td>
							<td class="last_bor0 align_lf pad_lf20">한국방송작가협회</td>
						</tr>
						<tr>
							<td>미술</td>
							<td class="last_bor0 align_lf pad_lf20">(주)대중문화저작권관리 등</td>
						</tr>
						<tr>
							<td>기타</td>
							<td class="last_bor0 align_lf pad_lf20">방송, 이미지 등 관련 저작권위탁관리업자</td>
						</tr>
					</tbody>
				</table>
				<p class="mar_tp20">※ 확인할 수 있는 저작권 정보는 위의 저작권 정보 제공단체로부터 수집된 정보입니다.<br/></p>
				<p class="color_ff6000 mar_tp5">※ 저작권 정보란? </p>
				<p class="color_ff6000 pad_lf20 mar_tp5">저작물 정보(명칭 등) 및 권리관리정보(작사자·작곡가 등의 저작권자, 가수·연주자 및 음반제작자 등의 저작인접권자, 신탁관리단체 등)</p>
				<!-- 
				<div class="mar_tp30">
					<div class="sub_con_h2 float_lf">저작물 조회 방법
					</div>
					<div class="float_rt"><a href="/mlsInfo/rghtInfo01.jsp" class="method_bg_25869b">서비스 소개 바로가기</a></div>
					<p class="clear"></p>
				</div>
				<p class="mar_tp5">1. 조회하려는 저작물의 분야를 선택하고 검색 키워드를 입력하신 후 조회 버튼을 클릭합니다.</p>
				<p class="mar_tp5">(선택하신 분야에 따라 검색 항목은 다를 수 있습니다.)</p>
				<ul class="sub_menu1 w114 mar_tp40">
					<li class="on"><a href="#none" class="on">음악</a></li>
					<li><a href="#none">어문</a></li>
					<li><a href="#none">뉴스</a></li>
					<li><a href="#none">방송대본</a></li>
					<li><a href="#none">이미지</a></li>
					<li><a href="#none">영화</a></li>
					<li><a href="#none" class="last_rt_bor">방송</a></li>
				</ul>
				<p class="clear"></p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_30.jpg" alt="그림" /></div>
				<p class="mar_tp20">2. 검색 결과를 확인하고 저작물의 제목(제호)을 클릭하여 상세정보 팝업에서 저작물 및 저작권 정보를 확인합니다.</p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_31.jpg" alt="그림" /></div>
				<div class="mar_tp20"><img src="/images/sub_img/sub_32.jpg" alt="그림" /></div>
				<div class="mar_tp20"><img src="/images/sub_img/sub_33.jpg" alt="그림" /></div>
				<p class="mar_tp20">3. 저작권 정보의 변경이 필요할 경우 검색화면 상단에 안내된 단체로 관련사항을 문의합니다.</p>
				<div class="mar_tp20"><img src="/images/sub_img/sub_34.jpg" alt="그림" /></div>
				 --><!--  --><!-- 
				 -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	    	
	    		    	
	</body>
</html>