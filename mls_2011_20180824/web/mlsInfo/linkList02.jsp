<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권정보자료 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
function popLink(code){
	var URL = '';
	if(code == 'A'){	   //네티즌이 알아야할 저작권
	    URL = 'http://www.copyright.or.kr/info/knowledge/netizen/netizen01.do?hm_seq=101';
	}else if(code == 'B'){ //저작권 상담 사례100
		URL = 'http://www.copyright.or.kr/info/knowledge/counsel.do?hm_seq=100';
	}else if(code == 'C'){ //저작권동향
		URL = 'http://www.copyright.or.kr/info/ground_list.do?hm_seq=89';
	}else if(code == 'D'){ //저작권 법령정보
		URL = 'http://www.copyright.or.kr/info/law/precedent_list.do?hm_seq=230';
	}else if(code == 'E'){ //저작권 도서관
		URL = 'http://www.copyright.or.kr/info/data/library.do?hm_seq=105';
	}
	
	var newWin = window.open('about:blank');
	
	newWin.location.href = URL;
	//window.open(URL, "small", "width=750, height=650, scrollbars=yes, menubar=no, location=yes");
}
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<div id="header">
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(4);</script>
		</div>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis4">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_4.gif" alt="저작권 정보자료" title="저작권 정보자료" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	      <!-- 래프 -->
			      <div class="left">
			        <ul id="sub_lnb">
			          <li id="lnb1"><a href="/mlsInfo/linkList01.jsp">홍보자료</a></li>
			          <li id="lnb2" class="active"><a href="/mlsInfo/linkList02.jsp">저작권정보</a></li>
			        </ul>
			      </div>
			      <!-- //래프 -->
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
				      	<p class="path"><span>Home</span><span>저작권 정보자료</span> <em>저작권정보</em> </p>
				      	<h1><img src="/images/2012/title/content_h1_0402.gif" alt="저작권정보" title="저작권정보" /></h1>
				      					      	
				      	<div class="section pr_area">
				      		
				      		<div class="floatDiv mt15">
				      			<div class="fl ce">
				      				<h3 class="lft">네티즌이 알아야할 저작권</h3>
				      				<div class="pr_site"><p class="site_img"><img src="/images/2012/content/site1.gif" alt="" class="site" /><a href="javascript:popLink('A')" class="inBlock mt10"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a></p></div>
				      			</div>
				      			<div class="fr ce">
				      				<h3 class="lft">저작권상담사례100</h3>
				      				<div class="pr_site"><p class="site_img"><img src="/images/2012/content/site2.gif" alt="" class="site" /><a href="javascript:popLink('B')" class="inBlock mt10"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a></p></div>
				      			</div>
				      			
				      			<div class="clear fl ce mt20">
				      				<h3 class="lft">저작권동향</h3>
				      				<div class="pr_site"><p class="site_img"><img src="/images/2012/content/site3.gif" alt="" class="site" /><a href="javascript:popLink('C')" class="inBlock mt10"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a></p></div>
				      			</div>
				      			<div class="fr ce mt20">
				      				<h3 class="lft">저작권 법령정보</h3>
				      				<div class="pr_site"><p class="site_img"><img src="/images/2012/content/site4.gif" alt="" class="site" /><a href="javascript:popLink('D')" class="inBlock mt10"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a></p></div>
				      			</div>
				      			
				      			<div class="clear fl ce mt20">
				      				<h3 class="lft">저작권 도서관</h3>
				      				<div class="pr_site"><p class="site_img"><img src="/images/2012/content/site5.gif" alt="" class="site" /><a href="javascript:popLink('E')" class="inBlock mt10"><img src="/images/2012/button/btn_go_site.gif" alt="바로가기" title="바로가기" /></a></p></div>
				      			</div>
				      		</div>
				      		
					</div>
						<!-- //section -->
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>