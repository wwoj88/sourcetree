<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권자 검색 및 상당한 노력신청 소개 및 이용방법 - 이용방법 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--

//-->
function detail(id)
{
 	if(sub03_01_see_popup.style.display == 'none')
 	{
 		sub03_01_see_popup.style.display='block';
	}
 	else
 	{
 		sub03_01_see_popup.style.display = 'none';
 	}
}

function closePop(){
	if(sub03_01_see_popup.style.display == 'block') {
		sub03_01_see_popup.style.display = 'none';
	}
}
function detail_see(id) {
 	if(sub03_01_see_popup.style.display == 'none') {
 		sub03_01_see_popup.style.display='block';
	}else {
 		sub03_01_see_popup.style.display = 'none';
 	}
}
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');

</script>
</head>

<body>
		
		<!-- HEADER str-->
		<jsp:include page="/include/2012/subHeader3.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
	  		<div class="con_lf">
			<div class="con_lf_big_title">저작권자 찾기</div>
				<ul class="sub_lf_menu">
					<li><a href="/mlsInfo/liceSrchInfo01.jsp" class="on">저작권자 검색 및 상당한 노력 신청</a>
						<ul class="sub_lf_menu2">
							<li><a href="/mlsInfo/liceSrchInfo01.jsp" class="on">소개 및 이용방법</a></li>
							<li><a href="/srchList.do">서비스 이용</a></li>
							<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
						</ul>
					</li>
					<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청</a>
						<ul class="sub_lf_menu2 disnone">
							<li><a href="/mlsInfo/statInfo01.jsp">소개 및 이용방법</a></li>
							<li><a href="/stat/statSrch.do">서비스 이용</a></li>
							<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 공고</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				저작권자 찾기
				&gt;
				저작권자 검색 및 상당한 노력 신청
				&gt;
				<span class="bold">소개 및 이용방법</span>
			</div>
			<div class="con_rt_hd_title">소개 및 이용방법</div>
			
			<div id="sub_contents_con">
				<h1 class="sub_con_h1 float_lf">상당한 노력 직접 수행 방법</h1>
				<!-- <div class="float_rt"><a href="#1" onclick="javascript:detail('sub03_01_see_popup');" class="method_bg_25869b sub03_01_see_popup_open" style="padding-right: 20px;">서비스 소개 바로가기</a></div>
				<p class="clear"></p> -->
				<div class="float_rt"><a href="/mlsInfo/liceSrchInfo01.jsp" class="method_bg_25869b">서비스 소개 바로가기</a></div>
				<p class="clear"></p>
				<div class="w685">
					<div class="sub_con_h2 mar_tp20">저작권등록부 열람안내</div>
						<div class="word_dian_bg mar_tp15 pad_lf20">찾고자 하는 저작물을 본 사이트인 저작권 찾기 정보시스템이 아닌, 저작권등록사이트<span class="color_2c65aa">(www.cros.or.kr)</span>에서 열람하고 열람한 내역을 법정허락 상당한노력 게시판에 본인이 직접 게시합니다.</div>
						<div class="word_dian_bg mar_tp10 pad_lf20">저작권법 시행령 제18조 제1항 제1호 : 저작권법 제55조 제3항에 따른 저작권등록부의 열람 또는 그 사본의 교부신청을 통하여 해당 저작물의 저작재산권자나 그의 거소를 조회할 것</div>
					<div class="mar_tp20 pad_lf20">
						<div class="float_lf">
							<div class="float_rt"><a href="http://www.cros.or.kr" class="committee w238_pad_lf40" target="_blank">저작권등록사이트 바로가기</a></div>
						</div>
						<div class="float_rt"><a href="https://www.cros.or.kr/reg/main.cc" class="committee w238_pad_lf40" target="_blank">등록부 열람/사본 페이지 바로가기</a></div>
						<p class="clear"></p>
					</div>
					<div class="sub_con_h2 mar_tp30">저작권자 조회 공고</div>
						<div class="word_dian_bg mar_tp15 pad_lf20">찾고자 하는 저작물에 대해 저작권등록부 열람 및 신탁단체에 조회 요청하였으나 저작물의 저작재산권자나 그의 거소의 조회가 불가능한 경우, 열람 및 조회 내역을 저작권자 조회공고 게시판에 본인이 직접 게시합니다.</div>
						<div class="word_dian_bg mar_tp10 pad_lf20">저작권법 시행령 제18조 제1항 제3호 : 저작재산권자나 그의 거소 등 문화체육관광부령으로 정하는 사항을 다음 각 목의 어느 하나에 공고한 날부터 10일이 지났을 것</div>
					<div class="mar_tp20 pad_lf20">
						<div class="float_lf">
							<div class="float_rt"><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5" target="_blank" class="committee w238_pad_lf40">저작권자 조회공고 바로가기</a></div>
						</div>
						<div class="float_rt"><a href="http://www.mcst.go.kr" class="committee w238_pad_lf40" target="_blank">문화체육관광부 홈페이지 바로가기</a></div>
						<p class="clear"></p>
					</div>
				</div>
					<h1 class="sub_con_h1 mar_tp30">신탁단체 확인요청 안내</h1>
					<p class="word_dian_bg mar_tp20">찾고자 하는 저작물을 저작권신탁업자에게 조회요청을 하고 그 내역을 법정허락 상당한노력 게시판에 본인이 직접 게시를 합니다. ( 저작재산권자나 그의 거소를 조회하는 확정일자 있는 문서를 보냈으나 이를 알 수 없다는 회신을 받거나 문서를 발송한 날부터 1개월이 지났는데도 회신이 없으면 상당한 노력이 충족된 것입니다.)</p>
					<p class="word_dian_bg mar_tp20">저작권법 시행령 제18조 제1항 제2호 : 해당 저작물을 취급하는 저작권법 제105조 제1항에 따른 저작권신탁업자(해당 저작물이 속하는 분야의 저작물을 취급하는 저작권신탁관리업자가 없는 경우에는 저작권법 제105조 제1항에 따른 저작권대리중개업자 또는 해당 저작물에 대한 이용을 허락 받은 사실이 있는 이용자 중 2명 이상)에게 저작재산권자나 그의 거소를 조회하는 확정일자 있는 문서를 보냈으나 이를 알 수 없다는 회신을 받거나 문서를 발송한 날부터 1개월이 지났는데도 회신이 없을 것</p>
					<!-- <div class="mar_tp20 committee w238_pad_lf40"><a href="#1" onclick="javascript:detail_see('sub03_01_see_popup');" class="committee w186 spacing_f1 sub03_01_see_popup_open">저작권신탁관리업자 연락처 안내</a></div> -->
						<div class="float_lf pad_lf20">
							<div class=" mar_tp20 float_lf"><a href="#1" onclick="javascript:detail_see('sub03_01_see_popup');" class="committee w238_pad_lf40 sub03_01_see_popup_open">저작권신탁관리업자 연락처 안내</a></div>
						</div>
						<p class="clear"></p>
					<h1 class="sub_con_h1 mar_tp30">저작권자 검색 및 상당한 노력 신청(위원회 대행)</h1>
					<p class="word_dian_bg mar_tp20">위탁관리(신탁 및 대리중개업자, 900여 곳) 단체 및 등록부에서 관리하는 저작권 정보를 한 곳에 모아서 손쉽게 저작권자를 확인할 수 있는 저작권자 검색 서비스 제공 </p>
					<p class="word_dian_bg mar_tp5">저작권자 미 검색 시 법정허락의 사전절차인 저작권자를 찾기 위한 상당한 노력을 정부가 대신 해주는 서비스</p>
					<h2 class="sub_con_h2 mar_tp30">미분배 보상금 대상 저작물</h2>
					<!-- <div class="mar_tp30"><img src="/images/sub_img/sub_44.gif" alt="미분배 보상금 대상 저작물" /></div> -->
					<div class="mar_tp30"><img src="/images/sub/sub_tu5.jpg" alt="미분배 보상금 대상 저작물" /></div>
					<p class="mar_tp20 pad_lf20">공고 및 법정허락 신청 대상물 게시 단계에서 이의신청 및 승인이 완료된 저작물은 해당 대상에서 제외함</p>
					<h2 class="sub_con_h2 mar_tp30">통합검색 순서도</h2>
					<!-- <div class="mar_tp30"><img src="/images/sub_img/sub_41.gif" alt="통합검색 순서도" /></div> -->
					<div class="mar_tp30"><img src="/images/sub/sub_tu2.jpg" alt="통합검색 순서도" /></div>
					<p class="mar_tp20 pad_lf20">통합검색DB : 저작권등록부 , 위탁관리저작물 , 기승인된 법정허락저작물 , 법정허락 대상 저작물 DB</p>
					<h2 class="sub_con_h2 mar_tp30">상당한 노력 신청 절차</h2>
					<!-- <div class="mar_tp30"><img src="/images/sub_img/sub_45.gif" alt="통합검색 순서도" /></div> -->
					<div class="mar_tp30"><img src="/images/sub/sub_tu9.jpg" alt="통합검색 순서도" /></div>
			</div>
				<div id="sub03_01_see_popup" class="sub03_01_popup_bg disnone">
					<div class="sub03_01_pop_posi">
						<div class="sub03_01_pop_head">상당한 노력 방법 자세히 보기
							<a href="#1" onclick="closePop();" class="popup_close"><img src="/images/sub_img/sub_pop_close.gif" alt="그림" /></a>
						</div>
						<div class="sub03_01_pop_con">
							<table class="sub_tab td_cen tab_lfbor" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
								<caption>한국저작권위원회</caption>
								<colgroup>
									<col width="24%"/>
									<col width="14%"/>
									<col width="15%"/>
									<col width="12%"/>
									<col width="18%"/>
									<col width="*"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="row">보상금관리 신탁단체</th>
										<th scope="row" colspan="2">담당업무</th>
										<th scope="row">담당자</th>
										<th scope="row">전화번호</th>
										<th scope="row">Email</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>한국음악실연자연합회</td>
										<td colspan="2">보상금/저작권 찾기 <br />신청관리</td>
										<td>권기태 팀장</td>
										<td>02-2659-7048</td>
										<td>kjery@fkmp.kr</td>
									</tr>
									<tr>
										<td>한국음반산업협회</td>
										<td colspan="2">보상금/저작권 찾기 <br />신청관리</td>
										<td>김제경</td>
										<td>02-3270-5933</td>
										<td>kjk@riak.or.kr</td>
									</tr>
									<tr>
										<td rowspan="3">한국복제전송저작권협회</td>
										<td rowspan="2">보상금 <br />신청관리</td>
										<td>교과용<br />보상금</td>
										<td>김나영</td>
										<td>070-4265-2525</td>
										<td>nayoung@korra.kr</td>
									</tr>
									<tr>
										<td>도서관<br />보상금</td>
										<td>김광성 과장</td>
										<td>070-4265-2522</td>
										<td>kskim@korra.kr</td>
									</tr>
									<tr>
										<td colspan="2">저작권 찾기 신청관리</td>
										<td>심명관</td>
										<td>070-4265-2536</td>
										<td>smk@korra.kr</td>
									</tr>
								</tbody>
							</table>
							<table class="sub_tab td_cen tab_lfbor mar_tp30" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
								<caption>한국저작권위원회</caption>
								<colgroup>
									<col width="24%"/>
									<col width="29%"/>
									<col width="12%"/>
									<col width="18%"/>
									<col width="*"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="row">신탁단체</th>
										<th scope="row">담당업무</th>
										<th scope="row">담당자</th>
										<th scope="row">전화번호</th>
										<th scope="row">Email</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>한국음악저작권협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>전종훈</td>
										<td>02-2660-0585</td>
										<td>bigshow0411<br />@komca.or.kr</td>
									</tr>
									<tr>
										<td>한국문예학술저작권협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>박남규</td>
										<td>02-508-0440</td>
										<td>png@ekosa.org</td>
									</tr>
									<tr>
										<td>한국영화배급협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>김의수 부장</td>
										<td>02-3452-1001</td>
										<td>kjk@riak.or.kr</td>
									</tr>
									<tr>
										<td>한국영화제작자협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>정재훈</td>
										<td>02-2267-9983</td>
										<td>nayoung@korra.kr</td>
									</tr>
									<tr>
										<td>한국방송작가협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>김지숙 부장</td>
										<td>02-782-1696</td>
										<td>kskim@korra.kr</td>
									</tr>
									<tr>
										<td>한국시나리오작가협회</td>
										<td>저작권 찾기 신청관리</td>
										<td>정지영 부장</td>
										<td>02-2275-0566</td>
										<td>smk@korra.kr</td>
									</tr>
								</tbody>
							</table>
							
							<div class="align_cen mar_tp40"><a href="#1" onclick="closePop();" class="pop_check popup_close">확&nbsp;인</a></div>
						</div>
					</div>
				</div>
			</div>
				<p class="clear"></p>
	  	</div>
	  	<!-- //CONTAINER --> 
	    	<!-- FOOTER str-->
			<jsp:include page="/include/2012/footer.jsp" />
			<!-- FOOTER end -->
	    		    	
	
</body>
</html>