<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>이용문의 | 저작권찾기 소개 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title">저작권 찾기 소개</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/guideInfo.jsp">저작권 찾기 서비스란?</a></li>
				<li><a href="/mlsInfo/contactUs.jsp" class="on">이용문의</a></li>
				<li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				저작권 찾기 소개
				&gt;
				<span class="bold">이용문의</span>
			</div>
			<div class="con_rt_hd_title">이용문의</div>

			<!-- sub_contents_con -->

			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<div class="sub01_con_bg4_tp"></div>
				<div class="sub01_con_bg4">
					<h3>우리나라는 저작권법상 저작권위탁관리에 관한 규정을 두고 저작권신탁관리업 제도를 운영하고 있습니다</h3>
					<p class="word_dian_bg mar_tp5">1988년 한국음악저작권협회, 한국음악실연자연합회 및 한국방송작가협회를 시작으로, 현재 국내에는 각 분야별 총 13개 단체들이 문화체육관광부의 신탁허가를 받아 신탁관리를 해오고 있으며, 이들 단체는 ‘저작권 신탁관리’라는 고유의 업무 이외에도 다양한 활동을 통해 저작권 보호와 이용활성화를 하고 있습니다.</p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">한국저작권위원회</h1>
				<h2 class="sub_con_h2 mar_tp40">유통진흥팀</h2>
				<table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="*"/>
						<col width="10%"/>
						<col width="18%"/>
						<col width="25%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">담당업무</th>
							<th scope="col">담당자</th>
							<th scope="col">전화번호</th>
							<th scope="col" class="last_bor0">Email</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>법정허락 간소화 제도 운영 지원/저작권 찾기 캠페인 추진</td>
							<td>유동헌</td>
							<td>055-792-0122</td>
							<td class="last_bor0">dhyou@copyright.or.kr</td>
						</tr>
						<tr>
							<td>법정허락 시스템 운영지원</td>
							<td>유동헌</td>
							<td>055-792-0122</td>
							<td class="last_bor0">dhyou@copyright.or.kr</td>
						</tr>
					</tbody>
				</table>
				<h2 class="sub_con_h2 mar_tp40">심의조사팀</h2>
				<table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="*"/>
						<col width="10%"/>
						<col width="18%"/>
						<col width="25%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col">담당업무</th>
							<th scope="col">담당자</th>
							<th scope="col">전화번호</th>
							<th scope="col" class="last_bor0">Email</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>법정허락제도 운영 지원</td>
							<td>정희정</td>
							<td>055-792-0087</td>
							<td class="last_bor0">629ashley@copyright.or.kr</td>
						</tr>
					</tbody>
				</table>
				<h1 class="sub_con_h1 mar_tp40">보상금관리 신탁단체</h1>
				<h2 class="sub_con_h2 mar_tp40">한국음악실연자연합회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg01">
						<a href="http://www.fkmp.kr/" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국음악실연자연합회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="27%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>보상금/저작권찾기 신청관리</td>
									<td>권기태 팀장</td>
									<td>02-2659-7048</td>  
									<td class="last_bor0">kjery@fkmp.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">한국음반산업협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg02">
						<a href="http://www.riak.or.kr/" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국음반산업협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="27%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>보상금/저작권찾기 신청관리</td>
									<td>김현지</td>
									<td>02-3270-5963</td>
									<td class="last_bor0">chcho@riak.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">한국복제전송저작권협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg03">
						<a href="http://www.korra.kr" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국저작권위원회</caption>
							<colgroup>
								<col width="*"/>
								<col width="12%"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="22%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col" colspan="2">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td rowspan="3">보상금<br />신청관리</td>
									<td>교과용<br />보상금</td>
									<td>김세진 주임</td>
									<td>02-2608-2038</td>
									<td class="last_bor0">serin@korra.kr</td>
								</tr>
								<tr>
									<td>도서관<br />보상금</td>
									<td>김소희 주임</td>
									<td>02-4265-2525</td>
									<td class="last_bor0">sohee@korra.kr</td>
								</tr>
								<tr>
									<td>수업목적<br />보상금</td>
									<td>김소희 주임</td>
									<td>070-4265-2525</td>
									<td class="last_bor0">sohee@korra.kr</td>
								</tr>
								<tr>
									<td colspan="2">저작권찾기 신청관리</td>
									<td>오기민</td>
									<td>02-2608-2036</td>
									<td class="last_bor0"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h1 class="sub_con_h1 mar_tp40">신탁단체</h1>
				<h2 class="sub_con_h2 mar_tp40">한국음악저작권협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg04">
						<a href="http://www.komca.or.kr" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국저작권위원회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>전종훈 팀장</td>
									<td>02-2660-0531</td>
									<td class="last_bor0">bigshow0411@komca.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">함께하는음악저작인협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg05">
						<a href="http://www.koscap.or.kr" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>함께하는음악저작인협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>이용수 차장</td>
									<td>02-333-8766</td>
									<td class="last_bor0">yongsoo.lee@koscap.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">한국문예학술저작권협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg06">
						<a href="http://www.copyrightkorea.or.kr" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국문예학술저작권협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>서영훈 사원</td>
									<td>02-508-0440<br/>(대표)</td>
									<td class="last_bor0">syh@ekosa.org</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">한국영화배급협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg07">
						<a href="http://www.mdak.or.kr/" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국영화배급협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>안형진 대리</td>
									<td>02-3452-1004</td>
									<td class="last_bor0">webmaster@mdak.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">사단법인 한국방송작가협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg08">
						<a href="http://www.ktrwa.or.kr/" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>사단법인 한국방송작가협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>윤선미 과장</td>
									<td>02-782-1696</td>
									<td class="last_bor0">copyright@ktwa.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				<h2 class="sub_con_h2 mar_tp40">사단법인 한국시나리오작가협회</h2>
				<div class="mar_tp15">
					<div class="float_lf sub01_logo_bg09">
						<a href="http://www.scenario.or.kr/" class="shortcut"><img src="/images/sub_img/sub_09.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>사단법인 한국시나리오작가협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>전수영 총무</td>
									<td>02-2275-0566(대표)</td>
									<td class="last_bor0">scenario11@hanmail.net</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				
				<h2 class="sub_con_h2 mar_tp40">한국영화제작가협회</h2>
				<div class="mar_tp15">
					<div class="float_lf">
						<a href="http://new.kfpa.net/"><img src="/images/2012/link/btn_logo13.png" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국영화제작가협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>김민희 대리</td>
									<td>02-3153-2884</td>
									<td class="last_bor0">kfpa3@kfpa.net</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				
				<h2 class="sub_con_h2 mar_tp40">한국문화정보원</h2>
				<div class="mar_tp15">
					<div class="float_lf">
						<a href="http://www.kcisa.kr/"><img src="/images/2012/link/btn_logo14.jpg" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국문화정보원</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>류지현 선임</td>
									<td>02-3153-2873</td>
									<td class="last_bor0">mhkim@kcisa.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				
				<h2 class="sub_con_h2 mar_tp40">한국언론진흥재단</h2>
				<div class="mar_tp15">
					<div class="float_lf">
						<a href="http://www.kpf.or.kr/"><img src="/images/2012/link/btn_logo15.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국언론진흥재단</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>정제우 사원</td>
									<td>02-2001-7797</td>
									<td class="last_bor0">jaewoo@kpf.or.kr</td>
								</tr>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>양승혜 팀장</td>
									<td>02-2001-7791</td>
									<td class="last_bor0">shyang@kpf.or.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				
				<h2 class="sub_con_h2 mar_tp40">한국방송실연자협회</h2>
				<div class="mar_tp15">
					<div class="float_lf">
						<a href="http://www.kbpa.kr/"><img src="/images/2012/link/btn_logo16.gif" alt="바로가기" /></a>
					</div>
					<div class="float_rt w_b75">
						<table class="sub_tab td_cen" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
							<caption>한국방송실연자협회</caption>
							<colgroup>
								<col width="*"/>
								<col width="13%"/>
								<col width="18%"/>
								<col width="30%"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="col">담당업무</th>
									<th scope="col">담당자</th>
									<th scope="col">전화번호</th>
									<th scope="col" class="last_bor0">Email</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>저작권찾기 신청관리</td>
									<td>장유정</td>
									<td>02-784-7801</td>
									<td class="last_bor0">kbpa@kbpa.kr</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p class="clear"></p>
				</div>
				
				<h1 class="sub_con_h1 mar_tp40">저작권 상담 안내</h1>
				<div class="sub01_con_bg4_tp mar_tp20"></div>
				<div class="sub01_con_bg4">
					<p class="word_dian_bg mar_tp5">저작권 관련으로 보다 자세한 상담을 원하시면, <br/>
					저작권자동상담시스템(http://www.copyright.or.kr/customer/counsel/main.do)에 문의를 하세요.</p>
					<p class="word_dian_bg mar_tp5">저작권 법률에 대하여 유형별상담, 업무별상담이 가능하며, 법정허락에 대해서 문의가 가능합니다.</p>
				</div>
				<div class="sub01_consultation_bg mar_tp20">
					<span class="sub01_consultation_bg1"><a href="http://counsel.copyright.or.kr/autoCounsel/autoCounselList.srv?searchTarget=&amp;searchWord=&amp;page=1&amp;category_seq=1&amp;category_type=001&amp;use_form_type=all&amp;seq=0" class="shortcut2"><img src="/images/sub_img/sub_10_02.gif" alt="바로가기" /></a></span>
					<span class="sub01_consultation_bg2"><a href="http://counsel.copyright.or.kr/autoCounsel/autoCounselList.srv?searchTarget=&amp;searchWord=&amp;page=1&amp;category_seq=3&amp;category_type=001&amp;use_form_type=all&amp;seq=0" class="shortcut2"><img src="/images/sub_img/sub_11_02.gif" alt="바로가기" /></a></span>
					<span class="sub01_consultation_bg3"><a href="http://counsel.copyright.or.kr/autoCounsel/autoCounselList.srv?searchTarget=&amp;searchWord=&amp;page=1&amp;category_seq=97&amp;category_type=&amp;seq=0" class="shortcut2"><img src="/images/sub_img/sub_12_02.gif" alt="바로가기" /></a></span>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
	    		    	
	
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>