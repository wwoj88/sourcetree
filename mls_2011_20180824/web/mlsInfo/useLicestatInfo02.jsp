<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>미분배 보상금 관리저작물 등록안내 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 
//-->
</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
<STYLE type="text/css">
.pr_area .pr_mv{ background: url(/images/2012/content/pr_box1.gif) no-repeat 0 0; padding: 22px 0 0 26px; position: relative; height: 162px;}
.pr_area .pr_mv h3{ background: none; padding: 0;}
.pr_area .pr_mv .pr_down{ position: absolute; top: 24px; right: 0; width: 317px; height: 165px; background: url(/images/2012/content/pr_down.png) no-repeat 0 0;}
.pr_area .pr_mv .pr_down a{ display: block; margin: 49px 0 0 133px; width: 58px; height: 55px; text-indent: -10000000px;}
.pr_area .pr_mv .btn_download{ display: block; margin: 10px 345px 0 0; text-align: right}
</STYLE>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader4.jsp" /> --%>
		<!-- 2017 주석처리 -->
<!-- 		<script type="text/javascript">initNavigation(4);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title" style="padding:23px 0;">관리저작물<br />등록 안내</div>
			<ul class="sub_lf_menu">
				<li><a href="/mlsInfo/useLicestatInfo01.jsp">미분배 보상금 대상 저작물 등록</a></li>
				<li><a href="/mlsInfo/useLicestatInfo02.jsp" class="on">위탁관리저작물 등록</a></li>
				<li><a href="/mlsInfo/useLicestatInfo03.jsp">위탁관리저작물 정보 등록기관 현황</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				관리저작물 등록 안내
				&gt;
				<span class="bold">위탁관리저작물 등록</span>
			</div>
			<div class="con_rt_hd_title">위탁관리저작물 등록</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<!-- 
				<div class="bg_f8f8f8">
					<div>
						<div class="float_lf">
							<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
							<input type="checkbox" id="inp1" /><label for="inp1">전체</label>
							<input type="checkbox" id="inp2" /><label for="inp2">저작물명</label>
							<input type="checkbox" id="inp3" /><label for="inp3">저작권자</label>
						</div>
						<div class="float_rt">
							<span class="sub_blue_point_bg">검색대상</span>&nbsp;&nbsp;&nbsp;
							<select>
								<option selected="selected">정확도순</option>
								<option>정확도</option>
								<option>정확</option>
							</select>&nbsp;&nbsp;&nbsp;
							<select>
								<option selected="selected">내림차순</option>
								<option>내림차</option>
								<option>내림</option>
							</select>
						</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp10">
						<select title="김색 족근을 선택 해주세요">
							<option selected="selected">통합검색</option>
							<option>정확도</option>
							<option>정확</option>
						</select>&nbsp;&nbsp;&nbsp;
						<select title="김색 족근을 선택 해주세요">
							<option selected="selected">전체</option>
							<option>내림차</option>
							<option>내림</option>
						</select>
						<input type="text" title="김색 내용을 입럭 해주세요" style="padding:3px;width:57%;" /><a href="#none" class="search_button">검색</a>
						<input type="checkbox" id="inp4" /><label for="inp4">결과 내 검색</label>
					</div>
				</div>
				 -->
				<div class="sub01_con_bg4_tp mar_tp30"></div>
				<div class="sub01_con_bg4">
					<h3 style="font-size:22px;">제출근거 및 목적</h3>
					<p class="mar_tp20">저작권법 시행령 제52조 제3항에 의거 위탁관리업자는 관리 저작물을 매월 10일까지 보고하도록 함</p>
					<p class="mar_tp5">저작권법 시행령 제18조 제2항 제2호에 근거하여 문화체육관광부 장관은 위탁관리업자가 보고한 사항을 근거로  </p>
					<p class="mar_tp5">해당 저작물의 저작재산권자나 그의 거소를 조회하는 등의 저작권자 찾기 위한 상당한 노력을 수행하기 위함</p>
				</div>
				<h2 class="sub_con_h2 mar_tp30">대상기관</h2>
				<p class="mar_tp10">신탁관리단체, 대리중개업체</p>
				<h2 class="sub_con_h2 mar_tp30">등록절차</h2>
				<div class="mar_tp30">
					<span class="sub04_img_bg w145">회원가입<br />(로그인)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w145">저작물 등록<br />(매년)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w145">변경 등록 <br />(매월 10일)</span>
				</div>
				<h2 class="sub_con_h2 mar_tp30">등록방법</h2>
				<div class="mar_tp30">
					<span class="sub04_img_bg" style="width: 266px;">저작권위탁관리업시스템 최초 허가/신고한 계정으로 로그인<br />(www.cocoms.go.kr)</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w188" style="width: 136px;"> 위탁관리저작물 보고/수정</span>
					<img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
					<span class="sub04_img_bg w217" style="width: 251px;">저작물 종류에 맞는 양식 파일 다운 후 위탁관리저작물 정보 입력</span>
					 <img src="/images/sub_img/sub_48.gif" alt="그림" style="vertical-align:top;" />
          <span class="sub04_img_bg w217" style="width: 251px;">보고자 정보 입력 및 파일 업로드 후 보고</span>
				</div>
				<%-- <div class="mar_tp10">※ 모든 항목이 <span class="color_d4281e">필수 입력사항</span>으로 데이터가 없을 경우 ‘없음’으로 입력 <a href="#1" onclick="javascript:fn_fileDownLoad('<spring:message code='file.form' />','위탁관리저작물_제출항목(양식).xlsx','위탁관리저작물_제출항목(양식).xlsx')" class="pop_check">양식 다운로드</a></div> --%>
				<h2 class="sub_con_h2 mar_tp30">등록항목</h2>
				<div class="sub01_con_bg4 mar_tp30" style="border-top:1px solid #dddddd;line-height:19px;">
				<form name="form1" method="post" action="#">
					<input type="hidden" name="filePath"> <input type="hidden"
						name="fileName"> <input type="hidden" name="realFileName">
					<input type="hidden" name="action_div"> <input type="submit"
						style="display: none;">
				</form>
					<h3 class="mar_tp20">음악</h3>
					<div class="mar_tp5">
						<div class="float_lf">* 음저협 : </div>
						<div class="float_lf" style="width:90%;"> 작성기관명, 기관내부관리ID, 곡명, 국내외구분, 앨범명, 작사가명, 작곡가명, 편곡가명, 역사가명,가수명, 음반제작자명, 위탁관리구분</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp5">
						<div class="float_lf">* 함저협 : </div>
						<div class="float_lf" style="width:90%;">작성기관명, 기관내부관리ID, 곡명, 국내외구분, 앨범명, 작사가명, 작곡가명, 편곡가명, 역사가명,가수명, 음반제작자명, 위탁관리구분</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp5">
						<div class="float_lf">* 음실연 : </div>
						<div class="float_lf" style="width:90%;">작성기관명, 기관내부관리ID, 곡명, 국내외구분, 앨범명, 앨범제작사, 디스크면, 트랙번호, 앨범발매년도, 가수명, 연주자명,음반제작자명, 위탁관리구분</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp5">
						<div class="float_lf">* 음산협 : </div>
						<div class="float_lf" style="width:90%;">작성기관명, 기관내부관리ID, 곡명, 국내외구분, 앨범명, 앨범제작사, 디스크면, 트랙번호,  앨범발매년도, 가수명, 음반제작자명, 위탁관리구분</div>
						<p class="clear"></p>
					</div>
					<div class="mar_tp5">
						<div class="float_lf">* 대리중개 : </div>
						<div class="float_lf" style="width:88%;">작성기관명, 기관내부관리ID, 곡명, 국내외구분, 앨범명, 앨범제작사, 디스크면, 트랙번호, 앨범발매년도,  <br /> 작사가명, 작곡가명, 편곡가명, 역사가명, 가수명, 연주자명, 음반제작자명, 위탁관리구분, 저작권자 권리처리구분(신탁관리단체계약, 개별계약, 권리자), 실연자 권리처리구분(신탁관리단체계약, 개별계약, 권리자), <br />음반제작자권리처리구분(신탁관리단체계약, 개별계약, 권리자)</div>
						<p class="clear"></p>
					</div>
					<p class="mar_tp5">- 권리처리구분이 개별계약일 경우 작사,저작권자 및 실연자,음반제작자 정보 입력 필수</p>
					<h3 class="mar_tp20">어문</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 저작물명, 저작물부제, 창작년도, 도서명, 출판사, 발행년도, 저작자명, 위탁관리구분</p>
					<h3 class="mar_tp20">방송대본</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 작품명(대제목), 원작명, 장르분류, 소재분류, 원작작가명, 주요출연진, 방송횟차, 방송일자, &nbsp;&nbsp;제작사, 작가(저자), 연출자명, 위탁관리구분</p>
					<h3 class="mar_tp20">영화</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 제목(국문), 제작년도, 출연자명, 감독, 작가명, 연출자명, 제작자명, 배급사명, 투자자명, <br/>&nbsp;&nbsp;위탁관리구분</p>
					<h3 class="mar_tp20">방송</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 제목, 장르구분, 제작년도, 회차, 연출자명, 작가명, 위탁관리구분</p>
					<h3 class="mar_tp20">뉴스</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 제목, 지면번호, 지면면종, 기고자, 기자명, 변경버전, 기사일자, 기사발행일시, 언론사명, <br/>&nbsp;&nbsp;위탁관리구분</p>
					<h3 class="mar_tp20">미술</h3>
					<p class="mar_tp5">- 작성기관명, 기관내부관리ID, 제목, 부제목, 분류, 저작년월일, 출처, 크기(mm), 주재료, 구조 및 특징, 작가, 소장년월일, &nbsp;&nbsp;소장기관명, 위탁관리구분</p>
					<h3 class="mar_tp20">기타</h3>
					<p class="mar_tp5">- 작성기관명, 기관 내부관리ID, 저작물종류, 저작물명 또는 키워드, 저작자명, 창작년도, 공표매체명, 공표일자, <br/>&nbsp;&nbsp;위탁관리구분</p>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
	  	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
</body>
</html>