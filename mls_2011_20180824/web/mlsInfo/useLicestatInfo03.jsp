<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>위탁관리저작물 정보 등록기관 현황 | 저작권자찾기 | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<!-- <script type="text/javascript" src="/js/2012/deScript.js"></script> -->
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
function fn_fileDownLoad(filePath, fileName, realFileName) {
	var frm = document.form1;
	frm.filePath.value     = filePath;
	frm.fileName.value     = fileName;
	frm.realFileName.value = realFileName;

	frm.action = "/board/board.do?method=fileDownLoad";
	frm.submit();
} 
//-->


</script>
<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69621660-2', 'auto');
  ga('send', 'pageview');
</script>
<STYLE type="text/css">
.pr_area .pr_mv{ background: url(/images/2012/content/pr_box1.gif) no-repeat 0 0; padding: 22px 0 0 26px; position: relative; height: 162px;}
.pr_area .pr_mv h3{ background: none; padding: 0;}
.pr_area .pr_mv .pr_down{ position: absolute; top: 24px; right: 0; width: 317px; height: 165px; background: url(/images/2012/content/pr_down.png) no-repeat 0 0;}
.pr_area .pr_mv .pr_down a{ display: block; margin: 49px 0 0 133px; width: 58px; height: 55px; text-indent: -10000000px;}
.pr_area .pr_mv .btn_download{ display: block; margin: 10px 345px 0 0; text-align: right}

.companyBox img{
max-width:160px;
max-height:80px; 
border:0;
margin-left: 16px;
margin-right: 39px;
}

.companyBox td{
border-right:none;
border-left:none;
border-top:none;
border-bottom:none;
text-align: left;
}
</STYLE>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<script type="text/javascript">
		$(function(){
			for(var i = 1 ; i <= 13 ; i++)
			{
				 $('#companyBtn'+i).css('cursor','pointer');
				 $('#companyBtn'+i).click(companyBtnClick); 
			}
		})

		function companyBtnClick(event){
			var companyUrl = ["http://www.copyrightkorea.or.kr","http://www.kcisa.kr","http://www.kbpa.co.kr","http://www.ktrwa.or.kr",
					"http://www.korra.kr","http://www.scenario.or.kr","http://www.kpf.or.kr","http://www.kmva.or.kr","http://www.kfpa.net"
					,"http://www.riak.or.kr","http://www.fkmp.kr","http://www.komca.or.kr","http://www.fkmp.kr","http://www.komca.or.kr"
					,"http://www.koscap.or.kr"];
			var targetId = event.target.id;
			var targetNum = targetId.substr(targetId.lastIndexOf("n")+1);
			console.log(companyUrl[(targetNum-1)]);
			//window.open(, '_blank'); 
			var openNewWindow = window.open("about:blank");
			openNewWindow.location.href = companyUrl[(targetNum-1)];
			//location.href = "http://www.copyrightkorea.or.kr";


		}
		</script>
		<%-- <jsp:include page="/include/2012/subHeader4.jsp" /> --%>
		<!-- 2017 주석처리 -->
<!-- 		<script type="text/javascript">initNavigation(4);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
	<div id="contents">
		<div class="con_lf">
			<div class="con_lf_big_title" style="padding:23px 0;">관리저작물<br />등록 안내</div>
			<ul class="sub_lf_menu">
				<!-- <li><a href="/mlsInfo/useLicestatInfo01.jsp">미분배 보상금 대상 저작물 등록</a></li> -->
				<li><a href="/mlsInfo/useLicestatInfo02.jsp">위탁관리저작물 등록</a></li>
				<li><a href="/mlsInfo/useLicestatInfo03.jsp" class="on">위탁관리저작물 정보 등록기관 현황</a></li>
			</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				관리저작물 등록 안내
				&gt;
				<span class="bold">위탁관리저작물 정보 등록기관 현황</span>
			</div>
			<div class="con_rt_hd_title">위탁관리저작물 정보 등록기관 현황</div>
			<div id="sub_contents_con">
				<h1 class="sub_con_h1">국내</h1>
				<table class="sub_tab td_cen mar_tp15" style="width:100%;  border:;">
					<tr>
					<td style="margin: 0px;padding: 0px; width: 50%" >
					<table class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img050.gif"></td>
						<td>한국문예학술저작권협회</td>
						</tr>
						<tr>
						<td><img id="companyBtn1" src="/images/2017/companyLogo/btn_link.gif"></td>
						</tr>
					</table>
					
					<td class="last_bor0" style="margin: 0px;padding: 0px; width: 50%">
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img051.gif"></td>
						<td> 한국문화정보원</td>
						</tr>
						<tr>
						<td><img id="companyBtn2" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img052.gif"></td>
						<td> 한국방송실연자협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn3" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					<td class="last_bor0">
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img053.gif"></td>
						<td> 한국방송작가협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn4" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img054.gif"></td>
						<td> 한국복제전송저작권협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn5" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					<td class="last_bor0">
						<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img055.gif"></td>
						<td> 한국시나리오작가협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn6" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img056.gif"></td>
						<td> 한국언론진흥재단</td>
						</tr>
						<tr>
						<td><img  id="companyBtn7" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					<td class="last_bor0">
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img057.gif"></td>
						<td> 한국영화배급협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn8" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img058.gif"></td>
						<td> 한국영화제작가협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn9" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					<td class="last_bor0">
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img059.gif"></td>
						<td> 한국음반산업협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn10" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img060.gif"></td>
						<td> 한국음악실연자연합회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn11" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					
					<td class="last_bor0">
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img061.gif"></td>
						<td> 한국음악저작권협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn12" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td>
					<table  class="companyBox">
						<tr >
						<td rowspan="2" width ="215px"><img src="/images/2017/companyLogo/organ_img062.gif"></td>
						<td> 함께하는음악저작인협회</td>
						</tr>
						<tr>
						<td><img  id="companyBtn13" src="/images/2017/companyLogo/btn_link.gif"></td></td>
						</tr>
					</table>
					</td>
					<td class="last_bor0"></td>
					</tr>
				</table>

		</div>
		<p class="clear"></p>
	</div>
	  	<!-- //CONTAINER --> 
	  	
	  	<!-- FOOTER str-->
	  	<!-- 2017변경 -->
		<jsp:include page="/include/2017/footer.jsp" />
			<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
		<!-- FOOTER end -->
	
</body>
</html>