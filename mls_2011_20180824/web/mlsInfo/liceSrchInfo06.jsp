<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기 소개 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<div id="header">
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		</div>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis3">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	  <!-- 래프 -->
					<jsp:include page="/include/2012/leftMenu03.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb1","lnb11"); 
	 				</script>
					<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      	<p class="path"><span>Home</span><span>저작권자찾기소개</span> <span>저작권자 검색 및 상당한 노력 신청</span> <em>소개 및 이용방법</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0301.gif" alt="소개 및 이용방법" title="소개 및 이용방법" /></h1>
			      	
			           <!-- Tab str -->
                              <ul class="tab_menuBg">
	                             <li class="first"><a href="/mlsInfo/liceSrchInfo01.jsp">소개</a></li>
	                             <li class="on"><strong><a href="/mlsInfo/liceSrchInfo02.jsp">이용방법</a></strong></li>
                             </ul>
                             <!-- //Tab -->
			      	
			      		<div class="section">
			      			
			      			<h2 class="mt20">상당한노력신청(개인) 안내</h2>
			      			
			      			<ul class="tab_style2">
			      				<li class="bgNone"><a href="/mlsInfo/liceSrchInfo02.jsp">미분배 보상금 대상 저작물</a></li>
			      				<li><a href="/mlsInfo/liceSrchInfo05.jsp">저작권자 검색 및 상당한 노력 신청</a></li>
			      				<li class="active"><a href="/mlsInfo/liceSrchInfo04.jsp"">개인직접 수행</a></li>
			      			</ul>
			      			
			      			<ul class="tab_style floatDiv">
			      			<li><a href="/mlsInfo/liceSrchInfo04.jsp">저작권등록부 열람안내</a></li>
			      			<li><a href="/mlsInfo/liceSrchInfo03.jsp">신탁단체확인요청안내</a></li>
			      			<li class="active"><a href="/mlsInfo/liceSrchInfo06.jsp">저작권자 조회공고 안내</a></li>
			      			</ul>
			      			
			      			
			      			<div class="white_box">
                                <div class="box5">
                                 <div class="box5_con floatDiv">
                                  <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
                                  <div class="fl ml30 ">
                                   <ul class="list1 mt10">
                                   <li>찾고자 하는 저작물에 대해 저작권등록부 열람 및 신탁단체에 조회요청하였으나 저작물의 저작재산권자나<br/> 그의 거소의 조회가 불가능한 경우, 열람 및 조회 내역을 저작권자 조회공고 게시판에 본인이 직접 게시합니다.</li>
				                   <li>저작권법 제18조 제3항 : 저작재산권자나 그의 거소 등 문화체육관광부령으로 정하는 사항을 다음 각 목의 <BR />어느 하나에 공고한 날부터 10일이 지났을 것
  <br/>가.「신문 등의 진흥에 관한 법률」제9조1항에 따라 보금지역을 전국으로 하여 등록한 일반일간신문
  <br/>나. 문화체육관광부의 인터넷 홈페이지와 제73조제2항에 따른 권리자가 불명인 저작물등의 권리자 찾기<br/> 정보시스템(이하 "권리자 찾기 정보시스템"이라 한다)</li>                                   
  								  </ul>
                                  </div>
                                 </div>
                                </div>
                              </div>
                              <div class="floatDiv mt20">
                              		<div class="fl w48">
                              			<h3>저작권자 조회공고 바로가기</h3>
                              			<p class="mt15"><img src="/images/2012/content/site_right4me1.gif" alt="" /><a href="/statBord/statBo01List.do?bordCd=1&divsCd=5" class="block ce mt5" target="_blank"><img alt="사이트 가기" src="/images/2012/button/go_site.gif" /></a></p>
                              		</div>
                              		<div class="fl w48 ml25">
                              			<h3>문화체육관광부 홈페이지 이동</h3>
                              			<p class="mt15"><img src="/images/2012/content/site_mcst1.gif" alt="" /><a href="http://www.mcst.go.kr" class="block ce mt5" target="_blank"><img alt="사이트 가기" src="/images/2012/button/go_site.gif" /></a></p>
                              		</div>
                             </div>
					</div>
					<!-- //section -->
					
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>

			      		