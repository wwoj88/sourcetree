<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권 찾기 서비스란? | 저작권 찾기 소개 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">

<script src="/js/jquery-1.7.js"  type="text/javascript"></script>

<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<!-- <script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script> -->
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-69621660-2', 'auto');
 ga('send', 'pageview');
//-->
</script>
</head>

<body>
		
		<!-- HEADER str-->
		<jsp:include page="/include/2017/header.jsp" />
<!-- 		<script type="text/javascript">initNavigation(1);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->

		<!-- HEADER end -->
		
		<!-- content st -->
			<div id="contents">
					<div class="con_lf">
						<div class="con_lf_big_title">저작권 찾기 소개</div>
						<ul class="sub_lf_menu">
							<li><a href="/mlsInfo/guideInfo.jsp" class="on">저작권 찾기 소개</a></li>
							<li><a href="/mlsInfo/contactUs.jsp">이용문의</a></li>
							<li><a href="/mlsInfo/comeInfo.jsp">찾아오시는 길</a></li>
						</ul>
					</div>
					<div class="con_rt">
						<div class="con_rt_head">
							<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
							&gt;
							저작권 찾기 소개
							&gt;
							<span class="bold">저작권 찾기 소개</span>
						</div>
						<div class="con_rt_hd_title">저작권 찾기 소개</div>
						<div id="sub_contents_con">
							<!--  --><!--  --><!--  -->
							<h1 class="sub_con_h1">저작권 찾기 서비스란?</h1>
							 <div class="mar_tp20">저작권 찾기 서비스는 권리자 입장에서 보상금 수령단체에서 분배되지 못한 저작물 정보를 모아 권리자가 보상금을 수령할 수</div>
							 <div class="mar_tp5">있도록 안내하고, 이용자 입장에서는 저작권자를 찾지 못해 저작물을 유통하지 못하거나 이용하지 못했던 분들을 위해 저작권</div>
							 <div class="mar_tp5">정보 검색 서비스를 제공하고 있습니다. </div>
							 <div class="mar_tp5">또, 법령에 의해 저작물 이용을 승인해주는 법정허락 신청서비스와 저작권자를 찾기 위한 상당한 노력을 대신 수행하고 있습니다.</div>
							 <br/>
							<div class="mar_tp20" style="margin-left:50px;">
								<img src="/images/sub/sub_tu1.jpg" alt="저작권서비스저작권 정보제공관리자미분배 보상금대상 저작물 안내,저작권 정보수정 안내.저작권서비스저작권 정보제공이용자상단한 노력 신청 대행,법정허락 신청 대행.">
							<!-- 
								<div class="sub01_con_bg1">저작권 찾기 서비스</div>
								<img src="/images/sub_img/sub_02.png" alt="그림" />
								<div class="sub01_con_bg1">저작권 정보 제공</div>
								<div class="w400 mar_auto">
									<div class="float_lf"><img src="/images/sub_img/sub_03.png" alt="권리자" /></div>
									<div class="float_rt"><img src="/images/sub_img/sub_04.png" alt="이용자" /></div>
									<p class="clear"></p>
								</div>
								<div class="mar_tp10">
									<span class="sub01_con_bg2">미분배 보상금<br />대상 저작물 안내</span>
									<span class="sub01_con_bg2">저작권 정보<br />수정 안내</span>
									<span class="sub01_con_bg2">상당한 노력<br />신청 대행</span>
									<span class="sub01_con_bg2">법정허락<br />신청 대행</span>
								</div>
							-->
							</div>
							<h1 class="sub_con_h1 mar_tp40">권리자에게는</h1>
							<div class="mar_tp20">저작권 정보를 확인할 수 있는 검색 서비스 및 미분배 보상금 대상 저작물 확인 서비스를 제공하고 있습니다.</div>
							<div class="mar_tp20">
								<span class="sub01_con_bg3"><a href="/mlsInfo/rghtInfo01.jsp">저작권 정보 확인 안내 </a></span>
								<span class="sub01_con_bg3"><a href="/mlsInfo/inmtInfo02.jsp">미분배 보상금 대상 저작물 확인 안내</a></span>
							</div>
							<h1 class="sub_con_h1 mar_tp40">이용자에게는</h1>
							<div class="mar_tp20">저작물의 권리자를 확인할 수 있는 검색 서비스 및 법정허락 승인 신청을 위한 상당한 노력 신청 서비스를 제공하고 있습니다.</div>
							<div class="mar_tp20">
								<span class="sub01_con_bg3"><a href="/mlsInfo/liceSrchInfo01.jsp" style="margin-right: 20px;">저작권자 검색 및 상당한 노력 신청 서비스 안내</a></span>
								<span class="sub01_con_bg3"><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청서비스 안내</a></span>
							</div>
							<!--  --><!--  --><!--  -->
						</div>
			
						<!-- sub_contents_con -->
						<!-- head_and_lf -->
			
					</div>
					<p class="clear"></p>
				</div>

				<!-- //주요컨텐츠 end -->
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	    	
	    		    	
	<!-- //전체를 감싸는 DIVISION -->

</body>
</html>