<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="kr.or.copyright.mls.common.utils.CommonUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="java.util.List"%>
<%
	String menuSeqn = request.getParameter("menuSeqn");
	String srchDivs = request.getParameter("srchDivs") == null ? "" : request.getParameter("srchDivs");
	String srchText = request.getParameter("srchText") == null ? "" : request.getParameter("srchText");
	String page_no  = request.getParameter("page_no") == null ? "" : request.getParameter("page_no");
%>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ page contentType="text/html;charset=euc-kr" %>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>알림마당(저작권찾기 캠페인) | 저작권찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link type="text/css" rel="stylesheet" href="/css/2012/style.css">
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/JavaScript">
<!--

function imgDetail(id) {

	if(id=='image2012'){
		jQuery("#tab12").css({ display :'block' });
		jQuery("#tab11").css({ display :'none' });
		jQuery("#image2012").css({ display :'block' });
		jQuery("#image2011").css({ display :'none' });
	}
	else if(id=='image2011'){
		jQuery("#tab12").css({ display :'none' });
		jQuery("#tab11").css({ display :'block' });
		jQuery("#image2012").css({ display :'none' });
		jQuery("#image2011").css({ display :'block' });
	}
	
}
	
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		<!-- HEADER str-->
		
		<jsp:include page="/include/2012/header.jsp" />
		<script type="text/javascript">initNavigation(5);</script>
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
			
		<!-- HEADER end -->
		<!-- CONTAINER str-->
		<div id="container" style="background: url(/images/2012/content/container_vis5.gif) no-repeat 100% 0;">
			<div class="container_vis" style="background: url(/images/2012/content/container_vis5.gif) no-repeat 100% 0;">
				<h2><span><img src="/images/2012/title/container_vis_h2_5.gif" alt="알림마당" title="알림마당" /><em><img src="/images/2012/common/container_vis_txt.gif" alt="저작물에 대한 주인의식이 필요합니다!" title="저작물에 대한 주인의식이 필요합니다!" /></em></span></h2>
				<p class="fr mr20"><img src="/images/2012/common/container_vis.gif" alt="" /></p>
			</div>
			<div class="content">
			
			<!-- 래프 -->
				<jsp:include page="/include/2012/leftMenu05.jsp" />				
					<script type="text/javascript">subSlideMenu("sub_lnb","lnb4");</script>
			<!-- //래프 -->
				
				
				<!-- 주요컨텐츠 str -->
				<div class="contentBody" id="contentBody">
					<p class="path"><span>Home</span><span>알림마당</span><em>저작권찾기 캠페인</em></p>
					<h1><img src="/images/2012/title/content_h1_0504.gif" alt="저작권찾기 캠페인" title="저작권찾기 캠페인" /></h1>
					
						<!-- Tab str 2012 -->
                          <ul id="tab12" class="tab_menuBg" style="display: block;">
                              <li class="first on" ><strong><a href="javascript:imgDetail('image2012')">2012</a></strong></li>
							  <li><a href="javascript:imgDetail('image2011')">2011</a></li>
                      		</ul>
                    	<!-- //Tab -->
                    
                    	<!-- Tab str 2011-->
                          <ul id="tab11" class="tab_menuBg" style="display: none;">
                              <li class="first"><a href="javascript:imgDetail('image2012')">2012</a></li>
							  <li class="on"><strong><a href="javascript:imgDetail('image2011')">2011</a></strong></li>
                      		</ul>
                    	<!-- //Tab -->
                    
                    	<!--image 2012  -->
						<div id="image2012" style="display: block; margin-bottom: 40px;" class="mt20">
			    			<img alt="저작권찾기 캠페인 2012" src="/images/2012/banner/event_2012.jpg">
						</div>
						<!--image 2011  -->
						<div id="image2011" style="display: none; margin-bottom: 40px;" class="mt20">
			    			<img alt="저작권찾기 캠페인 2012" src="/images/2012/banner/event_2011.jpg">
						</div>
				</div>
			</div>
		</div>
		
		<!-- FOOTER str-->
		<jsp:include page="/include/2012/footer.jsp" />
		<!-- FOOTER end -->
	</div>
	
	<!-- //전체를 감싸는 DIVISION -->
</body>
</html>
