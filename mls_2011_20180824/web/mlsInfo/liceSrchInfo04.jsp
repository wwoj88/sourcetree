<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>저작권찾기 소개 | 저작권자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common.css">
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript"> 
<!--
//-->
</script>
</head>

<body>
	<!-- 전체를 감싸는 DIVISION -->
	<div id="wrap">
		
		<!-- HEADER str-->
		<div id="header">
		<jsp:include	page="/include/2012/header.jsp" /> 
		<script type="text/javascript">initNavigation(3);</script>
		</div>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="container" class="container_vis3">
		    	<div class="container_vis">
		      <h2><img src="/images/2012/title/container_vis_h2_3.gif" alt="저작권자찾기" title="저작권자찾기" /></h2>
		    	</div>
		    	<!-- content st -->
		    	<div class="content">
		    		
		    	      <!-- 래프 -->
					<jsp:include page="/include/2012/leftMenu03.jsp" />
					<script type="text/javascript">
						subSlideMenu("sub_lnb","lnb1","lnb11"); 
	 				</script>
					<!-- //래프 -->
					
			      <div id="ajaxBox" style="position:absolute; z-index:1; background: url(/images/2012/common/lodingBg.png) no-repeat 0 0; left:-500px; width: 306px; height: 38px; padding: 102px 0 0 0; ">
			        <p style="height: 38px; text-align: center; margin: 0;"> <img src="/images/2012/common/loading.gif" alt="" style="margin-top:-4px;margin-bottom: 3px;" /><br />
			          <span id="ajaxBoxMent" style="font-size: 12px; padding-top: 5px; color: #b44f00; font-weight: bold;">잠시만 기다려주세요..</span> </p>
			      </div>
			      
			      <!-- 주요컨텐츠 str -->
			      <div class="contentBody" id="contentBody">
			      	<p class="path"><span>Home</span><span>저작권자찾기소개</span> <span>저작권자 검색 및 상당한 노력 신청</span> <em>소개 및 이용방법</em> </p>
			      	<h1><img src="/images/2012/title/content_h1_0301.gif" alt="소개 및 이용방법" title="소개 및 이용방법" /></h1>
			      	
			           <!-- Tab str -->
                             <ul class="tab_menuBg">
	                             <li class="first"><a href="/mlsInfo/liceSrchInfo01.jsp">소개</a></li>
	                             <li class="on"><strong><a href="/mlsInfo/liceSrchInfo02.jsp">이용방법</a></strong></li>
                             </ul>
                             <!-- //Tab -->
			      	
			      		<div class="section">
			      			
			      			<h2 class="mt20">상당한노력신청(개인) 안내</h2>
			      			
			      			<ul class="tab_style2">
			      				<li class="bgNone"><a href="/mlsInfo/liceSrchInfo02.jsp">미분배 보상금 대상 저작물</a></li>
			      				<li><a href="/mlsInfo/liceSrchInfo05.jsp">저작권자 검색 및 상당한 노력 신청</a></li>
			      				<li class="active"><a href="/mlsInfo/liceSrchInfo04.jsp"">개인직접 수행</a></li>
			      			</ul>
			      			
			      			<ul class="tab_style floatDiv">
			      			<li class="active"><a href="/mlsInfo/liceSrchInfo04.jsp">저작권등록부 열람안내</a></li>
			      			<li><a href="/mlsInfo/liceSrchInfo03.jsp">신탁단체확인요청안내</a></li>
			      			<li><a href="/mlsInfo/liceSrchInfo06.jsp">저작권자 조회공고 안내</a></li>
			      			</ul>
			      			
			      			<div class="white_box">
	                                          <div class="box5">
		                                          <div class="box5_con floatDiv">
			                                          <p class="fl mt5"><img alt="" src="/images/2012/content/box_img4.gif"></p>
			                                          <div class="fl ml30 ">
				                                          <ul class="list1 mt10">
				                                          <li>찾고자 하는 저작물을 본 사이트인 저작권찾기 정보시스템이 아닌, 저작권등록사이트(www.cros.or.kr)에서 <br/>열람하고 열람한 내역을 법정허락 상당한노력 게시판에 본인이 직접 게시를 합니다.   </li>
				                                          <li>저작권법 제18조 제1항 : 법 제55조 제3항에 따른 저작권등록부의 열람 또는 그 사본의 교부신청을 통하여 해당 저작물의 저작재산권자나 그의 거소를 조회할것</li>
				                                          </ul>
			                                          </div>
		                                          </div>
	                                          </div>
                                         </div>
                                         <div class="floatDiv mt20">
                                         		<div class="fl w48">
                                         			<h3>저작권등록 사이트로 이동하기</h3>
                                         			<p class="mt15"><img src="/images/2012/content/site_cros1.gif" alt="" /><a href="http://www.cros.or.kr" class="block ce mt5" target="_blank"><img alt="사이트 가기" src="/images/2012/button/go_site.gif" /></a></p>
                                         		</div>
                                         		<div class="fl w48 ml25">
                                         			<h3>등록부 열람/사본교부 페이지 이동</h3>
                                         			<p class="mt15"><img src="/images/2012/content/site_cros2.gif" alt="" /><a href="http://www.cros.or.kr" class="block ce mt5" target="_blank"><img alt="사이트 가기" src="/images/2012/button/go_site.gif" /></a></p>
                                         		</div>
                                        </div>
					</div>
					<!-- //section -->
					
				</div>
				<!-- //주요컨텐츠 end -->
		    	</div>
	    		<!-- //content -->
	    		
	    		<!-- FOOTER str-->
				<jsp:include page="/include/2012/footer.jsp" />
				<!-- FOOTER end -->
		
	  	</div>
	  	<!-- //CONTAINER --> 
	    	
	    		    	
	</div>
	<!-- //전체를 감싸는 DIVISION -->
	
</body>
</html>