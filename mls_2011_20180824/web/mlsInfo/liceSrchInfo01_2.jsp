<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리자 검색 및 상당한 노력신청 소개 및 이용방법 - 소개 | 권리자찾기 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->



</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69621660-2', 'auto');
	  ga('send', 'pageview');

</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
<!-- 		<script type="text/javascript">initNavigation(3);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<script type="text/javascript">
		$(function(){
			modalSet();
			 
			$("#sub03_01_see_popup").css('display','none');
			$("#modalBg").css('display','none');
		})
		
		$(window).resize(function(){
				modalSet();
		})
		
		
		function modalSet(){
			var windowWidth = $( window ).width();
			var windowHeight = $( document ).height();
			var seePopupWidth = $(".sub03_01_pop_posi").css('width').substr(0,$(".sub03_01_pop_posi").css('width').indexOf('px'));
		
			$('#modalBg').css('background-color','black');
			$('#modalBg').css('position','absolute');
			$('#modalBg').css('top',0);
			$('#modalBg').css('left',0);
			$('#modalBg').css('width',windowWidth);
			$('#modalBg').css('height',windowHeight);
			$("#modalBg").css({'opacity':0.7, 'filter':'alpha(opacity=0.7)','z-index':'999999'});
			
			$("#sub03_01_see_popup").css('top','100px');
			$("#sub03_01_see_popup").css('left',windowWidth/2-seePopupWidth/2);
		}
		
		function detail_copy(id) {
			if(sub03_01_copyright_popup.style.display == 'none') {
				sub03_01_copyright_popup.style.display='block';
				$("#modalBg").css('display','block');
			}else {
				sub03_01_copyright_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
		}
		var interval = null;
		function detail_see(id) {
		 	if(sub03_01_see_popup.style.display == 'none') {
		 		sub03_01_see_popup.style.display='block';
		 		$("#sub03_01_see_popup").css('zIndex','9999999')
		 		$("#modalBg").css('display','block');
		 		interval = setInterval(modalTopSet,30);
			}else {
		 		sub03_01_see_popup.style.display = 'none';
		 		$("#modalBg").css('display','none');
		 	}
			
		}
		
		function closePop(id){
			clearInterval(interval);
			interval = null;
			if(sub03_01_see_popup.style.display == 'block') {
				sub03_01_see_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
			
			if(sub03_01_copyright_popup.style.display == 'block') {
				sub03_01_copyright_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
		}
		
		 //setInterval(function() { ... }, 지연시간);
		function modalTopSet(){
			$("#sub03_01_see_popup").css('top',($(document).scrollTop()+50)+'px');
		}
		
		</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
				<div class="con_lf_big_title">법정허락<br>승인 신청</div>
				<ul class="sub_lf_menu">
				<li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li>
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li>
				<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li>
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
				<li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li>
				</ul>
		</div>
		<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				법정허락 승인 신청 
				&gt;
				<span class="bold">상당한 노력 신청 방법</span>
			</div>
			<div class="con_rt_hd_title">상당한 노력 신청 방법</div>
			<div id="sub_contents_con">
				<!--  --><!--  --><!--  -->
				<!-- <h1 class="sub_con_h1 float_lf">저작권자 검색 서비스는?</h1>
				<div class="float_rt"><a href="/mlsInfo/liceSrchInfo02.jsp" class="method_bg_25869b">자세한 이용방법 바로가기</a></div>
				<p class="clear"></p>
				<div class="mar_tp20">
					<div class="float_lf">
						<h3 class="color_ff6000">&nbsp;※ 저작권 정보란? </h3>
						<h3 class="color_ff6000 pad_lf20 mar_tp5">저작물 정보(명칭 등) 및 권리관리정보(작사자·작곡가 등의</h3>
						<h3 class="color_ff6000 pad_lf20 mar_tp5">저작권자, 가수·연주자 및 음반제작자 등의 저작인접권자, 신탁관리단체 등)를 말합니다.</h3>
					</div>
					<div class="float_rt"><a href="#1" onclick="javascript:detail_copy('sub03_01_copyright_popup');" class="committee w186 sub03_01_copyright_popup_open">저작권 정보 자세히 보기</a></div>
					<p class="clear"></p>
				</div> -->
				<h1 id="service" class="sub_con_h1 mar_tp30">상당한 노력이란?</h1><br/>
				<p class="mar_tp5">저작물을 이용하고자 하였으나 권리자 또는 권리자의 소재를 알 수 없을 경우 법률에 의하여 이용 승인해 주는 법정허락제도의 사전절차입니다.</h3>
				<div class="mar_tp20">
					<div class="float_lf">
						<p class="mar_tp5">상당한 노력은 1. 직접 수행하는 방법과 2.위원회에 신청하는 방법이 있으며, </h3>
						<p class="mar_tp5">① 저작권등록부 조회 ② 위탁관리업자의 권리 정보 목록 조회 </h3>
						<p class="mar_tp5">③ 권리자 찾기 사이트 공고 등을 수행해야 합니다</h3>
					</div>
					<div class="float_rt"><a href="#1" onclick="javascript:detail_see('sub03_01_see_popup');" class="committee w186 spacing_f1 sub03_01_see_popup_open">상당한 노력 방법 자세히 보기</a></div>
					<p class="clear"></p>
				</div>
				<br/>
				<div class="sub_con_h2 mar_tp20">상당한 노력_직접 수행</div>
					<p class="mar_tp10 pad_lf20">① 저작권등록부 확인</p>
					<p class="mar_tp5 pad_lf20">② 신탁관리업자에게 회신받거나 1개월 이후 회신 없음을 문서로 증빙서류 준비</p>
					<p class="mar_tp5 pad_lf20">&nbsp;&nbsp;&nbsp;(신탁관리단체 없을 경우 대리중개업체 또는 이용자 2명에게 동일 내용 확인)</p>
					<p class="mar_tp5 pad_lf20">③ 일반일간지 또는 문화부 및 위원회 권리자 찾기 사이트 공고(10일간)</p>
					<p class="mar_tp5 pad_lf20">④ 국내의 정보통신망 정보검색도구를 이용하여 저작재산권자나 거소 검색</p>
				<div class="sub_con_h2 mar_tp30">상당한 노력 신청_위원회 대행</div>
					<p class="mar_tp10 pad_lf20">① 저작권등록부 조회</p>
					<p class="mar_tp5 pad_lf20">② 위탁관리업자의 권리 정보 목록 조회</p>
					<p class="mar_tp5 pad_lf20">③ 권리자 찾기 사이트 2개월 이상 공고</p>
				<div class="sub_con_h2 mar_tp30">통합검색 순서도</div>
				<!-- <div class="mar_tp30 pad_lf20"><img src="/images/sub_img/sub_41.gif" alt="통합검색 순서도" /></div> -->
				<!-- <div class="mar_tp30 pad_lf20"><img src="/images/sub/sub_tu2.jpg" alt="저작권물 이용자 통합검색 저작물 존재여부 위탁관리저작물 저작권등록 저작물 이용안내 체결안내.저작권물 이용자 통합검색 저작물 존재여부 자작권자 찾기 위한 상당한 노력 신청 기승인된 법정허락저작물법정허락신청 대상 저작물 법정허락 승인 신청." /></div> -->
				<div style="position:relative;left:-80px;"><img src="/images/2017/flowChart/flow_chart3.jpg" alt="저작권물 이용자 통합검색 저작물 존재여부 위탁관리저작물 저작권등록 저작물 이용안내 체결안내.저작권물 이용자 통합검색 저작물 존재여부 자작권자 찾기 위한 상당한 노력 신청 기승인된 법정허락저작물법정허락신청 대상 저작물 법정허락 승인 신청." /></div>
				<div class="sub_con_h2 mar_tp30">미분배 보상금 대상 저작물</div>
					<p class="mar_tp10 pad_lf20">미분배 보상금 저작물을 합법적으로 이용할 수 있도록 법정허락의 사전절차인 저작권자를 찾기 위한 상당한 노력을 </p>
					<p class="mar_tp5 pad_lf20">정부가 개인을 대신하여 수행하는 서비스</p>
				<!-- <div class="mar_tp30"><img src="/images/sub_img/sub_42.gif" alt="미분배 보상금 대상 저작물" /></div> -->
				<!-- <div class="mar_tp30 pad_lf20"><img src="/images/sub/sub_tu3.jpg" alt="미분배 보상금 대상 저작물" style="margin-left:60px;"/></div> -->
				<div class="mar_tp30" style="position: relative; left : 0px;"><img src="/images/2017/flowChart/flow_chart4.jpg" alt="미분배 보상금 대상 저작물"/></div>
				<!-- <div class="mar_tp40"><img src="/images/sub_img/sub_43.gif" alt="미분배 보상금 대상 저작물" /></div> -->
				<!-- <div class="mar_tp40"><img src="/images/sub/sub_tu4.jpg" alt="미분배 보상금 대상 저작물" /></div> -->
				<div class="mar_tp40" ><img src="/images/2017/flowChart/flow_chart5.jpg" alt="미분배 보상금 대상 저작물" style="max-width: 700px;" /></div>
				<h1 class="sub_con_h1 mar_tp30">저작권자 조회공고</h1>
				<p class="mar_tp10">저작권자를 찾기 위한 상당한 노력 중 하나로 저작권자 조회사항을 공고 및 조회할 수 있습니다.</p>
				<h2 class="sub_con_h2 mar_tp30">저작권자 조회 공고_개인</h2>
				<div class="pad_lf20">
					<div class="mar_tp10">법정허락의 사전절차인 저작권자를 찾기 위한 상당한 노력 중<span class="color_41629d line_ht20">&nbsp;개인이 직접 수행하는 권리자 찾기 사이트 공고</span>를</div>
					<p class="mar_tp5">할 수 있도록 준비한 서비스 입니다.</p>
					<p class="mar_tp5">공고 신청 후 담당자 승인이 되면 이후 10일 간 공고가 진행됩니다.</p>
				</div>
				<h2 class="sub_con_h2 mar_tp30">저작권자 조회 공고_위원회</h2>
				<div class="pad_lf20">
					<div class="mar_tp10">법정허락의 사전절차인 저작권자를 찾기 위한 상당한 노력 중<span class="color_41629d line_ht20">&nbsp;위원회에서 대신 진행하는 권리자 찾기 사이트 공고</span>를</div>
					<p class="mar_tp5">2개월간 진행합니다.</p>
				</div>
				<!--  --><!--  --><!--  -->
			</div>

			<!-- sub_contents_con -->
			<!-- head_and_lf -->

		</div>
		<p class="clear"></p>
	</div>
	<div id="sub03_01_copyright_popup" class="sub03_01_popup disnone" style="position:absolute; background-color: white;" z-index="999999">
		<div class="sub03_01_pop_posi">
			<div class="sub03_01_pop_head">저작권 정보 자세히 보기
				<a href="#1" onclick="closePop('sub03_01_copyright_popup');" class="popup_close"><img src="/images/sub_img/sub_pop_close.gif" alt="그림" /></a>
			</div>
			<div class="sub03_01_pop_con">
				<table class="sub_tab td_pad_lr20" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
					<caption>한국저작권위원회</caption>
					<colgroup>
						<col width="16%"/>
						<col width="14%"/>
						<col width="*"/>
					</colgroup>
					<tbody>
						<tr>
							<th scope="col" colspan="2">저작자</th>
							<td>저작물을 창작한 자를 저작자라고 합니다. 그러나 실제로 창작한 사람이 누구인지를 안다는 것이 어렵기 때문에 그 저작자의 원작품이나 그 복제물에 성명이나 예명이 표시된 자가 저작자로 추정되며, 이러한 표시가 없는 경우에는 그 작품의 발행자나 공연자가 그 저작물의 저작자로 추정됩니다.(저작권법 제8조). <span class="color_2c65aa">저작자는 자기의 저작물에 대한 저작권, 즉 저작인격권과 저작재산권을 가집니다.</span></td>
						</tr>
						<tr>
							<th scope="col" colspan="2">저작권자</th>
							<td>
								<p class="word_dian_bg">저작권법에 따라 저작권을 인정받아 이를 행사할 수 있는 사람을 말하며, 저작자와 저작권자가 동일인일수도 있고, 다른 사람일수도 있습니다.</p>
								<p class="word_dian_bg">저작자가 저작권을 양도한다고 해도 저작인격권은 여전히 저작자에게 남아 있게 되므로, 통상 저작권자라고 할 때에는 저작재산권자를 의미합니다. </p>
								<p class="word_dian_bg">저작권를 넘겨주는 경우에는 저작권자가 양수자(남의 물건을 넘겨받는자), 저작권자가 상속인(상속 개시 후에 재산이나 기타의 것을 물려받는 사람)이 됩니다.</p>
							</td>
						</tr>
						<tr>
							<th scope="col" rowspan="4">저작인접권자</th>
							<th scope="col">&nbsp;</th>
							<td>저작인접권이란 저작물의 복제·전파기술의 발달로 전통적인 저작권의 보호 외에 저작물의 실연, 녹음 및 방송을 통하여 저작물의 배포, 전파에 기여한 사람들의 권리를 보호해 주기 위해 인정된 권리 개념입니다. <br />저작인접권자로 보호를 받는 자는 실연자, 음반제작자, 방송사업자입니다.</td>
						</tr>
						<tr>
							<th scope="col">실연자</th>
							<td>
								<p class="word_dian_bg">실연은 저작물을 연기, 무용, 연주, 연술, 그 밖의 예능적 방법으로 표현하는 것을 말하며, 저작물이 아닌 곡예나 요술 등을 이와 유사한 방법으로 표현하는 것을 포함합니다. </p>
								<p class="word_dian_bg">실연자에는 실연을 직접 하는 자뿐만 아니라 실연을 지휘, 연출 또는 감독하는 자도 포함합니다.</p>
								<p class="word_dian_bg">실연자는 자신의 실연에 대하여 성명표시권, 동일성유지권을 가지며, 재산권으로는 복제권, 배포권(실연의 복제물이 실연자의 허락을 받아 판매 등의 방법으로 거래에 제공되는 경우 제외), 대여권(그의 실연이 판매용 음반을 영리를 목적으로 대여 하는 경우), 공연권(그 실연이 방송되는 실연인 경우 제외), 방송권(실연자의 허락을 받아 녹음된 경우 제외), 전송권, 판매용 음반에 대한 방송보상청구권, 판매용 음반에 대한 디지털음성송신보상청구권, 판매용 음반에 대한 공연보상청구권을 가집니다.</p>
							</td>
						</tr>
						<tr>
							<th scope="col">음반제작자</th>
							<td>음반제작자는 음반의 복제권, 배포권, 대여권(판매용 음반을 영리를 목적으로 대여하는 경우), 전송권, 판매용 음반에 대한 방송보상청구권, 판매용 음반에 대한 디지털음성송신보상청구권, 판매용 음반에 대한 공연보상청구권을 가집니다.</td>
						</tr>
						<tr>
							<th scope="col">방송사업자</th>
							<td>방송사업자는 그의 방송을 녹음, 녹화, 사진, 그 밖의 이와 유사한 방법으로 복제하거나 동시중계방송할 권리를 가집니다. 방송사업자의 복제권은 방송의 녹음·녹화뿐만 아니라 그 녹음·녹화물을 또다시 복제하는 것에도 미칩니다.다시 말하면, TV 연속극을 녹화하여 판매한 비디오테이프를 다른 사람이 임의로 복제하여 이용할 수 없다는 것입니다.</td>
						</tr>
					</tbody>
				</table>
				<div class="align_cen mar_tp40"><a href="#1" onclick="closePop('sub03_01_copyright_popup');" class="pop_check popup_close">확&nbsp;인</a></div>
			</div>
		</div>
	</div>
	<div id="sub03_01_see_popup" class="sub03_01_popup disnone" style="position:absolute;background-color: white;" z-index="9999999">
					<div class="sub03_01_pop_posi">
						<div class="sub03_01_pop_head">상당한 노력 방법 자세히 보기
							<a href="#1" onclick="closePop();" class="popup_close"><img src="/images/sub_img/sub_pop_close.gif" alt="그림" /></a>
						</div>
						<div class="sub03_01_pop_con">
							<table class="sub_tab td_cen tab_lfbor" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
								<caption>한국저작권위원회</caption>
								<colgroup>
									<col width="24%"/>
									<col width="14%"/>
									<col width="15%"/>
									<col width="12%"/>
									<col width="18%"/>
									<col width="*"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="row">보상금관리 신탁단체</th>
										<th scope="row" colspan="2">담당업무</th>
										<th scope="row">담당자</th>
										<th scope="row">전화번호</th>
										<th scope="row">Email</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>한국음악실연자연합회</td>
										<td colspan="2">보상금/권리자 찾기 <br />신청관리</td>
										<td>권기태 팀장</td>
										<td>02-2659-7048</td>
										<td>kjery@fkmp.kr</td>
									</tr>
									<tr>
										<td>한국음반산업협회</td>
										<td colspan="2">보상금/권리자 찾기 <br />신청관리</td>
										<td>김제경</td>
										<td>02-3270-5933</td>
										<td>kjk@riak.or.kr</td>
									</tr>
									<tr>
										<td rowspan="3">한국복제전송저작권협회</td>
										<td rowspan="2">보상금 <br />신청관리</td>
										<td>교과용<br />보상금</td>
										<td>김나영</td>
										<td>070-4265-2525</td>
										<td>nayoung@korra.kr</td>
									</tr>
									<tr>
										<td>도서관<br />보상금</td>
										<td>김광성 과장</td>
										<td>070-4265-2522</td>
										<td>kskim@korra.kr</td>
									</tr>
									<tr>
										<td colspan="2">권리자 찾기 신청관리</td>
										<td>심명관</td>
										<td>070-4265-2536</td>
										<td>smk@korra.kr</td>
									</tr>
								</tbody>
							</table>
							<table class="sub_tab td_cen tab_lfbor mar_tp30" cellspacing="0" cellpadding="0" width="100%" summary="한국저작권위원회">
								<caption>한국저작권위원회</caption>
								<colgroup>
									<col width="24%"/>
									<col width="29%"/>
									<col width="12%"/>
									<col width="18%"/>
									<col width="*"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="row">신탁단체</th>
										<th scope="row">담당업무</th>
										<th scope="row">담당자</th>
										<th scope="row">전화번호</th>
										<th scope="row">Email</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>한국음악저작권협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>전종훈</td>
										<td>02-2660-0585</td>
										<td>bigshow0411<br />@komca.or.kr</td>
									</tr>
									<tr>
										<td>한국문예학술저작권협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>박남규</td>
										<td>02-508-0440</td>
										<td>png@ekosa.org</td>
									</tr>
									<tr>
										<td>한국영화배급협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>김의수 부장</td>
										<td>02-3452-1001</td>
										<td>kjk@riak.or.kr</td>
									</tr>
									<tr>
										<td>한국영화제작자협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>정재훈</td>
										<td>02-2267-9983</td>
										<td>nayoung@korra.kr</td>
									</tr>
									<tr>
										<td>한국방송작가협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>김지숙 부장</td>
										<td>02-782-1696</td>
										<td>kskim@korra.kr</td>
									</tr>
									<tr>
										<td>한국시나리오작가협회</td>
										<td>권리자 찾기 신청관리</td>
										<td>정지영 부장</td>
										<td>02-2275-0566</td>
										<td>smk@korra.kr</td>
									</tr>
								</tbody>
							</table>
							
							<div class="align_cen mar_tp40"><a href="#1" onclick="closePop();" class="pop_check popup_close">확&nbsp;인</a></div>
						</div>
					</div>
				</div>
				
				<div id="modalBg">
					
				</div>
				
				<!-- FOOTER str-->
				<!-- 2017변경 -->
				<jsp:include page="/include/2017/footer.jsp" />
				<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
				<!-- FOOTER end -->
	  	<!-- //CONTAINER --> 
	    		    	
</body>
</html>