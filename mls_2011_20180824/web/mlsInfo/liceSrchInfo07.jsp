<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<%@ page import="kr.or.copyright.mls.support.util.SessionUtil" %>
<%@ page import="kr.or.copyright.common.userLogin.model.User" %>
<%
	User user = SessionUtil.getSession(request);
	String sessUserIdnt = user.getUserIdnt();
	pageContext.setAttribute("UserName", sessUserIdnt); 
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>권리자 검색 및 상당한 노력신청 소개 및 이용방법 - 소개 | 권리자찾기 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link rel="stylesheet" type="text/css" href="/css/sub.css" />
<link rel="stylesheet" type="text/css" href="/css/table.css" />
<link rel="stylesheet" type="text/css" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<script src="/js/jquery-1.7.js"  type="text/javascript"></script>
<script src="/js/jquery-1.7.1.js"  type="text/javascript"></script>
<script type="text/JavaScript" language="javascript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js" ></script>
<script type="text/javascript" src="/js/2012/prototype.js"> </script> 
<script type="text/javascript" language="javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<script type="text/javascript"> 
<!--
//-->



</script>
<script>

</script>
</head>

<body>
		
		<!-- HEADER str-->
		<!-- 2017변경 -->
		<jsp:include page="/include/2017/header.jsp" />
		<%-- <jsp:include page="/include/2012/subHeader3.jsp" /> --%>
<!-- 		<script type="text/javascript">initNavigation(3);</script> -->
		<!-- GNB setOn 각페이지에 넣어야합니다. -->
		<script type="text/javascript">
		$(function(){
			modalSet();
			 
			$("#sub03_01_see_popup").css('display','none');
			$("#modalBg").css('display','none');
		})
		
		$(window).resize(function(){
				modalSet();
		})
		
		
		function modalSet(){
			var windowWidth = $( window ).width();
			var windowHeight = $( document ).height();
			var seePopupWidth = $(".sub03_01_pop_posi").css('width').substr(0,$(".sub03_01_pop_posi").css('width').indexOf('px'));
		
			$('#modalBg').css('background-color','black');
			$('#modalBg').css('position','absolute');
			$('#modalBg').css('top',0);
			$('#modalBg').css('left',0);
			$('#modalBg').css('width',windowWidth);
			$('#modalBg').css('height',windowHeight);
			$("#modalBg").css({'opacity':0.7, 'filter':'alpha(opacity=0.7)','z-index':'999999'});
			
			$("#sub03_01_see_popup").css('top','100px');
			$("#sub03_01_see_popup").css('left',windowWidth/2-seePopupWidth/2);
		}
		
		function detail_copy(id) {
			if(sub03_01_copyright_popup.style.display == 'none') {
				sub03_01_copyright_popup.style.display='block';
				$("#modalBg").css('display','block');
			}else {
				sub03_01_copyright_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
		}
		var interval = null;
		function detail_see(id) {
		 	if(sub03_01_see_popup.style.display == 'none') {
		 		sub03_01_see_popup.style.display='block';
		 		$("#sub03_01_see_popup").css('zIndex','9999999')
		 		$("#modalBg").css('display','block');
		 		interval = setInterval(modalTopSet,30);
			}else {
		 		sub03_01_see_popup.style.display = 'none';
		 		$("#modalBg").css('display','none');
		 	}
			
		}
		
		function closePop(id){
			clearInterval(interval);
			interval = null;
			if(sub03_01_see_popup.style.display == 'block') {
				sub03_01_see_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
			
			if(sub03_01_copyright_popup.style.display == 'block') {
				sub03_01_copyright_popup.style.display = 'none';
				$("#modalBg").css('display','none');
			}
		}
		
		 //setInterval(function() { ... }, 지연시간);
		function modalTopSet(){
			$("#sub03_01_see_popup").css('top',($(document).scrollTop()+50)+'px');
		}
		
		</script>
		<!-- HEADER end -->
		
		<!-- CONTAINER str-->
	  	<div id="contents">
		<div class="con_lf">
				<div class="con_lf_big_title">법정허락<br>승인 신청</div>
				<ul class="sub_lf_menu">
				<!-- <li><a href="/srchList.do?menuFlag=N">상당한 노력 신청 서비스</a></li> -->
				<!-- <li><a href="/mlsInfo/liceSrchInfo01_1.jsp">상당한 노력 신청 방법</a></li> -->
				<!-- <li><a class="on" href="/mlsInfo/liceSrchInfo01_2.jsp">상당한 노력 신청 방법</a></li> -->
				<li><a class="on" href="/mlsInfo/liceSrchInfo07.jsp">법정허락 제도 안내</a></li>
				<li><a href="/statBord/statBo01List.do?bordCd=1&amp;divsCd=5">저작권자 조회 공고</a></li>
				<li><a href="/stat/statSrch.do">법정허락 승인 신청 서비스</a></li>
				<!-- <li><a href="/mlsInfo/statInfo01.jsp">법정허락 승인 신청 방법</a></li> -->
				<li><a href="/statBord/statBo03List.do?bordCd=3">법정허락 승인 공고</a></li>
				<!-- <li><a href="/statBord/statBo07List.do">법정허락 기승인 공고</a></li> -->
				</ul>
		</div>
				<div class="con_rt">
			<div class="con_rt_head">
				<img src="/images/sub_img/sub_home.png" alt="홈 페이지" />
				&gt;
				법정허락 승인 신청 
				&gt;
				<span class="bold">법정허락 제도 안내</span>
			</div>
			<div class="con_rt_hd_title">법정허락 제도 안내</div>
			<div id="sub_contents_con">
				<!-- 2018.12.03 컨텐츠 추가 -->
				<h1 class="sub_con_h1">법정허락 개요</h1>
				<p class="mar_tp20">저작재산권자 불명인 저작물(실연·음반·방송 및 데이터베이스)의 이용을 위해서는</p>
				<p class="mar_tp10 txt_num"><span>①</span> 대통령령에 따라 ‘상당한 노력’을 기울였어도 권리자를 알 수 없거나 권리자의 거소를 알 수 없는 경우</p>
				<p class="addition mar_tp5"><span>※</span> 법정허락 : 저작권법 제 50조(저작재산권자 불명인 저작물의 이용)</p>
				<p class="addition mar_tp5"><span>※</span> 상당한 노력 : 저작권법 시행령 제 18조(상당한 노력의 기준)</p>
				<p class="fs12 mar_tp5 letter-sp1"><a="#"><strong class="txt_yellow_n fs13">* 상당한 노력의 증빙자료 샘플 및 상세안내</strong></a><!--<span class="txt_yellow_n fs12">(클릭)</span>(제공한 파일에 나와있습니다.) 종합안내서(보상금 산정기준 및 산정내역서 작성 방법, 신청에 필요한 그 밖의 자료)--></p>
				<div class="sub01_con_bg4_tp mar_tp30"></div>
				<div class="sub01_con_bg4 letter-sp1 line-ht27">
					<h3 class="txt_yellow_n">상당한 노력 절차</h3>
					<p>1) 위원회 저작권등록시스템(www.cros.or.kr) 조회</p>
					<p>2) 해당 저작물이 속하는 분야의 저작권신탁관리업자 조회</p>
					<p>3) 권리자찾기정보시스템(www.findcopyright.or.kr) 또는 전국을 보급지역으로 하는 일반일간신문에 공고한 날로부터 10일 경과</p>
					<p>4) 국내 정보통신망 정보검색도구를 이용한 인터넷 검색</p>
				</div>
				<p class="mar_tp20 txt_num "><span>②</span>  특별한 목적으로 저작물을 이용하고자 하였으나 권리자와 협의가 성립되지 않아 이용할 수 없는 경우, 문화체육관광부장관에 저작물의 이용에 대한 승인을 신청하는 제도입니다.</p>
				<p class="addition pad_lf20 mar_tp20"><span>※</span> 저작권법 제 51조(공표된 저작물의 방송), 제 52조(상업용 음반의 제작), 제 89조((실연ㆍ음반 및 방송이용의 법정허락), 제 97조(데이터베이스이용의 법정허락)</p>
				<h1 class="sub_con_h1 mar_tp40">법정허락 개요</h1>
				<h2 class="sub_con_h2 mar_tp30 txt_black">과거 법정허락을 받은 적이 없는 저작물 등에 대해 법정허락 신청을 하는 경우</h2>
				<div class="img_step">
					<img src="images/new/step_intro_1.png" alt="">
				</div>
				<h2 class="sub_con_h2 mar_tp30 txt_black">과거 법정허락을 받은 적이 있는 저작물 등에 대해 법정허락 신청을 하는 경우</h2>
				<div class="img_step">
					<img src="images/new/step_intro_2.png" alt="">
				</div>
				<h2 class="sub_con_h2 mar_tp30 txt_black">법정허락 신청 수수료 : <span>1건당 1만원</span> <span class="txt_red fs13">(보상금 공탁은 별도)</span></h2>
				<h2 class="sub_con_h2 mar_tp30 txt_black">법정허락 문의 :  <span> 내방 및 우편</span></h2>
				<p class="pad_lf20 mar_tp10"><strong class="txt_yellow_n">주소 :  </strong>경남 진주시 충의로(LH공사) 5층 심의조사팀</p>
				<p class="pad_lf20 mar_tp5"><strong class="txt_yellow_n">문의전화 :</strong> 055-792-0083, <strong class="txt_yellow_n">팩스 :</strong> 055-792-0079</p>
				<!-- 2018.12.03 컨텐츠 추가 끝-->
						</div>
					</div>
		
		
			
			<!-- 	<div id="modalBg">
					
				</div>
				 -->
				<!-- FOOTER str-->
				<!-- 2017변경 -->
				<jsp:include page="/include/2017/footer.jsp" />
				<%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
				<!-- FOOTER end -->
	  	<!-- //CONTAINER --> 
	    		</div>
					    	
</body>
</html>