<?xml version="1.0" encoding="EUC-KR" ?>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/paging.tld" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>소개 및 이용방법 | 미분배 보상금 대상 저작물 찾기 | 권리자찾기</title>
<link type="text/css" rel="stylesheet" href="/css/2012/common_sub.css">
<link type="text/css" rel="stylesheet" href="/css/sub.css">
<link type="text/css" rel="stylesheet" href="/css/table.css">
<link type="text/css" rel="stylesheet" href="/css/2012/calendar.css">
<link type="text/css" rel="stylesheet" href="/css/2017/new.css">
<!-- <script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="/js/jquery-1.7.js"  type="text/javascript"></script> -->
<script type="text/JavaScript" src="/js/jax.js"></script>
<script type="text/javascript" src="/js/2012/deScript.js"></script>
<script type="text/javascript" src="/js/2010/general.js"></script>
<script type="text/javascript" src="/js/Function.js"></script>
<script type="text/javascript" src="/js/makePCookie.js"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/flexslider.js"></script>
<script type="text/javascript" src="/js/js.js"></script>
<!-- <script type="text/javascript" src="/js/2010/calendarcode.js"></script>  -->
<script type="text/javascript"> 
<!--
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69621660-2', 'auto');
ga('send', 'pageview');
//-->
    /* $(document).ready(function() {
      
      $.ajax({
        type:"POST",  
            url:"/inmtPrps/inmtPrps.do",      
            data:"method=inmtPrpsTopList&limit=10",
            dataType : 'html',
        success : function(html) {
          $('#inmtPrpsTopList').html(html);
        },  
            error:function(e){  
                alert(e.responseText);  
            }  
        });
      
    }); */
</script>
</head>

<body>

    <!-- HEADER str-->
    <!-- 2017변경 -->
    <jsp:include page="/include/2017/header.jsp" />
    <script type="text/javascript">
    $(function(){
      modalSet();
       
      $("#sub03_01_see_popup").css('display','none');
      $("#modalBg").css('display','none');
    })
    
    $(window).resize(function(){
        modalSet();
    })
    
    
    function modalSet(){
      var windowWidth = $( window ).width();
      var windowHeight = $( document ).height();
      var seePopupWidth = $(".sub03_01_pop_posi").css('width').substr(0,$(".sub03_01_pop_posi").css('width').indexOf('px'));
    
      $('#modalBg').css('background-color','black');
      $('#modalBg').css('position','absolute');
      $('#modalBg').css('top',0);
      $('#modalBg').css('left',0);
      $('#modalBg').css('width',windowWidth);
      $('#modalBg').css('height',windowHeight);
      $("#modalBg").css({'opacity':0.7, 'filter':'alpha(opacity=0.7)','z-index':'999999'});
      
      $("#sub03_01_see_popup").css('top','100px');
      $("#sub03_01_see_popup").css('left',windowWidth/2-seePopupWidth/2);
    }
    
    function detail_copy(id) {
      if(sub03_01_copyright_popup.style.display == 'none') {
        sub03_01_copyright_popup.style.display='block';
        $("#modalBg").css('display','block');
      }else {
        sub03_01_copyright_popup.style.display = 'none';
        $("#modalBg").css('display','none');
      }
    }
    var interval = null;
    function detail_see(id) {
      if(sub03_01_see_popup.style.display == 'none') {
        sub03_01_see_popup.style.display='block';
        document.getElementById("sub03_01_see_popup_content").focus();
        $("#sub03_01_see_popup").css('zIndex','9999999')
        $("#modalBg").css('display','block');
        interval = setInterval(modalTopSet,30);
      }else {
        sub03_01_see_popup.style.display = 'none';
        $("#modalBg").css('display','none');
      }
      
    }
    
    function closePop(id){
      clearInterval(interval);
      interval = null;
      if(sub03_01_see_popup.style.display == 'block') {
        sub03_01_see_popup.style.display = 'none';
        $("#modalBg").css('display','none');
        document.getElementById("sub01_con_bg3_btn").focus();
      }
      
      if(sub03_01_copyright_popup.style.display == 'block') {
        sub03_01_copyright_popup.style.display = 'none';
        $("#modalBg").css('display','none');
      }
    }
    
     //setInterval(function() { ... }, 지연시간);
    function modalTopSet(){
      $("#sub03_01_see_popup").css('top',($(document).scrollTop()+50)+'px');
    }
    
    </script>
    <%-- <jsp:include page="/include/2012/subHeader2.jsp" /> --%>
<!--    <script type="text/javascript">initNavigation(1);</script> -->
    <!-- GNB setOn 각페이지에 넣어야합니다. -->

    <!-- HEADER end -->
    
    <!-- CONTAINER str-->
        <div id="contents">
    <div class="con_lf">
      <h2><div class="con_lf_big_title">미분배보상금<br>저작물 찾기</div></h2>
      <ul class="sub_lf_menu">
        <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1">미분배보상금 대상 저작물확인</a></li>
        <li><a href="/mlsInfo/inmtInfo02.jsp" class="on">소개 및 이용방법</a></li>
        <!-- <li><a href="/mlsInfo/rghtInfo01.jsp" alt="저작권 정보 확인">저작권 정보 확인</a>
          <ul class="sub_lf_menu2 disnone">
            <li><a href="/mlsInfo/rghtInfo01.jsp" alt="소개 및 이용방법">소개 및 이용방법</a></li>
            <li><a href="/rghtPrps/rghtSrch.do?DIVS=M" alt="서비스 이용">서비스 이용</a></li>
          </ul>
        </li> -->
        <!-- <li><a href="/mlsInfo/inmtInfo02.jsp" class="on" alt="미분배 보상금 대상 저작물 확인">미분배 보상금 대상 저작물 확인</a>
          <ul class="sub_lf_menu2">
            <li><a href="/mlsInfo/inmtInfo02.jsp" class="on" alt="소개 및 이용방법">소개 및 이용방법</a></li>
            <li><a href="/inmtPrps/inmtPrps.do?mNum=4&amp;sNum=0&amp;leftsub=0&amp;srchDIVS=1" alt="서비스 이용">서비스 이용</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
    <div class="con_rt" id="contentBody">
      <div class="con_rt_head">
        <img src="/images/sub_img/sub_home.png" alt="홈 페이지">
        &gt;
        미분배 보상금 대상 저작물 찾기
        &gt;
        <span class="bold">소개 및 이용방법</span>
      </div>
      <h1><div class="con_rt_hd_title">소개 및 이용방법</div></h1>
      <div id="sub_contents_con">
        <!--  --><!--  --><!--  -->
        <h2 class="sub_con_h1 float_lf">미분배 보상금 대상 저작물 확인 서비스란?</h2>
        <!-- <div class="float_rt"><a href="#"  onclick="javascript:detail_see('sub03_01_see_popup');" class="method_bg_25869b" >자세한 이용방법 바로가기</a></div> -->
        <div class="float_rt"><a href="#" id="sub01_con_bg3_btn" onclick="javascript:detail_see('sub03_01_see_popup');" class="committee w186 spacing_f1 sub03_01_see_popup_open">자세한 이용방법 바로가기</a></div>
        <p class="clear"></p>
        <p class="mar_tp10">분배되지 않은 보상금 대상 저작물 정보를 한곳에서 확인하고 해당 권리자일 경우 보상금을</p>
        <p class="mar_tp5">(선택하신 분야에 따라 검색 항목은 다를 수 있습니다.)</p>
        <h3 class="sub_con_h2 mar_tp30">이용순서</h3>
        <!-- <div class="mar_tp20"><img src="/images/sub_img/sub_39.gif" alt="그림"></div> -->
        <div class="mar_tp20"><img src="/images/2017/flowChart/flow_chart2.jpg" 
            alt="저작권 찾기 사이트 방문 했을 경우 미분배 보상금 저작물 검색을 하게되면 보상금 신청방법 및 절차에 대해 안내받으실 수 있습니다. 
            신탁관리단체로 방문했을 경우 신청관리단체로 신청 및 접수를 하게되면 신탁관리단체에서 저작물 관리관계를 확인을 하게 됩니다. 그 후 미분배보상금 분배내역 확인절차를 거쳐 신탁관리단체에서 미분1배보상금 신청결과를 처리하게 됩니다.
            저작물 관리관계 확인 단계에서 정부대행시 신탁관리단체에서 미분배보상금 신청결과 처리 과정을 거친 뒤 미분배보상금 분배내역 확인 과정으로 이동하게 됩니다."></div>
        

        <h3 class="sub_con_h2 mar_tp30">저작물 조회 방법</h3>
        <table class="sub_tab td_cen mar_tp15" cellspacing="0" cellpadding="0" width="100%" summary="저작물 조회 방법을 나타낸 표로 보상금 종류, 내용, 수령단체로 구성되어 있습니다.">
          <caption >저작물 조회 방법</caption>
          <colgroup>
            <col width="22%">
            <col width="*">
            <col width="22%">
          </colgroup>
          <thead>
            <tr>
              <th scope="col">보상금 종류</th>
              <th scope="col">내용</th>
              <th scope="col" class="last_bor0">수령단체</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>상업용 음반<br>방송·공연보상금</td>
              <td class="align_lf20">방송, 공연에 이용된 상업용 음반의 실연자 및 음반제작자에게 지급</td>
              <td rowspan="2" class="last_bor0">한국음악실연자연합회<br><br>한국음반산업협회</td>
            </tr>
            <tr>
              <td>디지털음성송신보상금</td>
              <td class="align_lf20">디지털음성송신에 이용되는 음반의 실연자 및 음반제작자에게 지급</td>
            </tr>
            <tr>
              <td>교과용도서보상금</td>
              <td class="align_lf20">교과용도서에 게재된 저작물의 저작권자에게 지급</td>
              <td rowspan="3" class="last_bor0">한국복제전송저작권협회</td>
            </tr>
            <tr>
              <td>도서관보상금</td>
              <td class="align_lf20">도서관에서 자료 출력 및 전송된 저작물의 저작권자에게 지급</td>
            </tr>
            <tr>
              <td>수업목적 보상금</td>
              <td class="align_lf20">특별법에 의해 설립되었거나 고등교육법에 따른 학교 등에서 수업 목적으로 게재된 저작물의 저작권자에게 지급</td>
            </tr>
          </tbody>
        </table>
        
        <div id="inmtPrpsTopList"></div>
        
        <!--  --><!--  --><!--  -->
      </div>

      <!-- sub_contents_con -->
      <!-- head_and_lf -->

    </div>
    <p class="clear"></p>
  </div>
  <div id="sub03_01_see_popup" class="sub03_01_popup disnone" style="position:absolute; z-index=9999999">
    <div class="sub03_01_pop_posi" style="position:absolute;background-color: white;width: 840px; z-index:9999999">
    <div class="sub03_01_pop_head">미분배 보상금 대상 저작물 확인 자세한 이용방법 
        <a href="#1" onclick="closePop();" class="popup_close"><img src="/images/sub_img/sub_pop_close.gif" alt="닫기"></a>
    </div>
      <div id="sub03_01_see_popup_content" style="padding : 20px; overflow-x:hidden; overflow-y:auto; height:600px; width: 800px" tabindex="0">
        <h1 class="sub_con_h2 float_lf">미분배 보상금 대상 저작물 조회 방법</h1>
        <!-- <div class="float_rt"><a href="/mlsInfo/inmtInfo02.jsp" class="method_bg_25869b">서비스 소개 바로가기</a></div> -->
        <p class="clear"></p>
        <p class="mar_tp5">1. 조회하려는 저작물의 분야를 선택하고 검색 키워드를 입력하신 후 조회 버튼을 클릭합니다.</p>
        <p class="mar_tp5">(선택하신 분야에 따라 검색 항목은 다를 수 있습니다.)</p>
        <ul class="sub_menu1 w200 mar_tp40">
          <li class="on" style="width: 195px;"><a class="on" style="width: 195px;" title="선택됨">방송음악</a></li>
          <li style="width: 195px;"><a style="width: 195px;">교과용</a></li>
          <li style="width: 195px;"><a style="width: 195px;">도서관</a></li>
          <li style="width: 195px;"><a class="last_rt_bor" style="width: 195px;">수업목적</a></li>
        </ul>
        <p class="clear"></p>
        <div class="mar_tp20"><img src="/images/sub_img/sub_35.jpg" alt="조회 화면 - 곡명, 가수, 앨범명 입력 서식과 조회 버튼"></div>
        <p class="mar_tp20">2. 검색 결과를 확인하고 저작물의 제목(제호)을 클릭하여 상세정보 팝업에서 저작물 및 저작권 정보를 확인합니다.</p>
        <div class="mar_tp20"><img src="/images/sub_img/sub_36.jpg" alt="조회 결과 화면 - 곡명, 가수, 앨범명 입력 아래영역에 순번, 곡명, 가수, 앨범명, 제공 및 확인기관으로 구성된 표로 제공"></div>
        <p class="mar_tp20">3. 보상금신청은 검색화면 상단에 안내된 단체로 관련사항을 문의합니다.</p>
        <div class="mar_tp20"><img src="/images/sub_img/sub_37.jpg" alt="보상금신청 문의 가능한 단체 연락처 화면"></div>
      </div>
      <div style="text-align:center; margin: 30px;"><a href="#1" onclick="closePop();" class="pop_check popup_close">확&nbsp;인</a></div>
    </div>
  </div>
  
  <div id="modalBg"></div>
      <!-- //CONTAINER -->
  <!-- FOOTER str-->
  <!-- 2017변경 -->
  <jsp:include page="/include/2017/footer.jsp" />
    <%-- <jsp:include page="/include/2012/footer.jsp" /> --%>
  <!-- FOOTER end -->
</body>
</html>
