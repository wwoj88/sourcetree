package kr.or.copyright.mls.rght.dao;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
 
public class RghtSqlMapDao extends SqlMapClientDaoSupport implements RghtDao {
	
	// 내권리찾기 음악리스트 조회 
	public List muscRghtList(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscRghtList",map);
	}	
	
	// 내권리찾기 음악리스트 조회 (count)
	public List muscRghtListCount(Map map) {
		return getSqlMapClientTemplate().queryForList("Rght.muscRghtListCount",map);
	}

}
