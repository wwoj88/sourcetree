package kr.or.copyright.mls.miplatform.controller;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.copyright.mls.common.service.BaseService;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.tobesoft.platform.PlatformRequest;
import com.tobesoft.platform.PlatformResponse;
import com.tobesoft.platform.data.PlatformData;
import com.tobesoft.platform.data.VariableList;

public class MiController extends MultiActionController {

	private Log logger = LogFactory.getLog(getClass());

	private int defaultEncodeMethod = PlatformRequest.XML;
	private String defaultCharset = "EUC-KR";	
	private PlatformData outputData = null;
	
	public void execute(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		PlatformRequest  miRequest  = new PlatformRequest(request, defaultCharset);
		PlatformResponse miResponse = new PlatformResponse(response, defaultEncodeMethod, defaultCharset);
		
		PlatformData inputData = miRequest.getPlatformData();
	
		String serviceName = null;
		String methodName  = null;
		
	
		try {
			
		
			Map param = request.getParameterMap();

			miRequest.receiveData();

			VariableList vl = inputData.getVariableList();

			serviceName = vl.getValueAsString("service");
			methodName  = vl.getValueAsString("method");

			logger.debug("serviceName ::: ["+serviceName+"]");
			logger.debug("methodName ::: ["+methodName+"]");
			logger.debug("\nJSESSIONID : " + vl.getValueAsString("JSESSIONID") + "\nServer SessionID : " + request.getSession().getId()+"\n");
			
			ServletContext ctx = request.getSession().getServletContext();
			WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(ctx);
			BaseService baseService = (BaseService)wac.getBean(serviceName);
			outputData = baseService.execute(methodName, miRequest, null);
		
			
		} catch (ClassNotFoundException e) { 
			logger.error(e.toString());
			setResultMessage(-9, "Invalid service call [" + serviceName + "]");
		} catch (NoSuchMethodException e) {
			logger.error(e.toString());
			setResultMessage(-8, "Invalid method call [" + serviceName + "/" + methodName + "]");
		} catch (Exception e) {

			Throwable rootCause = ExceptionUtils.getRootCause(e);

	        if (rootCause == null) {
	            rootCause = e;
	        }
	        logger.error(ExceptionUtils.getStackTrace(rootCause));
			setResultMessage(rootCause.toString());
		} finally {
			logger.debug("outputData : \n" + outputData);
			miResponse.sendData(outputData);
		}
	}
	
	private void setResultMessage(String msg) {

		setResultMessage(-1, msg);
	}
	
	private void setResultMessage(int code, String msg) {

		if(outputData == null) {

			outputData = new PlatformData();
		}

		outputData.getVariableList().add("ErrorCode", code);
		outputData.getVariableList().add("ErrorMsg", msg);
	}
}
