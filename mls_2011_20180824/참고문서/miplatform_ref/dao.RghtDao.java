package kr.or.copyright.mls.rght.dao;

import java.util.List;
import java.util.Map;
 
public interface RghtDao {
	      
	// 내권리찾기 음악리스트 조회 
	public List muscRghtList(Map map);
	
	// 내권리찾기 음악리스트 조회 (count)
	public List muscRghtListCount(Map map);

}
