package kr.or.copyright.mls.rght.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import kr.or.copyright.mls.common.service.BaseService;
import kr.or.copyright.mls.common.utils.FileUtil;
import kr.or.copyright.mls.rght.dao.RghtDao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tobesoft.platform.data.Dataset;

public class RghtServiceImpl extends BaseService implements RghtService {

	private Log logger = LogFactory.getLog(getClass());

	private RghtDao rghtDao;
	
	public void setRghtDao(RghtDao rghtDao){
		this.rghtDao = rghtDao; 
	}
	
	// 내권리찾기 음악리스트 조회 
	public void muscRghtList() throws Exception {

		Dataset ds_condition = getDataset("ds_condition");     

		Map map = getMap(ds_condition, 0);
		
		//DAO호출
		List list  = (List) rghtDao.muscRghtList(map);		
		List pageCount = (List) rghtDao.muscRghtListCount(map); 
	
		//hmap를 dataset로 변환한다.
		addList("ds_list",  list);
		addList("ds_page", pageCount);	

	}

}
